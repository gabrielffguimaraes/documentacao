# Documentação



## Servico-estoque-ui (front-end)

<li> story book documentação : ng run servico-estoque-ui:storybook 
http://127.0.0.1:6006
</li>
<li> compodoc documentação   : npm run docs:serve  http://127.0.0.1:9999 </li>

## Servico-estoque (backend)

<li> swagger documentação : http://localhost:8080/swagger-ui.html </li>

## Servico-relatorio (backend)

<li> swagger documentação : http://localhost:8083/swagger-ui.html </li>

## Servico-controle-acesso (backend)

<li> swagger documentação : http://localhost:8082/swagger-ui.html </li>