'use strict';

customElements.define('compodoc-menu', class extends HTMLElement {
    constructor() {
        super();
        this.isNormalMode = this.getAttribute('mode') === 'normal';
    }

    connectedCallback() {
        this.render(this.isNormalMode);
    }

    render(isNormalMode) {
        let tp = lithtml.html(`
        <nav>
            <ul class="list">
                <li class="title">
                    <a href="index.html" data-type="index-link">webtenax documentation</a>
                </li>

                <li class="divider"></li>
                ${ isNormalMode ? `<div id="book-search-input" role="search"><input type="text" placeholder="Type to search"></div>` : '' }
                <li class="chapter">
                    <a data-type="chapter-link" href="index.html"><span class="icon ion-ios-home"></span>Getting started</a>
                    <ul class="links">
                        <li class="link">
                            <a href="overview.html" data-type="chapter-link">
                                <span class="icon ion-ios-keypad"></span>Overview
                            </a>
                        </li>
                        <li class="link">
                            <a href="index.html" data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>README
                            </a>
                        </li>
                        <li class="link">
                            <a href="changelog.html"  data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>CHANGELOG
                            </a>
                        </li>
                        <li class="link">
                            <a href="license.html"  data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>LICENSE
                            </a>
                        </li>
                                <li class="link">
                                    <a href="dependencies.html" data-type="chapter-link">
                                        <span class="icon ion-ios-list"></span>Dependencies
                                    </a>
                                </li>
                                <li class="link">
                                    <a href="properties.html" data-type="chapter-link">
                                        <span class="icon ion-ios-apps"></span>Properties
                                    </a>
                                </li>
                    </ul>
                </li>
                    <li class="chapter modules">
                        <a data-type="chapter-link" href="modules.html">
                            <div class="menu-toggler linked" data-bs-toggle="collapse" ${ isNormalMode ?
                                'data-bs-target="#modules-links"' : 'data-bs-target="#xs-modules-links"' }>
                                <span class="icon ion-ios-archive"></span>
                                <span class="link-name">Modules</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                        </a>
                        <ul class="links collapse " ${ isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"' }>
                            <li class="link">
                                <a href="modules/AcabamentoMaterialModule.html" data-type="entity-link" >AcabamentoMaterialModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-AcabamentoMaterialModule-7d419cd621c85cc14eb77fc437c23aa236a0f5b3ffbe550701967025b6c7198a0e4483afb11c671de8b73709bd4c0c5fb11925f1d4a4e2cdc483f62ef93d1b05"' : 'data-bs-target="#xs-components-links-module-AcabamentoMaterialModule-7d419cd621c85cc14eb77fc437c23aa236a0f5b3ffbe550701967025b6c7198a0e4483afb11c671de8b73709bd4c0c5fb11925f1d4a4e2cdc483f62ef93d1b05"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AcabamentoMaterialModule-7d419cd621c85cc14eb77fc437c23aa236a0f5b3ffbe550701967025b6c7198a0e4483afb11c671de8b73709bd4c0c5fb11925f1d4a4e2cdc483f62ef93d1b05"' :
                                            'id="xs-components-links-module-AcabamentoMaterialModule-7d419cd621c85cc14eb77fc437c23aa236a0f5b3ffbe550701967025b6c7198a0e4483afb11c671de8b73709bd4c0c5fb11925f1d4a4e2cdc483f62ef93d1b05"' }>
                                            <li class="link">
                                                <a href="components/AcabamentoMaterialComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AcabamentoMaterialComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/AcabamentoMaterialRoutingModule.html" data-type="entity-link" >AcabamentoMaterialRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/AlertModule.html" data-type="entity-link" >AlertModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-AlertModule-6e34bd22a479358145b228097bcb544795443bf1729a37d9bb0a18f4a3869ac19ff3c449725f16dbf76515ac89577fd33d9952be4098eeeab425220cdcf4b4db"' : 'data-bs-target="#xs-components-links-module-AlertModule-6e34bd22a479358145b228097bcb544795443bf1729a37d9bb0a18f4a3869ac19ff3c449725f16dbf76515ac89577fd33d9952be4098eeeab425220cdcf4b4db"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AlertModule-6e34bd22a479358145b228097bcb544795443bf1729a37d9bb0a18f4a3869ac19ff3c449725f16dbf76515ac89577fd33d9952be4098eeeab425220cdcf4b4db"' :
                                            'id="xs-components-links-module-AlertModule-6e34bd22a479358145b228097bcb544795443bf1729a37d9bb0a18f4a3869ac19ff3c449725f16dbf76515ac89577fd33d9952be4098eeeab425220cdcf4b4db"' }>
                                            <li class="link">
                                                <a href="components/AlertDefaultComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AlertDefaultComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ApelidosItemEstoqueModule.html" data-type="entity-link" >ApelidosItemEstoqueModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-ApelidosItemEstoqueModule-ea00260bc8ff45a7caf8af26975ee387478f715441ad2d083ddbfd67be1460c25e5d947bd6808bfd7fbfc59ffed871a76bdd6e8835219e3db8ae775061bfdbaa"' : 'data-bs-target="#xs-components-links-module-ApelidosItemEstoqueModule-ea00260bc8ff45a7caf8af26975ee387478f715441ad2d083ddbfd67be1460c25e5d947bd6808bfd7fbfc59ffed871a76bdd6e8835219e3db8ae775061bfdbaa"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ApelidosItemEstoqueModule-ea00260bc8ff45a7caf8af26975ee387478f715441ad2d083ddbfd67be1460c25e5d947bd6808bfd7fbfc59ffed871a76bdd6e8835219e3db8ae775061bfdbaa"' :
                                            'id="xs-components-links-module-ApelidosItemEstoqueModule-ea00260bc8ff45a7caf8af26975ee387478f715441ad2d083ddbfd67be1460c25e5d947bd6808bfd7fbfc59ffed871a76bdd6e8835219e3db8ae775061bfdbaa"' }>
                                            <li class="link">
                                                <a href="components/ApelidosItemEstoqueComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ApelidosItemEstoqueComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ApelidosItemEstoqueRoutingModule.html" data-type="entity-link" >ApelidosItemEstoqueRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/AppConfigModule.html" data-type="entity-link" >AppConfigModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-AppConfigModule-fa259356769debac6cf755f732a135638da52ba95b970ab2a6f8cf630058f39f8b5526ae0c28d1434c9c19c7d6cb36710dc7005f7a3b4cabb07d8f1b898a2a1a"' : 'data-bs-target="#xs-components-links-module-AppConfigModule-fa259356769debac6cf755f732a135638da52ba95b970ab2a6f8cf630058f39f8b5526ae0c28d1434c9c19c7d6cb36710dc7005f7a3b4cabb07d8f1b898a2a1a"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AppConfigModule-fa259356769debac6cf755f732a135638da52ba95b970ab2a6f8cf630058f39f8b5526ae0c28d1434c9c19c7d6cb36710dc7005f7a3b4cabb07d8f1b898a2a1a"' :
                                            'id="xs-components-links-module-AppConfigModule-fa259356769debac6cf755f732a135638da52ba95b970ab2a6f8cf630058f39f8b5526ae0c28d1434c9c19c7d6cb36710dc7005f7a3b4cabb07d8f1b898a2a1a"' }>
                                            <li class="link">
                                                <a href="components/AppConfigComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AppConfigComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/AppLayoutModule.html" data-type="entity-link" >AppLayoutModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-AppLayoutModule-28a30a988f56775d1727fe38769df327352e8ad6976a980cf1c7dd9a6ef9eb5f4dcf9f613bd8e6ff991a686625030085b773b6590583acdb0cce215ed5f2caef"' : 'data-bs-target="#xs-components-links-module-AppLayoutModule-28a30a988f56775d1727fe38769df327352e8ad6976a980cf1c7dd9a6ef9eb5f4dcf9f613bd8e6ff991a686625030085b773b6590583acdb0cce215ed5f2caef"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AppLayoutModule-28a30a988f56775d1727fe38769df327352e8ad6976a980cf1c7dd9a6ef9eb5f4dcf9f613bd8e6ff991a686625030085b773b6590583acdb0cce215ed5f2caef"' :
                                            'id="xs-components-links-module-AppLayoutModule-28a30a988f56775d1727fe38769df327352e8ad6976a980cf1c7dd9a6ef9eb5f4dcf9f613bd8e6ff991a686625030085b773b6590583acdb0cce215ed5f2caef"' }>
                                            <li class="link">
                                                <a href="components/AppBreadcrumbComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AppBreadcrumbComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/AppFooterComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AppFooterComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/AppLayoutComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AppLayoutComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/AppMenuComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AppMenuComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/AppMenuProfileComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AppMenuProfileComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/AppMenuitemComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AppMenuitemComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/AppRightMenuComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AppRightMenuComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/AppSidebarComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AppSidebarComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/AppTopbarComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AppTopbarComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/AppModule.html" data-type="entity-link" >AppModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-AppModule-bbada15524314b5f5aa8a5920c9abd886612d99c6319cc26593b41bbac2608b239ea76c66e78fd94898741283ada797bfc28684a33946f4b1403d146873a9ee7"' : 'data-bs-target="#xs-components-links-module-AppModule-bbada15524314b5f5aa8a5920c9abd886612d99c6319cc26593b41bbac2608b239ea76c66e78fd94898741283ada797bfc28684a33946f4b1403d146873a9ee7"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AppModule-bbada15524314b5f5aa8a5920c9abd886612d99c6319cc26593b41bbac2608b239ea76c66e78fd94898741283ada797bfc28684a33946f4b1403d146873a9ee7"' :
                                            'id="xs-components-links-module-AppModule-bbada15524314b5f5aa8a5920c9abd886612d99c6319cc26593b41bbac2608b239ea76c66e78fd94898741283ada797bfc28684a33946f4b1403d146873a9ee7"' }>
                                            <li class="link">
                                                <a href="components/AppComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AppComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/AppRoutingModule.html" data-type="entity-link" >AppRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/BaseComboModule.html" data-type="entity-link" >BaseComboModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-BaseComboModule-f1065a6f4bac05202fcd375d4bd8b72526fd90ac28166170a4847ebc9eac898b47e14d307185a8d26554d891c2e2ccdad7b9d00a690e7055bfa15ce59beeefc8"' : 'data-bs-target="#xs-components-links-module-BaseComboModule-f1065a6f4bac05202fcd375d4bd8b72526fd90ac28166170a4847ebc9eac898b47e14d307185a8d26554d891c2e2ccdad7b9d00a690e7055bfa15ce59beeefc8"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-BaseComboModule-f1065a6f4bac05202fcd375d4bd8b72526fd90ac28166170a4847ebc9eac898b47e14d307185a8d26554d891c2e2ccdad7b9d00a690e7055bfa15ce59beeefc8"' :
                                            'id="xs-components-links-module-BaseComboModule-f1065a6f4bac05202fcd375d4bd8b72526fd90ac28166170a4847ebc9eac898b47e14d307185a8d26554d891c2e2ccdad7b9d00a690e7055bfa15ce59beeefc8"' }>
                                            <li class="link">
                                                <a href="components/BaseComboComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >BaseComboComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/BlocoModule.html" data-type="entity-link" >BlocoModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-BlocoModule-8f7fefb7618b27683bf3bec50ab53cc8187b87cb4deb218c0fee98fb001cda7ccf869c437b2e0457813f6c66ee55bd7fc138c31c740103950355506d917939f4"' : 'data-bs-target="#xs-components-links-module-BlocoModule-8f7fefb7618b27683bf3bec50ab53cc8187b87cb4deb218c0fee98fb001cda7ccf869c437b2e0457813f6c66ee55bd7fc138c31c740103950355506d917939f4"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-BlocoModule-8f7fefb7618b27683bf3bec50ab53cc8187b87cb4deb218c0fee98fb001cda7ccf869c437b2e0457813f6c66ee55bd7fc138c31c740103950355506d917939f4"' :
                                            'id="xs-components-links-module-BlocoModule-8f7fefb7618b27683bf3bec50ab53cc8187b87cb4deb218c0fee98fb001cda7ccf869c437b2e0457813f6c66ee55bd7fc138c31c740103950355506d917939f4"' }>
                                            <li class="link">
                                                <a href="components/BlocoComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >BlocoComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/TabelaBlocosEntradaComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >TabelaBlocosEntradaComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/BlocoModule.html" data-type="entity-link" >BlocoModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-BlocoModule-fde3ba4bbc717455783119838c1f5600175551ba3f92717cac0a97b1f6fd0fa1114158652e1b34472c6f5f124e48cf65a9839ac190a12ceea965f76a76f77e8e-1"' : 'data-bs-target="#xs-components-links-module-BlocoModule-fde3ba4bbc717455783119838c1f5600175551ba3f92717cac0a97b1f6fd0fa1114158652e1b34472c6f5f124e48cf65a9839ac190a12ceea965f76a76f77e8e-1"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-BlocoModule-fde3ba4bbc717455783119838c1f5600175551ba3f92717cac0a97b1f6fd0fa1114158652e1b34472c6f5f124e48cf65a9839ac190a12ceea965f76a76f77e8e-1"' :
                                            'id="xs-components-links-module-BlocoModule-fde3ba4bbc717455783119838c1f5600175551ba3f92717cac0a97b1f6fd0fa1114158652e1b34472c6f5f124e48cf65a9839ac190a12ceea965f76a76f77e8e-1"' }>
                                            <li class="link">
                                                <a href="components/BlocoComponent-1.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >BlocoComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/TabelaBlocoComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >TabelaBlocoComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/TabelaBlocoSaidaComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >TabelaBlocoSaidaComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ButtonPecasCortadasEstoqueModule.html" data-type="entity-link" >ButtonPecasCortadasEstoqueModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-ButtonPecasCortadasEstoqueModule-cdea7bf39c86109df83f08c62b84075f1e82ec6f97800c8ae6a9d5d3244f1043a1b947e235f1b2faa6c3467b45491b1d6a68c600927c9af0318992f79200e739"' : 'data-bs-target="#xs-components-links-module-ButtonPecasCortadasEstoqueModule-cdea7bf39c86109df83f08c62b84075f1e82ec6f97800c8ae6a9d5d3244f1043a1b947e235f1b2faa6c3467b45491b1d6a68c600927c9af0318992f79200e739"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ButtonPecasCortadasEstoqueModule-cdea7bf39c86109df83f08c62b84075f1e82ec6f97800c8ae6a9d5d3244f1043a1b947e235f1b2faa6c3467b45491b1d6a68c600927c9af0318992f79200e739"' :
                                            'id="xs-components-links-module-ButtonPecasCortadasEstoqueModule-cdea7bf39c86109df83f08c62b84075f1e82ec6f97800c8ae6a9d5d3244f1043a1b947e235f1b2faa6c3467b45491b1d6a68c600927c9af0318992f79200e739"' }>
                                            <li class="link">
                                                <a href="components/ButtonPecasCortadasEstoqueComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ButtonPecasCortadasEstoqueComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ButtonPecasCortadasLoteModule.html" data-type="entity-link" >ButtonPecasCortadasLoteModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-ButtonPecasCortadasLoteModule-b90177c5d76619ef1d2150a1afd7989b3c50b2a4a2d1967ca5179b166a3d82eee79c3ef8458ebf3b3e662b8257040a24bff85523342d33d14994a5782359349e"' : 'data-bs-target="#xs-components-links-module-ButtonPecasCortadasLoteModule-b90177c5d76619ef1d2150a1afd7989b3c50b2a4a2d1967ca5179b166a3d82eee79c3ef8458ebf3b3e662b8257040a24bff85523342d33d14994a5782359349e"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ButtonPecasCortadasLoteModule-b90177c5d76619ef1d2150a1afd7989b3c50b2a4a2d1967ca5179b166a3d82eee79c3ef8458ebf3b3e662b8257040a24bff85523342d33d14994a5782359349e"' :
                                            'id="xs-components-links-module-ButtonPecasCortadasLoteModule-b90177c5d76619ef1d2150a1afd7989b3c50b2a4a2d1967ca5179b166a3d82eee79c3ef8458ebf3b3e662b8257040a24bff85523342d33d14994a5782359349e"' }>
                                            <li class="link">
                                                <a href="components/ButtonPecasCortadasLoteComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ButtonPecasCortadasLoteComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ButtonPedidoCompraModule.html" data-type="entity-link" >ButtonPedidoCompraModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-ButtonPedidoCompraModule-2c490dd27203c37ef0f0c8e3b8a64d71b57475fa06b0490a12eef0c8f395d281cde7bf6528fa76001c4e22eb791ea8906ce732e01ae13c5495e1275f96cc9af6"' : 'data-bs-target="#xs-components-links-module-ButtonPedidoCompraModule-2c490dd27203c37ef0f0c8e3b8a64d71b57475fa06b0490a12eef0c8f395d281cde7bf6528fa76001c4e22eb791ea8906ce732e01ae13c5495e1275f96cc9af6"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ButtonPedidoCompraModule-2c490dd27203c37ef0f0c8e3b8a64d71b57475fa06b0490a12eef0c8f395d281cde7bf6528fa76001c4e22eb791ea8906ce732e01ae13c5495e1275f96cc9af6"' :
                                            'id="xs-components-links-module-ButtonPedidoCompraModule-2c490dd27203c37ef0f0c8e3b8a64d71b57475fa06b0490a12eef0c8f395d281cde7bf6528fa76001c4e22eb791ea8906ce732e01ae13c5495e1275f96cc9af6"' }>
                                            <li class="link">
                                                <a href="components/ButtonPedidoCompraComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ButtonPedidoCompraComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/CardTenaxModule.html" data-type="entity-link" >CardTenaxModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-CardTenaxModule-3fb2183ade7a58fa10a024bf68201c197a5aca21c949e977ea3e8e02555147a84b0e7eb7cb822e02215c3c0f2ef573305dd23dcba73c7e06305688c4a002cac1"' : 'data-bs-target="#xs-components-links-module-CardTenaxModule-3fb2183ade7a58fa10a024bf68201c197a5aca21c949e977ea3e8e02555147a84b0e7eb7cb822e02215c3c0f2ef573305dd23dcba73c7e06305688c4a002cac1"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-CardTenaxModule-3fb2183ade7a58fa10a024bf68201c197a5aca21c949e977ea3e8e02555147a84b0e7eb7cb822e02215c3c0f2ef573305dd23dcba73c7e06305688c4a002cac1"' :
                                            'id="xs-components-links-module-CardTenaxModule-3fb2183ade7a58fa10a024bf68201c197a5aca21c949e977ea3e8e02555147a84b0e7eb7cb822e02215c3c0f2ef573305dd23dcba73c7e06305688c4a002cac1"' }>
                                            <li class="link">
                                                <a href="components/CardComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >CardComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ClassificacaoNcmModule.html" data-type="entity-link" >ClassificacaoNcmModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-ClassificacaoNcmModule-3fe8808fe5049b7c192b39cfe60b9e10ba79bf55ee0e80ec7cb90af7ed75983336308fc1145208f3ddb2e4771a280d98447e7b6af87db8ae60a21a9b3ff7b137"' : 'data-bs-target="#xs-components-links-module-ClassificacaoNcmModule-3fe8808fe5049b7c192b39cfe60b9e10ba79bf55ee0e80ec7cb90af7ed75983336308fc1145208f3ddb2e4771a280d98447e7b6af87db8ae60a21a9b3ff7b137"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ClassificacaoNcmModule-3fe8808fe5049b7c192b39cfe60b9e10ba79bf55ee0e80ec7cb90af7ed75983336308fc1145208f3ddb2e4771a280d98447e7b6af87db8ae60a21a9b3ff7b137"' :
                                            'id="xs-components-links-module-ClassificacaoNcmModule-3fe8808fe5049b7c192b39cfe60b9e10ba79bf55ee0e80ec7cb90af7ed75983336308fc1145208f3ddb2e4771a280d98447e7b6af87db8ae60a21a9b3ff7b137"' }>
                                            <li class="link">
                                                <a href="components/ClassificacaoNcmComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ClassificacaoNcmComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ClassificacaoNcmRoutingModule.html" data-type="entity-link" >ClassificacaoNcmRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/CombosModule.html" data-type="entity-link" >CombosModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-CombosModule-a05ce313b475120e25030fd4ae700b4f231e05df41eb9d16cc3bf411a9b8315eeda370192923fc02d235cf7b8f2fa911c3e3fbcda47755effaeed8b3a99483c5"' : 'data-bs-target="#xs-components-links-module-CombosModule-a05ce313b475120e25030fd4ae700b4f231e05df41eb9d16cc3bf411a9b8315eeda370192923fc02d235cf7b8f2fa911c3e3fbcda47755effaeed8b3a99483c5"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-CombosModule-a05ce313b475120e25030fd4ae700b4f231e05df41eb9d16cc3bf411a9b8315eeda370192923fc02d235cf7b8f2fa911c3e3fbcda47755effaeed8b3a99483c5"' :
                                            'id="xs-components-links-module-CombosModule-a05ce313b475120e25030fd4ae700b4f231e05df41eb9d16cc3bf411a9b8315eeda370192923fc02d235cf7b8f2fa911c3e3fbcda47755effaeed8b3a99483c5"' }>
                                            <li class="link">
                                                <a href="components/ErroCampoFormularioComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ErroCampoFormularioComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/TabelaSaldosGalpaoComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >TabelaSaldosGalpaoComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ComboStatusPedidoModule.html" data-type="entity-link" >ComboStatusPedidoModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-ComboStatusPedidoModule-8d481db53335a0b8c9b7166b7eb8de20aba538ce49184a7ac19db1add023ac900f2758c78e4cb481a0a51cdc2e4525211920339a074e95d48659548950b99313"' : 'data-bs-target="#xs-components-links-module-ComboStatusPedidoModule-8d481db53335a0b8c9b7166b7eb8de20aba538ce49184a7ac19db1add023ac900f2758c78e4cb481a0a51cdc2e4525211920339a074e95d48659548950b99313"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ComboStatusPedidoModule-8d481db53335a0b8c9b7166b7eb8de20aba538ce49184a7ac19db1add023ac900f2758c78e4cb481a0a51cdc2e4525211920339a074e95d48659548950b99313"' :
                                            'id="xs-components-links-module-ComboStatusPedidoModule-8d481db53335a0b8c9b7166b7eb8de20aba538ce49184a7ac19db1add023ac900f2758c78e4cb481a0a51cdc2e4525211920339a074e95d48659548950b99313"' }>
                                            <li class="link">
                                                <a href="components/ComboStatusPedidoComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ComboStatusPedidoComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ComboTipoPedidoModule.html" data-type="entity-link" >ComboTipoPedidoModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-ComboTipoPedidoModule-c34c36c3fbef572d50df20060a2d0d064118c855cd4d9585c75b82071d02b3d1ee34942671005909c5f5b5e4c1e9ab76b7bece5350181737d88779f316b87cc4"' : 'data-bs-target="#xs-components-links-module-ComboTipoPedidoModule-c34c36c3fbef572d50df20060a2d0d064118c855cd4d9585c75b82071d02b3d1ee34942671005909c5f5b5e4c1e9ab76b7bece5350181737d88779f316b87cc4"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ComboTipoPedidoModule-c34c36c3fbef572d50df20060a2d0d064118c855cd4d9585c75b82071d02b3d1ee34942671005909c5f5b5e4c1e9ab76b7bece5350181737d88779f316b87cc4"' :
                                            'id="xs-components-links-module-ComboTipoPedidoModule-c34c36c3fbef572d50df20060a2d0d064118c855cd4d9585c75b82071d02b3d1ee34942671005909c5f5b5e4c1e9ab76b7bece5350181737d88779f316b87cc4"' }>
                                            <li class="link">
                                                <a href="components/ComboTipoPedidoComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ComboTipoPedidoComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ConfirmModule.html" data-type="entity-link" >ConfirmModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-ConfirmModule-51b6a7c80663c0c19845fe96e37eb96002333afd10f499bdc086d9edb0c8e3a4918da13533d982e71ca247e8be9b8436e130705d0f525ab2394853d30948685c"' : 'data-bs-target="#xs-components-links-module-ConfirmModule-51b6a7c80663c0c19845fe96e37eb96002333afd10f499bdc086d9edb0c8e3a4918da13533d982e71ca247e8be9b8436e130705d0f525ab2394853d30948685c"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ConfirmModule-51b6a7c80663c0c19845fe96e37eb96002333afd10f499bdc086d9edb0c8e3a4918da13533d982e71ca247e8be9b8436e130705d0f525ab2394853d30948685c"' :
                                            'id="xs-components-links-module-ConfirmModule-51b6a7c80663c0c19845fe96e37eb96002333afd10f499bdc086d9edb0c8e3a4918da13533d982e71ca247e8be9b8436e130705d0f525ab2394853d30948685c"' }>
                                            <li class="link">
                                                <a href="components/ConfirmDefaultComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ConfirmDefaultComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/CoreAppModule.html" data-type="entity-link" >CoreAppModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-CoreAppModule-79ddf24d06a4d45ead043eee550cc4c5464387f9220be9e427d74a8b838bb4c99b80a309f8a8764796543ea4200c727f5493c73a1e7440a36d4d62aac4d310db"' : 'data-bs-target="#xs-components-links-module-CoreAppModule-79ddf24d06a4d45ead043eee550cc4c5464387f9220be9e427d74a8b838bb4c99b80a309f8a8764796543ea4200c727f5493c73a1e7440a36d4d62aac4d310db"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-CoreAppModule-79ddf24d06a4d45ead043eee550cc4c5464387f9220be9e427d74a8b838bb4c99b80a309f8a8764796543ea4200c727f5493c73a1e7440a36d4d62aac4d310db"' :
                                            'id="xs-components-links-module-CoreAppModule-79ddf24d06a4d45ead043eee550cc4c5464387f9220be9e427d74a8b838bb4c99b80a309f8a8764796543ea4200c727f5493c73a1e7440a36d4d62aac4d310db"' }>
                                            <li class="link">
                                                <a href="components/SidebarComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >SidebarComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/CustomComboModule.html" data-type="entity-link" >CustomComboModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-CustomComboModule-22bd0c1d6aa7f6a55d5a21c8af2d084362282334d2ee458291d89fc65db99572efae694fcb59e85349450da2b20f2c857a8133156a083080872a797eda6f2d20"' : 'data-bs-target="#xs-components-links-module-CustomComboModule-22bd0c1d6aa7f6a55d5a21c8af2d084362282334d2ee458291d89fc65db99572efae694fcb59e85349450da2b20f2c857a8133156a083080872a797eda6f2d20"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-CustomComboModule-22bd0c1d6aa7f6a55d5a21c8af2d084362282334d2ee458291d89fc65db99572efae694fcb59e85349450da2b20f2c857a8133156a083080872a797eda6f2d20"' :
                                            'id="xs-components-links-module-CustomComboModule-22bd0c1d6aa7f6a55d5a21c8af2d084362282334d2ee458291d89fc65db99572efae694fcb59e85349450da2b20f2c857a8133156a083080872a797eda6f2d20"' }>
                                            <li class="link">
                                                <a href="components/CustomComboComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >CustomComboComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#pipes-links-module-CustomComboModule-22bd0c1d6aa7f6a55d5a21c8af2d084362282334d2ee458291d89fc65db99572efae694fcb59e85349450da2b20f2c857a8133156a083080872a797eda6f2d20"' : 'data-bs-target="#xs-pipes-links-module-CustomComboModule-22bd0c1d6aa7f6a55d5a21c8af2d084362282334d2ee458291d89fc65db99572efae694fcb59e85349450da2b20f2c857a8133156a083080872a797eda6f2d20"' }>
                                            <span class="icon ion-md-add"></span>
                                            <span>Pipes</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="pipes-links-module-CustomComboModule-22bd0c1d6aa7f6a55d5a21c8af2d084362282334d2ee458291d89fc65db99572efae694fcb59e85349450da2b20f2c857a8133156a083080872a797eda6f2d20"' :
                                            'id="xs-pipes-links-module-CustomComboModule-22bd0c1d6aa7f6a55d5a21c8af2d084362282334d2ee458291d89fc65db99572efae694fcb59e85349450da2b20f2c857a8133156a083080872a797eda6f2d20"' }>
                                            <li class="link">
                                                <a href="pipes/FormControlPipe.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FormControlPipe</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/CustomInputModule.html" data-type="entity-link" >CustomInputModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-CustomInputModule-1db727ca28750161c64af944d936dc649bc6557a816ab057b19742903a1c83fc1b9739f1fefa09ce641e92530685ff88e7903a8b7279a0e936242013188fcbea"' : 'data-bs-target="#xs-components-links-module-CustomInputModule-1db727ca28750161c64af944d936dc649bc6557a816ab057b19742903a1c83fc1b9739f1fefa09ce641e92530685ff88e7903a8b7279a0e936242013188fcbea"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-CustomInputModule-1db727ca28750161c64af944d936dc649bc6557a816ab057b19742903a1c83fc1b9739f1fefa09ce641e92530685ff88e7903a8b7279a0e936242013188fcbea"' :
                                            'id="xs-components-links-module-CustomInputModule-1db727ca28750161c64af944d936dc649bc6557a816ab057b19742903a1c83fc1b9739f1fefa09ce641e92530685ff88e7903a8b7279a0e936242013188fcbea"' }>
                                            <li class="link">
                                                <a href="components/CustomInputComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >CustomInputComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#pipes-links-module-CustomInputModule-1db727ca28750161c64af944d936dc649bc6557a816ab057b19742903a1c83fc1b9739f1fefa09ce641e92530685ff88e7903a8b7279a0e936242013188fcbea"' : 'data-bs-target="#xs-pipes-links-module-CustomInputModule-1db727ca28750161c64af944d936dc649bc6557a816ab057b19742903a1c83fc1b9739f1fefa09ce641e92530685ff88e7903a8b7279a0e936242013188fcbea"' }>
                                            <span class="icon ion-md-add"></span>
                                            <span>Pipes</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="pipes-links-module-CustomInputModule-1db727ca28750161c64af944d936dc649bc6557a816ab057b19742903a1c83fc1b9739f1fefa09ce641e92530685ff88e7903a8b7279a0e936242013188fcbea"' :
                                            'id="xs-pipes-links-module-CustomInputModule-1db727ca28750161c64af944d936dc649bc6557a816ab057b19742903a1c83fc1b9739f1fefa09ce641e92530685ff88e7903a8b7279a0e936242013188fcbea"' }>
                                            <li class="link">
                                                <a href="pipes/FormControlPipe.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FormControlPipe</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/DashboardModule.html" data-type="entity-link" >DashboardModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-DashboardModule-a523769dffe48f312334c23a33bc3d15b4f446e9d2c77d1b68ae29f3ce213c95c34ecc91f7de6afbbcc30eec7478a39f17e03f21449bc8cc1011f9149e3e82aa"' : 'data-bs-target="#xs-components-links-module-DashboardModule-a523769dffe48f312334c23a33bc3d15b4f446e9d2c77d1b68ae29f3ce213c95c34ecc91f7de6afbbcc30eec7478a39f17e03f21449bc8cc1011f9149e3e82aa"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-DashboardModule-a523769dffe48f312334c23a33bc3d15b4f446e9d2c77d1b68ae29f3ce213c95c34ecc91f7de6afbbcc30eec7478a39f17e03f21449bc8cc1011f9149e3e82aa"' :
                                            'id="xs-components-links-module-DashboardModule-a523769dffe48f312334c23a33bc3d15b4f446e9d2c77d1b68ae29f3ce213c95c34ecc91f7de6afbbcc30eec7478a39f17e03f21449bc8cc1011f9149e3e82aa"' }>
                                            <li class="link">
                                                <a href="components/DashboardComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >DashboardComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/DetalheRecebimentoItemMaterialModule.html" data-type="entity-link" >DetalheRecebimentoItemMaterialModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-DetalheRecebimentoItemMaterialModule-9f60ab513735429b245fde13ead8fde59a6294444de47317500288a7c6a1751a7938b25cbc5674a23d462f6de69f19abd72042f3765751c07c45acf059d68a33"' : 'data-bs-target="#xs-components-links-module-DetalheRecebimentoItemMaterialModule-9f60ab513735429b245fde13ead8fde59a6294444de47317500288a7c6a1751a7938b25cbc5674a23d462f6de69f19abd72042f3765751c07c45acf059d68a33"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-DetalheRecebimentoItemMaterialModule-9f60ab513735429b245fde13ead8fde59a6294444de47317500288a7c6a1751a7938b25cbc5674a23d462f6de69f19abd72042f3765751c07c45acf059d68a33"' :
                                            'id="xs-components-links-module-DetalheRecebimentoItemMaterialModule-9f60ab513735429b245fde13ead8fde59a6294444de47317500288a7c6a1751a7938b25cbc5674a23d462f6de69f19abd72042f3765751c07c45acf059d68a33"' }>
                                            <li class="link">
                                                <a href="components/DetalheRecebimentoItemMaterialComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >DetalheRecebimentoItemMaterialComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/FormQtdRecebidaComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FormQtdRecebidaComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ItemFornecedorComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ItemFornecedorComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/TabelaLoteRastreabilidadeComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >TabelaLoteRastreabilidadeComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#pipes-links-module-DetalheRecebimentoItemMaterialModule-9f60ab513735429b245fde13ead8fde59a6294444de47317500288a7c6a1751a7938b25cbc5674a23d462f6de69f19abd72042f3765751c07c45acf059d68a33"' : 'data-bs-target="#xs-pipes-links-module-DetalheRecebimentoItemMaterialModule-9f60ab513735429b245fde13ead8fde59a6294444de47317500288a7c6a1751a7938b25cbc5674a23d462f6de69f19abd72042f3765751c07c45acf059d68a33"' }>
                                            <span class="icon ion-md-add"></span>
                                            <span>Pipes</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="pipes-links-module-DetalheRecebimentoItemMaterialModule-9f60ab513735429b245fde13ead8fde59a6294444de47317500288a7c6a1751a7938b25cbc5674a23d462f6de69f19abd72042f3765751c07c45acf059d68a33"' :
                                            'id="xs-pipes-links-module-DetalheRecebimentoItemMaterialModule-9f60ab513735429b245fde13ead8fde59a6294444de47317500288a7c6a1751a7938b25cbc5674a23d462f6de69f19abd72042f3765751c07c45acf059d68a33"' }>
                                            <li class="link">
                                                <a href="pipes/FormataMedidaPipe.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FormataMedidaPipe</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/DetalheRecebimentoMaterialModule.html" data-type="entity-link" >DetalheRecebimentoMaterialModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-DetalheRecebimentoMaterialModule-d17bd175f4280ef4e4b4c61f7dad0bf63d0a8e8ffe39a4312c621014c4a18abcbea0420db7474759a332bbb6dd5c23ac28ddcd56cac86c9970a6a49d772c7f75"' : 'data-bs-target="#xs-components-links-module-DetalheRecebimentoMaterialModule-d17bd175f4280ef4e4b4c61f7dad0bf63d0a8e8ffe39a4312c621014c4a18abcbea0420db7474759a332bbb6dd5c23ac28ddcd56cac86c9970a6a49d772c7f75"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-DetalheRecebimentoMaterialModule-d17bd175f4280ef4e4b4c61f7dad0bf63d0a8e8ffe39a4312c621014c4a18abcbea0420db7474759a332bbb6dd5c23ac28ddcd56cac86c9970a6a49d772c7f75"' :
                                            'id="xs-components-links-module-DetalheRecebimentoMaterialModule-d17bd175f4280ef4e4b4c61f7dad0bf63d0a8e8ffe39a4312c621014c4a18abcbea0420db7474759a332bbb6dd5c23ac28ddcd56cac86c9970a6a49d772c7f75"' }>
                                            <li class="link">
                                                <a href="components/DetalheRecebimentoMaterialComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >DetalheRecebimentoMaterialComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/EntradaPedidoModule.html" data-type="entity-link" >EntradaPedidoModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-EntradaPedidoModule-717275191513cec5362164b13a700db402d86c547eba2dc234e2d5ece5a400ceb18aa9b0a0278e325c44306e71f9c8932c6c8c628162d6559aaa44dfc76622f2"' : 'data-bs-target="#xs-components-links-module-EntradaPedidoModule-717275191513cec5362164b13a700db402d86c547eba2dc234e2d5ece5a400ceb18aa9b0a0278e325c44306e71f9c8932c6c8c628162d6559aaa44dfc76622f2"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-EntradaPedidoModule-717275191513cec5362164b13a700db402d86c547eba2dc234e2d5ece5a400ceb18aa9b0a0278e325c44306e71f9c8932c6c8c628162d6559aaa44dfc76622f2"' :
                                            'id="xs-components-links-module-EntradaPedidoModule-717275191513cec5362164b13a700db402d86c547eba2dc234e2d5ece5a400ceb18aa9b0a0278e325c44306e71f9c8932c6c8c628162d6559aaa44dfc76622f2"' }>
                                            <li class="link">
                                                <a href="components/TemplateComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >TemplateComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/EntradaPedidoRoutingModule.html" data-type="entity-link" >EntradaPedidoRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/EstadoMaterialModule.html" data-type="entity-link" >EstadoMaterialModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-EstadoMaterialModule-cf36dfd24cba66eae8c16292590bdfdbf78dd306cad0488ce6faa5208db0bf8bbf91c72ada2045ac59e1cd2efbd7e0f9c374eca158607696bdb679b7ed9f5a40"' : 'data-bs-target="#xs-components-links-module-EstadoMaterialModule-cf36dfd24cba66eae8c16292590bdfdbf78dd306cad0488ce6faa5208db0bf8bbf91c72ada2045ac59e1cd2efbd7e0f9c374eca158607696bdb679b7ed9f5a40"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-EstadoMaterialModule-cf36dfd24cba66eae8c16292590bdfdbf78dd306cad0488ce6faa5208db0bf8bbf91c72ada2045ac59e1cd2efbd7e0f9c374eca158607696bdb679b7ed9f5a40"' :
                                            'id="xs-components-links-module-EstadoMaterialModule-cf36dfd24cba66eae8c16292590bdfdbf78dd306cad0488ce6faa5208db0bf8bbf91c72ada2045ac59e1cd2efbd7e0f9c374eca158607696bdb679b7ed9f5a40"' }>
                                            <li class="link">
                                                <a href="components/EstadoMaterialComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >EstadoMaterialComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/EstadoMaterialRoutingModule.html" data-type="entity-link" >EstadoMaterialRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/EstoqueCadastrosModule.html" data-type="entity-link" >EstoqueCadastrosModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-EstoqueCadastrosModule-942b64bb3cfb54e8764624b096d63ef80fd952d2f731a1d8847552148c5bf2841ebe2e2b4d9611a24772ec59aab8d39c2ebaa0d5498cf977c964a90e5e865cd4"' : 'data-bs-target="#xs-components-links-module-EstoqueCadastrosModule-942b64bb3cfb54e8764624b096d63ef80fd952d2f731a1d8847552148c5bf2841ebe2e2b4d9611a24772ec59aab8d39c2ebaa0d5498cf977c964a90e5e865cd4"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-EstoqueCadastrosModule-942b64bb3cfb54e8764624b096d63ef80fd952d2f731a1d8847552148c5bf2841ebe2e2b4d9611a24772ec59aab8d39c2ebaa0d5498cf977c964a90e5e865cd4"' :
                                            'id="xs-components-links-module-EstoqueCadastrosModule-942b64bb3cfb54e8764624b096d63ef80fd952d2f731a1d8847552148c5bf2841ebe2e2b4d9611a24772ec59aab8d39c2ebaa0d5498cf977c964a90e5e865cd4"' }>
                                            <li class="link">
                                                <a href="components/PortalCadastrosComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >PortalCadastrosComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/TemplateComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >TemplateComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/EstoqueCadastrosRoutingModule.html" data-type="entity-link" >EstoqueCadastrosRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/FiltroEntradaModule.html" data-type="entity-link" >FiltroEntradaModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-FiltroEntradaModule-8f4c6b3b0494b66f8c67a378949ef6be1b459daf5e034835bb122afde56fd91a6be58499732629bd97112e2b9b9bc1f9573856e1adc05a78ebaad4fa3b20626d"' : 'data-bs-target="#xs-components-links-module-FiltroEntradaModule-8f4c6b3b0494b66f8c67a378949ef6be1b459daf5e034835bb122afde56fd91a6be58499732629bd97112e2b9b9bc1f9573856e1adc05a78ebaad4fa3b20626d"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-FiltroEntradaModule-8f4c6b3b0494b66f8c67a378949ef6be1b459daf5e034835bb122afde56fd91a6be58499732629bd97112e2b9b9bc1f9573856e1adc05a78ebaad4fa3b20626d"' :
                                            'id="xs-components-links-module-FiltroEntradaModule-8f4c6b3b0494b66f8c67a378949ef6be1b459daf5e034835bb122afde56fd91a6be58499732629bd97112e2b9b9bc1f9573856e1adc05a78ebaad4fa3b20626d"' }>
                                            <li class="link">
                                                <a href="components/FiltroEntradaComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FiltroEntradaComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/FiltroItemEstoqueModule.html" data-type="entity-link" >FiltroItemEstoqueModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-FiltroItemEstoqueModule-974752b6e7f4a00f05dc7eab14eb08ec2a70060c6e604b1c2f69719b19b478893e14fdda07db79129cb3ea273e78fa540f6e3eb9893351f3e4f783d7f638aa54"' : 'data-bs-target="#xs-components-links-module-FiltroItemEstoqueModule-974752b6e7f4a00f05dc7eab14eb08ec2a70060c6e604b1c2f69719b19b478893e14fdda07db79129cb3ea273e78fa540f6e3eb9893351f3e4f783d7f638aa54"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-FiltroItemEstoqueModule-974752b6e7f4a00f05dc7eab14eb08ec2a70060c6e604b1c2f69719b19b478893e14fdda07db79129cb3ea273e78fa540f6e3eb9893351f3e4f783d7f638aa54"' :
                                            'id="xs-components-links-module-FiltroItemEstoqueModule-974752b6e7f4a00f05dc7eab14eb08ec2a70060c6e604b1c2f69719b19b478893e14fdda07db79129cb3ea273e78fa540f6e3eb9893351f3e4f783d7f638aa54"' }>
                                            <li class="link">
                                                <a href="components/FiltroItemEstoqueComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FiltroItemEstoqueComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/FiltroLoteRastreabilidadeModule.html" data-type="entity-link" >FiltroLoteRastreabilidadeModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-FiltroLoteRastreabilidadeModule-4769190708022b92a4df9c427b14b0a9abfcd51e28441e5d81c5d085add34c15edc4895f184162f25b3d6d5d9ab88ab4f4e5230686892222f62e8e45a6d83f59"' : 'data-bs-target="#xs-components-links-module-FiltroLoteRastreabilidadeModule-4769190708022b92a4df9c427b14b0a9abfcd51e28441e5d81c5d085add34c15edc4895f184162f25b3d6d5d9ab88ab4f4e5230686892222f62e8e45a6d83f59"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-FiltroLoteRastreabilidadeModule-4769190708022b92a4df9c427b14b0a9abfcd51e28441e5d81c5d085add34c15edc4895f184162f25b3d6d5d9ab88ab4f4e5230686892222f62e8e45a6d83f59"' :
                                            'id="xs-components-links-module-FiltroLoteRastreabilidadeModule-4769190708022b92a4df9c427b14b0a9abfcd51e28441e5d81c5d085add34c15edc4895f184162f25b3d6d5d9ab88ab4f4e5230686892222f62e8e45a6d83f59"' }>
                                            <li class="link">
                                                <a href="components/FiltroLoteRastreabilidadeComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FiltroLoteRastreabilidadeComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/FiltroManutencaoLoteModule.html" data-type="entity-link" >FiltroManutencaoLoteModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-FiltroManutencaoLoteModule-70fc9957756016ee00dbf4a4203df9a2b633c6df4ede5c2fc1cce0eafecdf12be95f5df58a78561b12d46b765e42d62bd3a2bb6974c0a9b87b703a14e50a6f9a"' : 'data-bs-target="#xs-components-links-module-FiltroManutencaoLoteModule-70fc9957756016ee00dbf4a4203df9a2b633c6df4ede5c2fc1cce0eafecdf12be95f5df58a78561b12d46b765e42d62bd3a2bb6974c0a9b87b703a14e50a6f9a"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-FiltroManutencaoLoteModule-70fc9957756016ee00dbf4a4203df9a2b633c6df4ede5c2fc1cce0eafecdf12be95f5df58a78561b12d46b765e42d62bd3a2bb6974c0a9b87b703a14e50a6f9a"' :
                                            'id="xs-components-links-module-FiltroManutencaoLoteModule-70fc9957756016ee00dbf4a4203df9a2b633c6df4ede5c2fc1cce0eafecdf12be95f5df58a78561b12d46b765e42d62bd3a2bb6974c0a9b87b703a14e50a6f9a"' }>
                                            <li class="link">
                                                <a href="components/FiltroManutencaoLoteComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FiltroManutencaoLoteComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/FiltroPedidoCompraModule.html" data-type="entity-link" >FiltroPedidoCompraModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-FiltroPedidoCompraModule-f5bd8b2e4a0f1c19fe42449a9897cb83277c4f4603821bf56993086c8c0503926c4b79b1d9268d692b17be2f6cb0d420c71f544f438434b9c1225f8639a4e893"' : 'data-bs-target="#xs-components-links-module-FiltroPedidoCompraModule-f5bd8b2e4a0f1c19fe42449a9897cb83277c4f4603821bf56993086c8c0503926c4b79b1d9268d692b17be2f6cb0d420c71f544f438434b9c1225f8639a4e893"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-FiltroPedidoCompraModule-f5bd8b2e4a0f1c19fe42449a9897cb83277c4f4603821bf56993086c8c0503926c4b79b1d9268d692b17be2f6cb0d420c71f544f438434b9c1225f8639a4e893"' :
                                            'id="xs-components-links-module-FiltroPedidoCompraModule-f5bd8b2e4a0f1c19fe42449a9897cb83277c4f4603821bf56993086c8c0503926c4b79b1d9268d692b17be2f6cb0d420c71f544f438434b9c1225f8639a4e893"' }>
                                            <li class="link">
                                                <a href="components/FiltroPedidoCompraComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FiltroPedidoCompraComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/FiltroRecebimentoMaterialModule.html" data-type="entity-link" >FiltroRecebimentoMaterialModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-FiltroRecebimentoMaterialModule-fd0f39c55bcadf6cc8b2ae1be39b8cdf6024391d1caf05b987cb4f38458c573001499c1a7403e534200fe63c0a6ae8cf1131cec77b5e9a0b8a6e3e522a581d99"' : 'data-bs-target="#xs-components-links-module-FiltroRecebimentoMaterialModule-fd0f39c55bcadf6cc8b2ae1be39b8cdf6024391d1caf05b987cb4f38458c573001499c1a7403e534200fe63c0a6ae8cf1131cec77b5e9a0b8a6e3e522a581d99"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-FiltroRecebimentoMaterialModule-fd0f39c55bcadf6cc8b2ae1be39b8cdf6024391d1caf05b987cb4f38458c573001499c1a7403e534200fe63c0a6ae8cf1131cec77b5e9a0b8a6e3e522a581d99"' :
                                            'id="xs-components-links-module-FiltroRecebimentoMaterialModule-fd0f39c55bcadf6cc8b2ae1be39b8cdf6024391d1caf05b987cb4f38458c573001499c1a7403e534200fe63c0a6ae8cf1131cec77b5e9a0b8a6e3e522a581d99"' }>
                                            <li class="link">
                                                <a href="components/FiltroRecebimentoMaterialComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FiltroRecebimentoMaterialComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/FormAcabamentoMaterialModule.html" data-type="entity-link" >FormAcabamentoMaterialModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-FormAcabamentoMaterialModule-12d77622909460ced68e673523884305b0e4ddb0beb5f734361baf7502f256d436ab3c9a5dd125ba5be1b319717e7b9c6ffa13b09c912901ec2d57d49711c7a0"' : 'data-bs-target="#xs-components-links-module-FormAcabamentoMaterialModule-12d77622909460ced68e673523884305b0e4ddb0beb5f734361baf7502f256d436ab3c9a5dd125ba5be1b319717e7b9c6ffa13b09c912901ec2d57d49711c7a0"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-FormAcabamentoMaterialModule-12d77622909460ced68e673523884305b0e4ddb0beb5f734361baf7502f256d436ab3c9a5dd125ba5be1b319717e7b9c6ffa13b09c912901ec2d57d49711c7a0"' :
                                            'id="xs-components-links-module-FormAcabamentoMaterialModule-12d77622909460ced68e673523884305b0e4ddb0beb5f734361baf7502f256d436ab3c9a5dd125ba5be1b319717e7b9c6ffa13b09c912901ec2d57d49711c7a0"' }>
                                            <li class="link">
                                                <a href="components/FormAcabamentoMaterialComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FormAcabamentoMaterialComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/FormaMaterialModule.html" data-type="entity-link" >FormaMaterialModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-FormaMaterialModule-57ad929ae857861099cf209058d0d16934e8648a37bfdf77241ed18f2a3e1fec659484bbbf412820919e234c7557ff54b9203e8181f43e3b199e50bfaaea9502"' : 'data-bs-target="#xs-components-links-module-FormaMaterialModule-57ad929ae857861099cf209058d0d16934e8648a37bfdf77241ed18f2a3e1fec659484bbbf412820919e234c7557ff54b9203e8181f43e3b199e50bfaaea9502"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-FormaMaterialModule-57ad929ae857861099cf209058d0d16934e8648a37bfdf77241ed18f2a3e1fec659484bbbf412820919e234c7557ff54b9203e8181f43e3b199e50bfaaea9502"' :
                                            'id="xs-components-links-module-FormaMaterialModule-57ad929ae857861099cf209058d0d16934e8648a37bfdf77241ed18f2a3e1fec659484bbbf412820919e234c7557ff54b9203e8181f43e3b199e50bfaaea9502"' }>
                                            <li class="link">
                                                <a href="components/FormaMaterialComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FormaMaterialComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/FormaMaterialRoutingModule.html" data-type="entity-link" >FormaMaterialRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/FormApelidosItemEstoqueModule.html" data-type="entity-link" >FormApelidosItemEstoqueModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-FormApelidosItemEstoqueModule-e369bc9e092588f26467aab5d51ffbb6225e249d2f68f5501f9f8a615712a2b7361e05a754f02ecff3a732d1961e1a340781aa7cf4deaf589122d97542921e0a"' : 'data-bs-target="#xs-components-links-module-FormApelidosItemEstoqueModule-e369bc9e092588f26467aab5d51ffbb6225e249d2f68f5501f9f8a615712a2b7361e05a754f02ecff3a732d1961e1a340781aa7cf4deaf589122d97542921e0a"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-FormApelidosItemEstoqueModule-e369bc9e092588f26467aab5d51ffbb6225e249d2f68f5501f9f8a615712a2b7361e05a754f02ecff3a732d1961e1a340781aa7cf4deaf589122d97542921e0a"' :
                                            'id="xs-components-links-module-FormApelidosItemEstoqueModule-e369bc9e092588f26467aab5d51ffbb6225e249d2f68f5501f9f8a615712a2b7361e05a754f02ecff3a732d1961e1a340781aa7cf4deaf589122d97542921e0a"' }>
                                            <li class="link">
                                                <a href="components/FormApelidosItemEstoqueComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FormApelidosItemEstoqueComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/FormClassificacaoNcmModule.html" data-type="entity-link" >FormClassificacaoNcmModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-FormClassificacaoNcmModule-3151433e30860de895c8832b5e464aeb2cedab41563f2d2e27e33aefde139a4e5259036a07bdb23e0d4167926dc8ba333cba146864db49eb71988f19cb560167"' : 'data-bs-target="#xs-components-links-module-FormClassificacaoNcmModule-3151433e30860de895c8832b5e464aeb2cedab41563f2d2e27e33aefde139a4e5259036a07bdb23e0d4167926dc8ba333cba146864db49eb71988f19cb560167"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-FormClassificacaoNcmModule-3151433e30860de895c8832b5e464aeb2cedab41563f2d2e27e33aefde139a4e5259036a07bdb23e0d4167926dc8ba333cba146864db49eb71988f19cb560167"' :
                                            'id="xs-components-links-module-FormClassificacaoNcmModule-3151433e30860de895c8832b5e464aeb2cedab41563f2d2e27e33aefde139a4e5259036a07bdb23e0d4167926dc8ba333cba146864db49eb71988f19cb560167"' }>
                                            <li class="link">
                                                <a href="components/FormClassificacaoNcmComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FormClassificacaoNcmComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/FormDevolucaoModule.html" data-type="entity-link" >FormDevolucaoModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-FormDevolucaoModule-a3b9e60bef555ca67c8ebc5aaa106eab043d57b4f1a4c9806114384075108279de2e270b616b6d803a382eba4290bc738f6ff6444ada37719764c39661242e29"' : 'data-bs-target="#xs-components-links-module-FormDevolucaoModule-a3b9e60bef555ca67c8ebc5aaa106eab043d57b4f1a4c9806114384075108279de2e270b616b6d803a382eba4290bc738f6ff6444ada37719764c39661242e29"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-FormDevolucaoModule-a3b9e60bef555ca67c8ebc5aaa106eab043d57b4f1a4c9806114384075108279de2e270b616b6d803a382eba4290bc738f6ff6444ada37719764c39661242e29"' :
                                            'id="xs-components-links-module-FormDevolucaoModule-a3b9e60bef555ca67c8ebc5aaa106eab043d57b4f1a4c9806114384075108279de2e270b616b6d803a382eba4290bc738f6ff6444ada37719764c39661242e29"' }>
                                            <li class="link">
                                                <a href="components/FormDevolucaoComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FormDevolucaoComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/FormEntradaItemEstoqueModule.html" data-type="entity-link" >FormEntradaItemEstoqueModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-FormEntradaItemEstoqueModule-0489452ce18638909ea89369095ede7ce589daa6344e5f34467759cbed72f17e3bf043013e670aa8e22a310e80e9185c0f5c7deee5a3722bb9718bb452591e12"' : 'data-bs-target="#xs-components-links-module-FormEntradaItemEstoqueModule-0489452ce18638909ea89369095ede7ce589daa6344e5f34467759cbed72f17e3bf043013e670aa8e22a310e80e9185c0f5c7deee5a3722bb9718bb452591e12"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-FormEntradaItemEstoqueModule-0489452ce18638909ea89369095ede7ce589daa6344e5f34467759cbed72f17e3bf043013e670aa8e22a310e80e9185c0f5c7deee5a3722bb9718bb452591e12"' :
                                            'id="xs-components-links-module-FormEntradaItemEstoqueModule-0489452ce18638909ea89369095ede7ce589daa6344e5f34467759cbed72f17e3bf043013e670aa8e22a310e80e9185c0f5c7deee5a3722bb9718bb452591e12"' }>
                                            <li class="link">
                                                <a href="components/FormEntradaItemEstoqueComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FormEntradaItemEstoqueComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/FormEstadoMaterialModule.html" data-type="entity-link" >FormEstadoMaterialModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-FormEstadoMaterialModule-d64724698954cdad5109e2d8804abbb35813cd822618754057a2745d208998e1025247145bc1bbf6a682b67cadc12e606a9ca8f89a05eb1e97f71fb2e0f9f828"' : 'data-bs-target="#xs-components-links-module-FormEstadoMaterialModule-d64724698954cdad5109e2d8804abbb35813cd822618754057a2745d208998e1025247145bc1bbf6a682b67cadc12e606a9ca8f89a05eb1e97f71fb2e0f9f828"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-FormEstadoMaterialModule-d64724698954cdad5109e2d8804abbb35813cd822618754057a2745d208998e1025247145bc1bbf6a682b67cadc12e606a9ca8f89a05eb1e97f71fb2e0f9f828"' :
                                            'id="xs-components-links-module-FormEstadoMaterialModule-d64724698954cdad5109e2d8804abbb35813cd822618754057a2745d208998e1025247145bc1bbf6a682b67cadc12e606a9ca8f89a05eb1e97f71fb2e0f9f828"' }>
                                            <li class="link">
                                                <a href="components/FormEstadoMaterialComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FormEstadoMaterialComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/FormFormaMaterialModule.html" data-type="entity-link" >FormFormaMaterialModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-FormFormaMaterialModule-6e28c4bac47b7a42a85ad5d5a5d6f6dd195566d791fd0358d5c215c8ac18d79d6c5a0b26e36f747006c9dc00e64d566650913c9648fd440d17a7ab7a04c25a66"' : 'data-bs-target="#xs-components-links-module-FormFormaMaterialModule-6e28c4bac47b7a42a85ad5d5a5d6f6dd195566d791fd0358d5c215c8ac18d79d6c5a0b26e36f747006c9dc00e64d566650913c9648fd440d17a7ab7a04c25a66"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-FormFormaMaterialModule-6e28c4bac47b7a42a85ad5d5a5d6f6dd195566d791fd0358d5c215c8ac18d79d6c5a0b26e36f747006c9dc00e64d566650913c9648fd440d17a7ab7a04c25a66"' :
                                            'id="xs-components-links-module-FormFormaMaterialModule-6e28c4bac47b7a42a85ad5d5a5d6f6dd195566d791fd0358d5c215c8ac18d79d6c5a0b26e36f747006c9dc00e64d566650913c9648fd440d17a7ab7a04c25a66"' }>
                                            <li class="link">
                                                <a href="components/FormFormaMaterialComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FormFormaMaterialComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/FormFornecedorCadastroModule.html" data-type="entity-link" >FormFornecedorCadastroModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-FormFornecedorCadastroModule-6ab2badaf223faf83d25e54ac97f1bba28a7ae93eb33619a2c53e7a93d3fc7d0e248fa039bbefb732e4b91845222ecb78534fc25cb1f7ea5f8872be37974c16c"' : 'data-bs-target="#xs-components-links-module-FormFornecedorCadastroModule-6ab2badaf223faf83d25e54ac97f1bba28a7ae93eb33619a2c53e7a93d3fc7d0e248fa039bbefb732e4b91845222ecb78534fc25cb1f7ea5f8872be37974c16c"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-FormFornecedorCadastroModule-6ab2badaf223faf83d25e54ac97f1bba28a7ae93eb33619a2c53e7a93d3fc7d0e248fa039bbefb732e4b91845222ecb78534fc25cb1f7ea5f8872be37974c16c"' :
                                            'id="xs-components-links-module-FormFornecedorCadastroModule-6ab2badaf223faf83d25e54ac97f1bba28a7ae93eb33619a2c53e7a93d3fc7d0e248fa039bbefb732e4b91845222ecb78534fc25cb1f7ea5f8872be37974c16c"' }>
                                            <li class="link">
                                                <a href="components/FormFornecedorCadastroComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FormFornecedorCadastroComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/FormInfoFornecedorComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FormInfoFornecedorComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/FormGalpaoModule.html" data-type="entity-link" >FormGalpaoModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-FormGalpaoModule-a5355c5e484ba63354d1709f793a62b878d663db6bbddb2880cfdf7e88fd3a36b58c8e140ce44c804b08ac21f73416ba7a71662cc203d2a9b8ecfa0a03e05ffc"' : 'data-bs-target="#xs-components-links-module-FormGalpaoModule-a5355c5e484ba63354d1709f793a62b878d663db6bbddb2880cfdf7e88fd3a36b58c8e140ce44c804b08ac21f73416ba7a71662cc203d2a9b8ecfa0a03e05ffc"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-FormGalpaoModule-a5355c5e484ba63354d1709f793a62b878d663db6bbddb2880cfdf7e88fd3a36b58c8e140ce44c804b08ac21f73416ba7a71662cc203d2a9b8ecfa0a03e05ffc"' :
                                            'id="xs-components-links-module-FormGalpaoModule-a5355c5e484ba63354d1709f793a62b878d663db6bbddb2880cfdf7e88fd3a36b58c8e140ce44c804b08ac21f73416ba7a71662cc203d2a9b8ecfa0a03e05ffc"' }>
                                            <li class="link">
                                                <a href="components/FormGalpaoComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FormGalpaoComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#pipes-links-module-FormGalpaoModule-a5355c5e484ba63354d1709f793a62b878d663db6bbddb2880cfdf7e88fd3a36b58c8e140ce44c804b08ac21f73416ba7a71662cc203d2a9b8ecfa0a03e05ffc"' : 'data-bs-target="#xs-pipes-links-module-FormGalpaoModule-a5355c5e484ba63354d1709f793a62b878d663db6bbddb2880cfdf7e88fd3a36b58c8e140ce44c804b08ac21f73416ba7a71662cc203d2a9b8ecfa0a03e05ffc"' }>
                                            <span class="icon ion-md-add"></span>
                                            <span>Pipes</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="pipes-links-module-FormGalpaoModule-a5355c5e484ba63354d1709f793a62b878d663db6bbddb2880cfdf7e88fd3a36b58c8e140ce44c804b08ac21f73416ba7a71662cc203d2a9b8ecfa0a03e05ffc"' :
                                            'id="xs-pipes-links-module-FormGalpaoModule-a5355c5e484ba63354d1709f793a62b878d663db6bbddb2880cfdf7e88fd3a36b58c8e140ce44c804b08ac21f73416ba7a71662cc203d2a9b8ecfa0a03e05ffc"' }>
                                            <li class="link">
                                                <a href="pipes/FormControlPipe.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FormControlPipe</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/FormGrupoModule.html" data-type="entity-link" >FormGrupoModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-FormGrupoModule-8d4dc90a9a6278adfdbb0a5c24f91e5a494932935c5dd1ae71cc2bf4de53b0c368a3c7be30e1895ca72e10caea1436588ed21c9519196ae0b040945d56fbbeb4"' : 'data-bs-target="#xs-components-links-module-FormGrupoModule-8d4dc90a9a6278adfdbb0a5c24f91e5a494932935c5dd1ae71cc2bf4de53b0c368a3c7be30e1895ca72e10caea1436588ed21c9519196ae0b040945d56fbbeb4"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-FormGrupoModule-8d4dc90a9a6278adfdbb0a5c24f91e5a494932935c5dd1ae71cc2bf4de53b0c368a3c7be30e1895ca72e10caea1436588ed21c9519196ae0b040945d56fbbeb4"' :
                                            'id="xs-components-links-module-FormGrupoModule-8d4dc90a9a6278adfdbb0a5c24f91e5a494932935c5dd1ae71cc2bf4de53b0c368a3c7be30e1895ca72e10caea1436588ed21c9519196ae0b040945d56fbbeb4"' }>
                                            <li class="link">
                                                <a href="components/FormGrupoComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FormGrupoComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/FormGrupoSegmentoModule.html" data-type="entity-link" >FormGrupoSegmentoModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-FormGrupoSegmentoModule-bd338d76c77eb01bd2630627239a78e16116fd9f85ecadbfeaf06eb930f3cc3484decfe7ae96af8a45abeaf4c2c60ee4b7b179d73b48761c89b1380987df11b4"' : 'data-bs-target="#xs-components-links-module-FormGrupoSegmentoModule-bd338d76c77eb01bd2630627239a78e16116fd9f85ecadbfeaf06eb930f3cc3484decfe7ae96af8a45abeaf4c2c60ee4b7b179d73b48761c89b1380987df11b4"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-FormGrupoSegmentoModule-bd338d76c77eb01bd2630627239a78e16116fd9f85ecadbfeaf06eb930f3cc3484decfe7ae96af8a45abeaf4c2c60ee4b7b179d73b48761c89b1380987df11b4"' :
                                            'id="xs-components-links-module-FormGrupoSegmentoModule-bd338d76c77eb01bd2630627239a78e16116fd9f85ecadbfeaf06eb930f3cc3484decfe7ae96af8a45abeaf4c2c60ee4b7b179d73b48761c89b1380987df11b4"' }>
                                            <li class="link">
                                                <a href="components/FormGrupoSegmentoComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FormGrupoSegmentoComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/FormItemEstoqueModule.html" data-type="entity-link" >FormItemEstoqueModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-FormItemEstoqueModule-4befd8963a3da91894175618e7bd6ca9fa098933f17ca7d23c71a5b570e6a81b5b03216b7726a8cbe59707515944f224fc8877624572afe0d21a186e04c77e23"' : 'data-bs-target="#xs-components-links-module-FormItemEstoqueModule-4befd8963a3da91894175618e7bd6ca9fa098933f17ca7d23c71a5b570e6a81b5b03216b7726a8cbe59707515944f224fc8877624572afe0d21a186e04c77e23"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-FormItemEstoqueModule-4befd8963a3da91894175618e7bd6ca9fa098933f17ca7d23c71a5b570e6a81b5b03216b7726a8cbe59707515944f224fc8877624572afe0d21a186e04c77e23"' :
                                            'id="xs-components-links-module-FormItemEstoqueModule-4befd8963a3da91894175618e7bd6ca9fa098933f17ca7d23c71a5b570e6a81b5b03216b7726a8cbe59707515944f224fc8877624572afe0d21a186e04c77e23"' }>
                                            <li class="link">
                                                <a href="components/CustoItemEstoqueComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >CustoItemEstoqueComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/FormItemEstoqueComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FormItemEstoqueComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/QuantidadeItemComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >QuantidadeItemComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/FormItemPedidoCompraModule.html" data-type="entity-link" >FormItemPedidoCompraModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-FormItemPedidoCompraModule-e3764e78ddd93b5795a42b421a982dd3f942597acc4a3df25be9e504ffd16fe85205a1943ca5eeb2817caac28c94d9b544a82730d718a9d09f01aab2d3ca910e"' : 'data-bs-target="#xs-components-links-module-FormItemPedidoCompraModule-e3764e78ddd93b5795a42b421a982dd3f942597acc4a3df25be9e504ffd16fe85205a1943ca5eeb2817caac28c94d9b544a82730d718a9d09f01aab2d3ca910e"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-FormItemPedidoCompraModule-e3764e78ddd93b5795a42b421a982dd3f942597acc4a3df25be9e504ffd16fe85205a1943ca5eeb2817caac28c94d9b544a82730d718a9d09f01aab2d3ca910e"' :
                                            'id="xs-components-links-module-FormItemPedidoCompraModule-e3764e78ddd93b5795a42b421a982dd3f942597acc4a3df25be9e504ffd16fe85205a1943ca5eeb2817caac28c94d9b544a82730d718a9d09f01aab2d3ca910e"' }>
                                            <li class="link">
                                                <a href="components/FormItemPedidoCompraComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FormItemPedidoCompraComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/FormItemPedidoCompraNovoModule.html" data-type="entity-link" >FormItemPedidoCompraNovoModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-FormItemPedidoCompraNovoModule-647f01f0d4476b928cb95fc7308272e3dbb06a16ced927601e35ffd84cec393bd4a45da1592386b78e8c56dacea413fb592b2269dcfe7e39010ec5470e5e66ae"' : 'data-bs-target="#xs-components-links-module-FormItemPedidoCompraNovoModule-647f01f0d4476b928cb95fc7308272e3dbb06a16ced927601e35ffd84cec393bd4a45da1592386b78e8c56dacea413fb592b2269dcfe7e39010ec5470e5e66ae"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-FormItemPedidoCompraNovoModule-647f01f0d4476b928cb95fc7308272e3dbb06a16ced927601e35ffd84cec393bd4a45da1592386b78e8c56dacea413fb592b2269dcfe7e39010ec5470e5e66ae"' :
                                            'id="xs-components-links-module-FormItemPedidoCompraNovoModule-647f01f0d4476b928cb95fc7308272e3dbb06a16ced927601e35ffd84cec393bd4a45da1592386b78e8c56dacea413fb592b2269dcfe7e39010ec5470e5e66ae"' }>
                                            <li class="link">
                                                <a href="components/FormItemPedidoCompraNovoComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FormItemPedidoCompraNovoComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/FormLoteRastreabilidadeModule.html" data-type="entity-link" >FormLoteRastreabilidadeModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-FormLoteRastreabilidadeModule-41cca396e011d3ee57539e0ba975b49fa32a7d0db685f4dca4da7c86e9e86f17bb11f7b15dc29a184079debddab35e5ac88a120194dc28240f284ae0550a8740"' : 'data-bs-target="#xs-components-links-module-FormLoteRastreabilidadeModule-41cca396e011d3ee57539e0ba975b49fa32a7d0db685f4dca4da7c86e9e86f17bb11f7b15dc29a184079debddab35e5ac88a120194dc28240f284ae0550a8740"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-FormLoteRastreabilidadeModule-41cca396e011d3ee57539e0ba975b49fa32a7d0db685f4dca4da7c86e9e86f17bb11f7b15dc29a184079debddab35e5ac88a120194dc28240f284ae0550a8740"' :
                                            'id="xs-components-links-module-FormLoteRastreabilidadeModule-41cca396e011d3ee57539e0ba975b49fa32a7d0db685f4dca4da7c86e9e86f17bb11f7b15dc29a184079debddab35e5ac88a120194dc28240f284ae0550a8740"' }>
                                            <li class="link">
                                                <a href="components/FormLoteRastreabilidadeComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FormLoteRastreabilidadeComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/TabelaComprasComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >TabelaComprasComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#pipes-links-module-FormLoteRastreabilidadeModule-41cca396e011d3ee57539e0ba975b49fa32a7d0db685f4dca4da7c86e9e86f17bb11f7b15dc29a184079debddab35e5ac88a120194dc28240f284ae0550a8740"' : 'data-bs-target="#xs-pipes-links-module-FormLoteRastreabilidadeModule-41cca396e011d3ee57539e0ba975b49fa32a7d0db685f4dca4da7c86e9e86f17bb11f7b15dc29a184079debddab35e5ac88a120194dc28240f284ae0550a8740"' }>
                                            <span class="icon ion-md-add"></span>
                                            <span>Pipes</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="pipes-links-module-FormLoteRastreabilidadeModule-41cca396e011d3ee57539e0ba975b49fa32a7d0db685f4dca4da7c86e9e86f17bb11f7b15dc29a184079debddab35e5ac88a120194dc28240f284ae0550a8740"' :
                                            'id="xs-pipes-links-module-FormLoteRastreabilidadeModule-41cca396e011d3ee57539e0ba975b49fa32a7d0db685f4dca4da7c86e9e86f17bb11f7b15dc29a184079debddab35e5ac88a120194dc28240f284ae0550a8740"' }>
                                            <li class="link">
                                                <a href="pipes/FormataMedidaPipe.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FormataMedidaPipe</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/FormManutencaoLoteModule.html" data-type="entity-link" >FormManutencaoLoteModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-FormManutencaoLoteModule-259a034f8dd9086a437d9e724000d16e00d82a4d00ebbe30e10a1ca9c085d4d1fc5ed409d45f1cad8d535f4a6a47c01ad566fdaac835548a8008644288ee8d1d"' : 'data-bs-target="#xs-components-links-module-FormManutencaoLoteModule-259a034f8dd9086a437d9e724000d16e00d82a4d00ebbe30e10a1ca9c085d4d1fc5ed409d45f1cad8d535f4a6a47c01ad566fdaac835548a8008644288ee8d1d"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-FormManutencaoLoteModule-259a034f8dd9086a437d9e724000d16e00d82a4d00ebbe30e10a1ca9c085d4d1fc5ed409d45f1cad8d535f4a6a47c01ad566fdaac835548a8008644288ee8d1d"' :
                                            'id="xs-components-links-module-FormManutencaoLoteModule-259a034f8dd9086a437d9e724000d16e00d82a4d00ebbe30e10a1ca9c085d4d1fc5ed409d45f1cad8d535f4a6a47c01ad566fdaac835548a8008644288ee8d1d"' }>
                                            <li class="link">
                                                <a href="components/FormManutencaoLoteComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FormManutencaoLoteComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/FormMaterialAchadoSemloteModule.html" data-type="entity-link" >FormMaterialAchadoSemloteModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-FormMaterialAchadoSemloteModule-93058e404ba329408c535c7e5b963a73c1cdc2a50911d46a10078a09cce8ea2c603673d4c1265b180add00df00e30d3defe6f5830d6017e9a81f72f8e0bb7e77"' : 'data-bs-target="#xs-components-links-module-FormMaterialAchadoSemloteModule-93058e404ba329408c535c7e5b963a73c1cdc2a50911d46a10078a09cce8ea2c603673d4c1265b180add00df00e30d3defe6f5830d6017e9a81f72f8e0bb7e77"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-FormMaterialAchadoSemloteModule-93058e404ba329408c535c7e5b963a73c1cdc2a50911d46a10078a09cce8ea2c603673d4c1265b180add00df00e30d3defe6f5830d6017e9a81f72f8e0bb7e77"' :
                                            'id="xs-components-links-module-FormMaterialAchadoSemloteModule-93058e404ba329408c535c7e5b963a73c1cdc2a50911d46a10078a09cce8ea2c603673d4c1265b180add00df00e30d3defe6f5830d6017e9a81f72f8e0bb7e77"' }>
                                            <li class="link">
                                                <a href="components/FormMaterialAchadoSemloteComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FormMaterialAchadoSemloteComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/FormMaterialMedidaModule.html" data-type="entity-link" >FormMaterialMedidaModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-FormMaterialMedidaModule-6b1b8a444d24d609576e15d5d16cd9b1a6b447ee524dce56d2afd4757933b4ea5ca3ecb9512251284145dc1ff9900c02b9c2606253f4ebeeb9b925b6617678f4"' : 'data-bs-target="#xs-components-links-module-FormMaterialMedidaModule-6b1b8a444d24d609576e15d5d16cd9b1a6b447ee524dce56d2afd4757933b4ea5ca3ecb9512251284145dc1ff9900c02b9c2606253f4ebeeb9b925b6617678f4"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-FormMaterialMedidaModule-6b1b8a444d24d609576e15d5d16cd9b1a6b447ee524dce56d2afd4757933b4ea5ca3ecb9512251284145dc1ff9900c02b9c2606253f4ebeeb9b925b6617678f4"' :
                                            'id="xs-components-links-module-FormMaterialMedidaModule-6b1b8a444d24d609576e15d5d16cd9b1a6b447ee524dce56d2afd4757933b4ea5ca3ecb9512251284145dc1ff9900c02b9c2606253f4ebeeb9b925b6617678f4"' }>
                                            <li class="link">
                                                <a href="components/FormMaterialMedidaComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FormMaterialMedidaComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/FormMaterialModule.html" data-type="entity-link" >FormMaterialModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-FormMaterialModule-b3d797352d8a2ccd26206c0f0cf4a1141da7c78be27f7d0e56ca2f031f7d13dd0d203c13f03b8a85c0a504895764906ce7ada85550b598d303390d913e5789a3"' : 'data-bs-target="#xs-components-links-module-FormMaterialModule-b3d797352d8a2ccd26206c0f0cf4a1141da7c78be27f7d0e56ca2f031f7d13dd0d203c13f03b8a85c0a504895764906ce7ada85550b598d303390d913e5789a3"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-FormMaterialModule-b3d797352d8a2ccd26206c0f0cf4a1141da7c78be27f7d0e56ca2f031f7d13dd0d203c13f03b8a85c0a504895764906ce7ada85550b598d303390d913e5789a3"' :
                                            'id="xs-components-links-module-FormMaterialModule-b3d797352d8a2ccd26206c0f0cf4a1141da7c78be27f7d0e56ca2f031f7d13dd0d203c13f03b8a85c0a504895764906ce7ada85550b598d303390d913e5789a3"' }>
                                            <li class="link">
                                                <a href="components/FormMaterialComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FormMaterialComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/FormMovimentacaoModule.html" data-type="entity-link" >FormMovimentacaoModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-FormMovimentacaoModule-e05bbc086962ea37206e0c01cc85708751d61b2556c584980618a964fead96bb190804c6b86121dc9d01907607d860194b3e09896e7323331f0229ebeb60c373"' : 'data-bs-target="#xs-components-links-module-FormMovimentacaoModule-e05bbc086962ea37206e0c01cc85708751d61b2556c584980618a964fead96bb190804c6b86121dc9d01907607d860194b3e09896e7323331f0229ebeb60c373"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-FormMovimentacaoModule-e05bbc086962ea37206e0c01cc85708751d61b2556c584980618a964fead96bb190804c6b86121dc9d01907607d860194b3e09896e7323331f0229ebeb60c373"' :
                                            'id="xs-components-links-module-FormMovimentacaoModule-e05bbc086962ea37206e0c01cc85708751d61b2556c584980618a964fead96bb190804c6b86121dc9d01907607d860194b3e09896e7323331f0229ebeb60c373"' }>
                                            <li class="link">
                                                <a href="components/FormMovimentacaoComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FormMovimentacaoComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/QuantidadeTotalComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >QuantidadeTotalComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/FormPedidoCompraModule.html" data-type="entity-link" >FormPedidoCompraModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-FormPedidoCompraModule-2bcba9ae1462da5acea1a7a4289f906ecaee2849fc039683c0c569ff866ee05801292a5f309352b4767e90763f81f9d76e15e5b7fbe09361997a707cb69c1337"' : 'data-bs-target="#xs-components-links-module-FormPedidoCompraModule-2bcba9ae1462da5acea1a7a4289f906ecaee2849fc039683c0c569ff866ee05801292a5f309352b4767e90763f81f9d76e15e5b7fbe09361997a707cb69c1337"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-FormPedidoCompraModule-2bcba9ae1462da5acea1a7a4289f906ecaee2849fc039683c0c569ff866ee05801292a5f309352b4767e90763f81f9d76e15e5b7fbe09361997a707cb69c1337"' :
                                            'id="xs-components-links-module-FormPedidoCompraModule-2bcba9ae1462da5acea1a7a4289f906ecaee2849fc039683c0c569ff866ee05801292a5f309352b4767e90763f81f9d76e15e5b7fbe09361997a707cb69c1337"' }>
                                            <li class="link">
                                                <a href="components/FormPedidoCompraComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FormPedidoCompraComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/TabelaFornecedorContatoComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >TabelaFornecedorContatoComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/TabelaItemPedidoNovoComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >TabelaItemPedidoNovoComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#pipes-links-module-FormPedidoCompraModule-2bcba9ae1462da5acea1a7a4289f906ecaee2849fc039683c0c569ff866ee05801292a5f309352b4767e90763f81f9d76e15e5b7fbe09361997a707cb69c1337"' : 'data-bs-target="#xs-pipes-links-module-FormPedidoCompraModule-2bcba9ae1462da5acea1a7a4289f906ecaee2849fc039683c0c569ff866ee05801292a5f309352b4767e90763f81f9d76e15e5b7fbe09361997a707cb69c1337"' }>
                                            <span class="icon ion-md-add"></span>
                                            <span>Pipes</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="pipes-links-module-FormPedidoCompraModule-2bcba9ae1462da5acea1a7a4289f906ecaee2849fc039683c0c569ff866ee05801292a5f309352b4767e90763f81f9d76e15e5b7fbe09361997a707cb69c1337"' :
                                            'id="xs-pipes-links-module-FormPedidoCompraModule-2bcba9ae1462da5acea1a7a4289f906ecaee2849fc039683c0c569ff866ee05801292a5f309352b4767e90763f81f9d76e15e5b7fbe09361997a707cb69c1337"' }>
                                            <li class="link">
                                                <a href="pipes/FormataMedidaPipe.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FormataMedidaPipe</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/FormPercentualBaixaBlocoModule.html" data-type="entity-link" >FormPercentualBaixaBlocoModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-FormPercentualBaixaBlocoModule-1411c91a7471add60957403a4ed9e5d980f137c91020c20e519507693aad88a2e7ac8d15325619cdf2a1784390cf8cb9923b954175c5dcde82a4856180bb42c4"' : 'data-bs-target="#xs-components-links-module-FormPercentualBaixaBlocoModule-1411c91a7471add60957403a4ed9e5d980f137c91020c20e519507693aad88a2e7ac8d15325619cdf2a1784390cf8cb9923b954175c5dcde82a4856180bb42c4"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-FormPercentualBaixaBlocoModule-1411c91a7471add60957403a4ed9e5d980f137c91020c20e519507693aad88a2e7ac8d15325619cdf2a1784390cf8cb9923b954175c5dcde82a4856180bb42c4"' :
                                            'id="xs-components-links-module-FormPercentualBaixaBlocoModule-1411c91a7471add60957403a4ed9e5d980f137c91020c20e519507693aad88a2e7ac8d15325619cdf2a1784390cf8cb9923b954175c5dcde82a4856180bb42c4"' }>
                                            <li class="link">
                                                <a href="components/FormPercentualBaixaBlocoComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FormPercentualBaixaBlocoComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/FormPesoBaixaBlocoModule.html" data-type="entity-link" >FormPesoBaixaBlocoModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-FormPesoBaixaBlocoModule-8d232de28c6e4b5a44e34bf763784c328c0834090d9f8a0ef763396aea82e842eb73fafbe5089044ef3e6e0e34961a1cd5c03a7c70bc32c28657769f928251ec"' : 'data-bs-target="#xs-components-links-module-FormPesoBaixaBlocoModule-8d232de28c6e4b5a44e34bf763784c328c0834090d9f8a0ef763396aea82e842eb73fafbe5089044ef3e6e0e34961a1cd5c03a7c70bc32c28657769f928251ec"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-FormPesoBaixaBlocoModule-8d232de28c6e4b5a44e34bf763784c328c0834090d9f8a0ef763396aea82e842eb73fafbe5089044ef3e6e0e34961a1cd5c03a7c70bc32c28657769f928251ec"' :
                                            'id="xs-components-links-module-FormPesoBaixaBlocoModule-8d232de28c6e4b5a44e34bf763784c328c0834090d9f8a0ef763396aea82e842eb73fafbe5089044ef3e6e0e34961a1cd5c03a7c70bc32c28657769f928251ec"' }>
                                            <li class="link">
                                                <a href="components/FormPesoBaixaBlocoComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FormPesoBaixaBlocoComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/FormPrecoPraticadoEstoqueModule.html" data-type="entity-link" >FormPrecoPraticadoEstoqueModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-FormPrecoPraticadoEstoqueModule-efb200c6621ebcafa4b7c3a8601e572ccd8f8d1a789b0a64add568b6999be2c34d6daba46cf5688f8d22bc573f135924fa8239e960ee413fe1984c13bdce0963"' : 'data-bs-target="#xs-components-links-module-FormPrecoPraticadoEstoqueModule-efb200c6621ebcafa4b7c3a8601e572ccd8f8d1a789b0a64add568b6999be2c34d6daba46cf5688f8d22bc573f135924fa8239e960ee413fe1984c13bdce0963"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-FormPrecoPraticadoEstoqueModule-efb200c6621ebcafa4b7c3a8601e572ccd8f8d1a789b0a64add568b6999be2c34d6daba46cf5688f8d22bc573f135924fa8239e960ee413fe1984c13bdce0963"' :
                                            'id="xs-components-links-module-FormPrecoPraticadoEstoqueModule-efb200c6621ebcafa4b7c3a8601e572ccd8f8d1a789b0a64add568b6999be2c34d6daba46cf5688f8d22bc573f135924fa8239e960ee413fe1984c13bdce0963"' }>
                                            <li class="link">
                                                <a href="components/FormPrecoPraticadoEstoqueComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FormPrecoPraticadoEstoqueComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/FormPrecosColunasModule.html" data-type="entity-link" >FormPrecosColunasModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-FormPrecosColunasModule-cf226be49f4cbc4a920059b98de31333410cd69fed5d2d25206abb5c0aaa7768ea73df027579c18ddf8518e15f85f0ecbe9c314967419e1e71db4f479676e009"' : 'data-bs-target="#xs-components-links-module-FormPrecosColunasModule-cf226be49f4cbc4a920059b98de31333410cd69fed5d2d25206abb5c0aaa7768ea73df027579c18ddf8518e15f85f0ecbe9c314967419e1e71db4f479676e009"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-FormPrecosColunasModule-cf226be49f4cbc4a920059b98de31333410cd69fed5d2d25206abb5c0aaa7768ea73df027579c18ddf8518e15f85f0ecbe9c314967419e1e71db4f479676e009"' :
                                            'id="xs-components-links-module-FormPrecosColunasModule-cf226be49f4cbc4a920059b98de31333410cd69fed5d2d25206abb5c0aaa7768ea73df027579c18ddf8518e15f85f0ecbe9c314967419e1e71db4f479676e009"' }>
                                            <li class="link">
                                                <a href="components/FormPrecosColunasComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FormPrecosColunasComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/FormSegmentoModule.html" data-type="entity-link" >FormSegmentoModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-FormSegmentoModule-252bbf5b5bf2ccca9bc0c729c30c5737b62f4f3e2494b8544b0c65a266db85b7302b3bac52bb2dee2ffe4e3fc8faa72dd3cfc4ec79a349b18cfd1ae6ca90e281"' : 'data-bs-target="#xs-components-links-module-FormSegmentoModule-252bbf5b5bf2ccca9bc0c729c30c5737b62f4f3e2494b8544b0c65a266db85b7302b3bac52bb2dee2ffe4e3fc8faa72dd3cfc4ec79a349b18cfd1ae6ca90e281"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-FormSegmentoModule-252bbf5b5bf2ccca9bc0c729c30c5737b62f4f3e2494b8544b0c65a266db85b7302b3bac52bb2dee2ffe4e3fc8faa72dd3cfc4ec79a349b18cfd1ae6ca90e281"' :
                                            'id="xs-components-links-module-FormSegmentoModule-252bbf5b5bf2ccca9bc0c729c30c5737b62f4f3e2494b8544b0c65a266db85b7302b3bac52bb2dee2ffe4e3fc8faa72dd3cfc4ec79a349b18cfd1ae6ca90e281"' }>
                                            <li class="link">
                                                <a href="components/FormSegmentoComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FormSegmentoComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/FormSubfatorModule.html" data-type="entity-link" >FormSubfatorModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-FormSubfatorModule-2a86578894c7970d58277394cf8aff4d89eab731aed0a1e3bb28e2e3db6b63a313a5f12452557c01cd67f98005c2de864aab31db05ff109b782c98c56e679440"' : 'data-bs-target="#xs-components-links-module-FormSubfatorModule-2a86578894c7970d58277394cf8aff4d89eab731aed0a1e3bb28e2e3db6b63a313a5f12452557c01cd67f98005c2de864aab31db05ff109b782c98c56e679440"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-FormSubfatorModule-2a86578894c7970d58277394cf8aff4d89eab731aed0a1e3bb28e2e3db6b63a313a5f12452557c01cd67f98005c2de864aab31db05ff109b782c98c56e679440"' :
                                            'id="xs-components-links-module-FormSubfatorModule-2a86578894c7970d58277394cf8aff4d89eab731aed0a1e3bb28e2e3db6b63a313a5f12452557c01cd67f98005c2de864aab31db05ff109b782c98c56e679440"' }>
                                            <li class="link">
                                                <a href="components/FormSubfatorComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FormSubfatorComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/FormSubGrupoModule.html" data-type="entity-link" >FormSubGrupoModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-FormSubGrupoModule-5e0b46f1842c9cc692db568dca62bd59501678371fcd8c690e592f76a0c479eec5ee8ad33eace57979279c8ca74ce35af548a18b5ace48b617cc56887a60ab3a"' : 'data-bs-target="#xs-components-links-module-FormSubGrupoModule-5e0b46f1842c9cc692db568dca62bd59501678371fcd8c690e592f76a0c479eec5ee8ad33eace57979279c8ca74ce35af548a18b5ace48b617cc56887a60ab3a"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-FormSubGrupoModule-5e0b46f1842c9cc692db568dca62bd59501678371fcd8c690e592f76a0c479eec5ee8ad33eace57979279c8ca74ce35af548a18b5ace48b617cc56887a60ab3a"' :
                                            'id="xs-components-links-module-FormSubGrupoModule-5e0b46f1842c9cc692db568dca62bd59501678371fcd8c690e592f76a0c479eec5ee8ad33eace57979279c8ca74ce35af548a18b5ace48b617cc56887a60ab3a"' }>
                                            <li class="link">
                                                <a href="components/FormSubGrupoComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FormSubGrupoComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/FormTipoMaterialModule.html" data-type="entity-link" >FormTipoMaterialModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-FormTipoMaterialModule-d514b57d4ee0d18c2551454516e4c9a03b368c5dae0cb1c8e34bf8dba88bc3fe369d191f752a9a31d37c06b5606004b5fd31d126354448e5bf76d85b18b04d8d"' : 'data-bs-target="#xs-components-links-module-FormTipoMaterialModule-d514b57d4ee0d18c2551454516e4c9a03b368c5dae0cb1c8e34bf8dba88bc3fe369d191f752a9a31d37c06b5606004b5fd31d126354448e5bf76d85b18b04d8d"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-FormTipoMaterialModule-d514b57d4ee0d18c2551454516e4c9a03b368c5dae0cb1c8e34bf8dba88bc3fe369d191f752a9a31d37c06b5606004b5fd31d126354448e5bf76d85b18b04d8d"' :
                                            'id="xs-components-links-module-FormTipoMaterialModule-d514b57d4ee0d18c2551454516e4c9a03b368c5dae0cb1c8e34bf8dba88bc3fe369d191f752a9a31d37c06b5606004b5fd31d126354448e5bf76d85b18b04d8d"' }>
                                            <li class="link">
                                                <a href="components/FormTipoMaterialComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FormTipoMaterialComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/FormTipoMovimentacaoModule.html" data-type="entity-link" >FormTipoMovimentacaoModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-FormTipoMovimentacaoModule-f7a175d839ecd474b1fc37e155c059784cde13ae4bc3e130db0404e017327d2eceee46c6f73c376bbdaf1d881830b0d830f45b80d64a878db526d59df0c21aa7"' : 'data-bs-target="#xs-components-links-module-FormTipoMovimentacaoModule-f7a175d839ecd474b1fc37e155c059784cde13ae4bc3e130db0404e017327d2eceee46c6f73c376bbdaf1d881830b0d830f45b80d64a878db526d59df0c21aa7"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-FormTipoMovimentacaoModule-f7a175d839ecd474b1fc37e155c059784cde13ae4bc3e130db0404e017327d2eceee46c6f73c376bbdaf1d881830b0d830f45b80d64a878db526d59df0c21aa7"' :
                                            'id="xs-components-links-module-FormTipoMovimentacaoModule-f7a175d839ecd474b1fc37e155c059784cde13ae4bc3e130db0404e017327d2eceee46c6f73c376bbdaf1d881830b0d830f45b80d64a878db526d59df0c21aa7"' }>
                                            <li class="link">
                                                <a href="components/BaseFormComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >BaseFormComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/FormTipoMovimentacaoComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FormTipoMovimentacaoComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#pipes-links-module-FormTipoMovimentacaoModule-f7a175d839ecd474b1fc37e155c059784cde13ae4bc3e130db0404e017327d2eceee46c6f73c376bbdaf1d881830b0d830f45b80d64a878db526d59df0c21aa7"' : 'data-bs-target="#xs-pipes-links-module-FormTipoMovimentacaoModule-f7a175d839ecd474b1fc37e155c059784cde13ae4bc3e130db0404e017327d2eceee46c6f73c376bbdaf1d881830b0d830f45b80d64a878db526d59df0c21aa7"' }>
                                            <span class="icon ion-md-add"></span>
                                            <span>Pipes</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="pipes-links-module-FormTipoMovimentacaoModule-f7a175d839ecd474b1fc37e155c059784cde13ae4bc3e130db0404e017327d2eceee46c6f73c376bbdaf1d881830b0d830f45b80d64a878db526d59df0c21aa7"' :
                                            'id="xs-pipes-links-module-FormTipoMovimentacaoModule-f7a175d839ecd474b1fc37e155c059784cde13ae4bc3e130db0404e017327d2eceee46c6f73c376bbdaf1d881830b0d830f45b80d64a878db526d59df0c21aa7"' }>
                                            <li class="link">
                                                <a href="pipes/FormControlPipe.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FormControlPipe</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/FormTratamentoTermicoModule.html" data-type="entity-link" >FormTratamentoTermicoModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-FormTratamentoTermicoModule-68493bdb709cc71903df972131cbca7197c438344a9f8bc2b140ef0be9338f85ab06fb3dc6a9ac3a5cc7393d6a043594506e0227e390083b1a139a08e4531962"' : 'data-bs-target="#xs-components-links-module-FormTratamentoTermicoModule-68493bdb709cc71903df972131cbca7197c438344a9f8bc2b140ef0be9338f85ab06fb3dc6a9ac3a5cc7393d6a043594506e0227e390083b1a139a08e4531962"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-FormTratamentoTermicoModule-68493bdb709cc71903df972131cbca7197c438344a9f8bc2b140ef0be9338f85ab06fb3dc6a9ac3a5cc7393d6a043594506e0227e390083b1a139a08e4531962"' :
                                            'id="xs-components-links-module-FormTratamentoTermicoModule-68493bdb709cc71903df972131cbca7197c438344a9f8bc2b140ef0be9338f85ab06fb3dc6a9ac3a5cc7393d6a043594506e0227e390083b1a139a08e4531962"' }>
                                            <li class="link">
                                                <a href="components/FormTratamentoTermicoComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FormTratamentoTermicoComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/FormUnidadeLocalModule.html" data-type="entity-link" >FormUnidadeLocalModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-FormUnidadeLocalModule-c0123080145acb7a598b126c7f78cfe381c119f8e3ec880541991f8c26504567ae34141e8e7ad0740430d91975b5d421df10efd5ab69e72376f43e7c741bab3e"' : 'data-bs-target="#xs-components-links-module-FormUnidadeLocalModule-c0123080145acb7a598b126c7f78cfe381c119f8e3ec880541991f8c26504567ae34141e8e7ad0740430d91975b5d421df10efd5ab69e72376f43e7c741bab3e"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-FormUnidadeLocalModule-c0123080145acb7a598b126c7f78cfe381c119f8e3ec880541991f8c26504567ae34141e8e7ad0740430d91975b5d421df10efd5ab69e72376f43e7c741bab3e"' :
                                            'id="xs-components-links-module-FormUnidadeLocalModule-c0123080145acb7a598b126c7f78cfe381c119f8e3ec880541991f8c26504567ae34141e8e7ad0740430d91975b5d421df10efd5ab69e72376f43e7c741bab3e"' }>
                                            <li class="link">
                                                <a href="components/FormUnidadeLocalComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FormUnidadeLocalComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/FornecedorModule.html" data-type="entity-link" >FornecedorModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-FornecedorModule-d53feadae7d18510c80bbf8237be4690acb2afa34891606025ab959597548a1a992e3fdaa8c66cb5fe5a1811b2b84befaa5cb29d65abdb8b14be7cee748c9134"' : 'data-bs-target="#xs-components-links-module-FornecedorModule-d53feadae7d18510c80bbf8237be4690acb2afa34891606025ab959597548a1a992e3fdaa8c66cb5fe5a1811b2b84befaa5cb29d65abdb8b14be7cee748c9134"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-FornecedorModule-d53feadae7d18510c80bbf8237be4690acb2afa34891606025ab959597548a1a992e3fdaa8c66cb5fe5a1811b2b84befaa5cb29d65abdb8b14be7cee748c9134"' :
                                            'id="xs-components-links-module-FornecedorModule-d53feadae7d18510c80bbf8237be4690acb2afa34891606025ab959597548a1a992e3fdaa8c66cb5fe5a1811b2b84befaa5cb29d65abdb8b14be7cee748c9134"' }>
                                            <li class="link">
                                                <a href="components/FornecedorComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FornecedorComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/FornecedorRoutingModule.html" data-type="entity-link" >FornecedorRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/GalpaoModule.html" data-type="entity-link" >GalpaoModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-GalpaoModule-acaba613cd734104f3316bf87e5027ecd6771b8211bfc73304ef2c5ac6ddc407bf1f6b3ea81fc37b735b14bb5df72693c0ec6b7506d8cda7677be401efabae2c"' : 'data-bs-target="#xs-components-links-module-GalpaoModule-acaba613cd734104f3316bf87e5027ecd6771b8211bfc73304ef2c5ac6ddc407bf1f6b3ea81fc37b735b14bb5df72693c0ec6b7506d8cda7677be401efabae2c"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-GalpaoModule-acaba613cd734104f3316bf87e5027ecd6771b8211bfc73304ef2c5ac6ddc407bf1f6b3ea81fc37b735b14bb5df72693c0ec6b7506d8cda7677be401efabae2c"' :
                                            'id="xs-components-links-module-GalpaoModule-acaba613cd734104f3316bf87e5027ecd6771b8211bfc73304ef2c5ac6ddc407bf1f6b3ea81fc37b735b14bb5df72693c0ec6b7506d8cda7677be401efabae2c"' }>
                                            <li class="link">
                                                <a href="components/GalpaoComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >GalpaoComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/GalpaoRoutingModule.html" data-type="entity-link" >GalpaoRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/GeraLoteItemPedidoModule.html" data-type="entity-link" >GeraLoteItemPedidoModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-GeraLoteItemPedidoModule-c382a0b2802466d7d91e6030099e0b9783c86c92fe285d31c362706805b1fdaf36fe68df9679c2705d92b6e88fe1e197c985e33491b259393342ddfe6736d0b3"' : 'data-bs-target="#xs-components-links-module-GeraLoteItemPedidoModule-c382a0b2802466d7d91e6030099e0b9783c86c92fe285d31c362706805b1fdaf36fe68df9679c2705d92b6e88fe1e197c985e33491b259393342ddfe6736d0b3"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-GeraLoteItemPedidoModule-c382a0b2802466d7d91e6030099e0b9783c86c92fe285d31c362706805b1fdaf36fe68df9679c2705d92b6e88fe1e197c985e33491b259393342ddfe6736d0b3"' :
                                            'id="xs-components-links-module-GeraLoteItemPedidoModule-c382a0b2802466d7d91e6030099e0b9783c86c92fe285d31c362706805b1fdaf36fe68df9679c2705d92b6e88fe1e197c985e33491b259393342ddfe6736d0b3"' }>
                                            <li class="link">
                                                <a href="components/GeraLoteItemPedidoComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >GeraLoteItemPedidoComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#pipes-links-module-GeraLoteItemPedidoModule-c382a0b2802466d7d91e6030099e0b9783c86c92fe285d31c362706805b1fdaf36fe68df9679c2705d92b6e88fe1e197c985e33491b259393342ddfe6736d0b3"' : 'data-bs-target="#xs-pipes-links-module-GeraLoteItemPedidoModule-c382a0b2802466d7d91e6030099e0b9783c86c92fe285d31c362706805b1fdaf36fe68df9679c2705d92b6e88fe1e197c985e33491b259393342ddfe6736d0b3"' }>
                                            <span class="icon ion-md-add"></span>
                                            <span>Pipes</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="pipes-links-module-GeraLoteItemPedidoModule-c382a0b2802466d7d91e6030099e0b9783c86c92fe285d31c362706805b1fdaf36fe68df9679c2705d92b6e88fe1e197c985e33491b259393342ddfe6736d0b3"' :
                                            'id="xs-pipes-links-module-GeraLoteItemPedidoModule-c382a0b2802466d7d91e6030099e0b9783c86c92fe285d31c362706805b1fdaf36fe68df9679c2705d92b6e88fe1e197c985e33491b259393342ddfe6736d0b3"' }>
                                            <li class="link">
                                                <a href="pipes/FormataMedidaPipe.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FormataMedidaPipe</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/GrupoModule.html" data-type="entity-link" >GrupoModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-GrupoModule-2f75cc6541c712d0c00a59e904d47ceb2d0b3a2ed8842883ae2b76f69f29b8dfac39856073ac3088863ca057d5c3fa95536339a63caeaaecba5699c87433b5fa"' : 'data-bs-target="#xs-components-links-module-GrupoModule-2f75cc6541c712d0c00a59e904d47ceb2d0b3a2ed8842883ae2b76f69f29b8dfac39856073ac3088863ca057d5c3fa95536339a63caeaaecba5699c87433b5fa"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-GrupoModule-2f75cc6541c712d0c00a59e904d47ceb2d0b3a2ed8842883ae2b76f69f29b8dfac39856073ac3088863ca057d5c3fa95536339a63caeaaecba5699c87433b5fa"' :
                                            'id="xs-components-links-module-GrupoModule-2f75cc6541c712d0c00a59e904d47ceb2d0b3a2ed8842883ae2b76f69f29b8dfac39856073ac3088863ca057d5c3fa95536339a63caeaaecba5699c87433b5fa"' }>
                                            <li class="link">
                                                <a href="components/GrupoComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >GrupoComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/GrupoRoutingModule.html" data-type="entity-link" >GrupoRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/GrupoSegmentoModule.html" data-type="entity-link" >GrupoSegmentoModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-GrupoSegmentoModule-3f0e90d3d1d4a4d4ea1c940b1cb0b6a519e34da4e24232bca0a2aeb55d40bed83e96364f5d907b1ad3a03342a4bcc374eacf0dc6e85dd86fa6639510a1b3f953"' : 'data-bs-target="#xs-components-links-module-GrupoSegmentoModule-3f0e90d3d1d4a4d4ea1c940b1cb0b6a519e34da4e24232bca0a2aeb55d40bed83e96364f5d907b1ad3a03342a4bcc374eacf0dc6e85dd86fa6639510a1b3f953"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-GrupoSegmentoModule-3f0e90d3d1d4a4d4ea1c940b1cb0b6a519e34da4e24232bca0a2aeb55d40bed83e96364f5d907b1ad3a03342a4bcc374eacf0dc6e85dd86fa6639510a1b3f953"' :
                                            'id="xs-components-links-module-GrupoSegmentoModule-3f0e90d3d1d4a4d4ea1c940b1cb0b6a519e34da4e24232bca0a2aeb55d40bed83e96364f5d907b1ad3a03342a4bcc374eacf0dc6e85dd86fa6639510a1b3f953"' }>
                                            <li class="link">
                                                <a href="components/GrupoSegmentoComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >GrupoSegmentoComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/GrupoSegmentoRoutingModule.html" data-type="entity-link" >GrupoSegmentoRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/InputDateModule.html" data-type="entity-link" >InputDateModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-InputDateModule-43359bb719810d8b68b49c573975913f680021bf12fbe43b344b746f54426ab965c9889f32284c6b7fbcb2b43fa320531d0cfc2ebeea71e627d4952143d8e9c2"' : 'data-bs-target="#xs-components-links-module-InputDateModule-43359bb719810d8b68b49c573975913f680021bf12fbe43b344b746f54426ab965c9889f32284c6b7fbcb2b43fa320531d0cfc2ebeea71e627d4952143d8e9c2"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-InputDateModule-43359bb719810d8b68b49c573975913f680021bf12fbe43b344b746f54426ab965c9889f32284c6b7fbcb2b43fa320531d0cfc2ebeea71e627d4952143d8e9c2"' :
                                            'id="xs-components-links-module-InputDateModule-43359bb719810d8b68b49c573975913f680021bf12fbe43b344b746f54426ab965c9889f32284c6b7fbcb2b43fa320531d0cfc2ebeea71e627d4952143d8e9c2"' }>
                                            <li class="link">
                                                <a href="components/InputDateComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >InputDateComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#pipes-links-module-InputDateModule-43359bb719810d8b68b49c573975913f680021bf12fbe43b344b746f54426ab965c9889f32284c6b7fbcb2b43fa320531d0cfc2ebeea71e627d4952143d8e9c2"' : 'data-bs-target="#xs-pipes-links-module-InputDateModule-43359bb719810d8b68b49c573975913f680021bf12fbe43b344b746f54426ab965c9889f32284c6b7fbcb2b43fa320531d0cfc2ebeea71e627d4952143d8e9c2"' }>
                                            <span class="icon ion-md-add"></span>
                                            <span>Pipes</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="pipes-links-module-InputDateModule-43359bb719810d8b68b49c573975913f680021bf12fbe43b344b746f54426ab965c9889f32284c6b7fbcb2b43fa320531d0cfc2ebeea71e627d4952143d8e9c2"' :
                                            'id="xs-pipes-links-module-InputDateModule-43359bb719810d8b68b49c573975913f680021bf12fbe43b344b746f54426ab965c9889f32284c6b7fbcb2b43fa320531d0cfc2ebeea71e627d4952143d8e9c2"' }>
                                            <li class="link">
                                                <a href="pipes/FormControlPipe.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FormControlPipe</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/InputFornecedorModule.html" data-type="entity-link" >InputFornecedorModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-InputFornecedorModule-151a361e343bec599201dae686502ed4623625f05fc4e3389bcef80076607f2d959f86d129eca2e0bfd77eed387d7f0f37529544e63d2256ede8c09687d87c31"' : 'data-bs-target="#xs-components-links-module-InputFornecedorModule-151a361e343bec599201dae686502ed4623625f05fc4e3389bcef80076607f2d959f86d129eca2e0bfd77eed387d7f0f37529544e63d2256ede8c09687d87c31"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-InputFornecedorModule-151a361e343bec599201dae686502ed4623625f05fc4e3389bcef80076607f2d959f86d129eca2e0bfd77eed387d7f0f37529544e63d2256ede8c09687d87c31"' :
                                            'id="xs-components-links-module-InputFornecedorModule-151a361e343bec599201dae686502ed4623625f05fc4e3389bcef80076607f2d959f86d129eca2e0bfd77eed387d7f0f37529544e63d2256ede8c09687d87c31"' }>
                                            <li class="link">
                                                <a href="components/InputFornecedorComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >InputFornecedorComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#pipes-links-module-InputFornecedorModule-151a361e343bec599201dae686502ed4623625f05fc4e3389bcef80076607f2d959f86d129eca2e0bfd77eed387d7f0f37529544e63d2256ede8c09687d87c31"' : 'data-bs-target="#xs-pipes-links-module-InputFornecedorModule-151a361e343bec599201dae686502ed4623625f05fc4e3389bcef80076607f2d959f86d129eca2e0bfd77eed387d7f0f37529544e63d2256ede8c09687d87c31"' }>
                                            <span class="icon ion-md-add"></span>
                                            <span>Pipes</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="pipes-links-module-InputFornecedorModule-151a361e343bec599201dae686502ed4623625f05fc4e3389bcef80076607f2d959f86d129eca2e0bfd77eed387d7f0f37529544e63d2256ede8c09687d87c31"' :
                                            'id="xs-pipes-links-module-InputFornecedorModule-151a361e343bec599201dae686502ed4623625f05fc4e3389bcef80076607f2d959f86d129eca2e0bfd77eed387d7f0f37529544e63d2256ede8c09687d87c31"' }>
                                            <li class="link">
                                                <a href="pipes/FormControlPipe.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FormControlPipe</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/InputMedidaGroupModule.html" data-type="entity-link" >InputMedidaGroupModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-InputMedidaGroupModule-0380bb936c3a6f799161597206f29bda49f705a0a4fb3b826888c8e2ec7061b84a6a3abbc5bd7f281e4d584825ef796c3ed4f0b66ead41068d04f532b0789ee8"' : 'data-bs-target="#xs-components-links-module-InputMedidaGroupModule-0380bb936c3a6f799161597206f29bda49f705a0a4fb3b826888c8e2ec7061b84a6a3abbc5bd7f281e4d584825ef796c3ed4f0b66ead41068d04f532b0789ee8"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-InputMedidaGroupModule-0380bb936c3a6f799161597206f29bda49f705a0a4fb3b826888c8e2ec7061b84a6a3abbc5bd7f281e4d584825ef796c3ed4f0b66ead41068d04f532b0789ee8"' :
                                            'id="xs-components-links-module-InputMedidaGroupModule-0380bb936c3a6f799161597206f29bda49f705a0a4fb3b826888c8e2ec7061b84a6a3abbc5bd7f281e4d584825ef796c3ed4f0b66ead41068d04f532b0789ee8"' }>
                                            <li class="link">
                                                <a href="components/InputMedidaComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >InputMedidaComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/InputMedidaGroupComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >InputMedidaGroupComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/InputModule.html" data-type="entity-link" >InputModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-InputModule-211b8ff928478c0d01794b352c6d8933d6337f25650e43e48597697374d3a6cf45b7a3b48f4d92f58d713ec6e810877f42ae233e3b122bebee53124c71dfb57b"' : 'data-bs-target="#xs-components-links-module-InputModule-211b8ff928478c0d01794b352c6d8933d6337f25650e43e48597697374d3a6cf45b7a3b48f4d92f58d713ec6e810877f42ae233e3b122bebee53124c71dfb57b"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-InputModule-211b8ff928478c0d01794b352c6d8933d6337f25650e43e48597697374d3a6cf45b7a3b48f4d92f58d713ec6e810877f42ae233e3b122bebee53124c71dfb57b"' :
                                            'id="xs-components-links-module-InputModule-211b8ff928478c0d01794b352c6d8933d6337f25650e43e48597697374d3a6cf45b7a3b48f4d92f58d713ec6e810877f42ae233e3b122bebee53124c71dfb57b"' }>
                                            <li class="link">
                                                <a href="components/InputComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >InputComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#pipes-links-module-InputModule-211b8ff928478c0d01794b352c6d8933d6337f25650e43e48597697374d3a6cf45b7a3b48f4d92f58d713ec6e810877f42ae233e3b122bebee53124c71dfb57b"' : 'data-bs-target="#xs-pipes-links-module-InputModule-211b8ff928478c0d01794b352c6d8933d6337f25650e43e48597697374d3a6cf45b7a3b48f4d92f58d713ec6e810877f42ae233e3b122bebee53124c71dfb57b"' }>
                                            <span class="icon ion-md-add"></span>
                                            <span>Pipes</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="pipes-links-module-InputModule-211b8ff928478c0d01794b352c6d8933d6337f25650e43e48597697374d3a6cf45b7a3b48f4d92f58d713ec6e810877f42ae233e3b122bebee53124c71dfb57b"' :
                                            'id="xs-pipes-links-module-InputModule-211b8ff928478c0d01794b352c6d8933d6337f25650e43e48597697374d3a6cf45b7a3b48f4d92f58d713ec6e810877f42ae233e3b122bebee53124c71dfb57b"' }>
                                            <li class="link">
                                                <a href="pipes/FormControlPipe.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FormControlPipe</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ItemEstoqueModule.html" data-type="entity-link" >ItemEstoqueModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-ItemEstoqueModule-92655e61ada82dbb93972a24ca7f648daf1b5cdad7521588498f37256a1b0f0f65f95ab1804aa76b91adc189ab309e51b125f4b981ca3b0bdfa6ea9556f85348"' : 'data-bs-target="#xs-components-links-module-ItemEstoqueModule-92655e61ada82dbb93972a24ca7f648daf1b5cdad7521588498f37256a1b0f0f65f95ab1804aa76b91adc189ab309e51b125f4b981ca3b0bdfa6ea9556f85348"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ItemEstoqueModule-92655e61ada82dbb93972a24ca7f648daf1b5cdad7521588498f37256a1b0f0f65f95ab1804aa76b91adc189ab309e51b125f4b981ca3b0bdfa6ea9556f85348"' :
                                            'id="xs-components-links-module-ItemEstoqueModule-92655e61ada82dbb93972a24ca7f648daf1b5cdad7521588498f37256a1b0f0f65f95ab1804aa76b91adc189ab309e51b125f4b981ca3b0bdfa6ea9556f85348"' }>
                                            <li class="link">
                                                <a href="components/TemplateComponent-1.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >TemplateComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ItemEstoqueRoutingModule.html" data-type="entity-link" >ItemEstoqueRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/LoginModule.html" data-type="entity-link" >LoginModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-LoginModule-05f90bad78ba10568c83bb9b8b7fb0f44629ca1e7de2aa131290080243f1c055ecd02a97410929f7c58e79fd43d9a872fa17fc65230f1e25bcfcb6ad72944758"' : 'data-bs-target="#xs-components-links-module-LoginModule-05f90bad78ba10568c83bb9b8b7fb0f44629ca1e7de2aa131290080243f1c055ecd02a97410929f7c58e79fd43d9a872fa17fc65230f1e25bcfcb6ad72944758"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-LoginModule-05f90bad78ba10568c83bb9b8b7fb0f44629ca1e7de2aa131290080243f1c055ecd02a97410929f7c58e79fd43d9a872fa17fc65230f1e25bcfcb6ad72944758"' :
                                            'id="xs-components-links-module-LoginModule-05f90bad78ba10568c83bb9b8b7fb0f44629ca1e7de2aa131290080243f1c055ecd02a97410929f7c58e79fd43d9a872fa17fc65230f1e25bcfcb6ad72944758"' }>
                                            <li class="link">
                                                <a href="components/LoginComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >LoginComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/LoginRoutingModule.html" data-type="entity-link" >LoginRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/LoteRastreabilidadeModule.html" data-type="entity-link" >LoteRastreabilidadeModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-LoteRastreabilidadeModule-f3a28a4bce1a73054ed34ecc7cdd47a1992747eebd61dc2ba15fd41e435208c20c47ad8babf17d35f5775db7713cc22be087d4a4a4ce4f95805c82c9c0ab321a"' : 'data-bs-target="#xs-components-links-module-LoteRastreabilidadeModule-f3a28a4bce1a73054ed34ecc7cdd47a1992747eebd61dc2ba15fd41e435208c20c47ad8babf17d35f5775db7713cc22be087d4a4a4ce4f95805c82c9c0ab321a"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-LoteRastreabilidadeModule-f3a28a4bce1a73054ed34ecc7cdd47a1992747eebd61dc2ba15fd41e435208c20c47ad8babf17d35f5775db7713cc22be087d4a4a4ce4f95805c82c9c0ab321a"' :
                                            'id="xs-components-links-module-LoteRastreabilidadeModule-f3a28a4bce1a73054ed34ecc7cdd47a1992747eebd61dc2ba15fd41e435208c20c47ad8babf17d35f5775db7713cc22be087d4a4a4ce4f95805c82c9c0ab321a"' }>
                                            <li class="link">
                                                <a href="components/TemplateComponent-2.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >TemplateComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/LoteRastreabilidadeRoutingModule.html" data-type="entity-link" >LoteRastreabilidadeRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/ManutencaoLoteModule.html" data-type="entity-link" >ManutencaoLoteModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-ManutencaoLoteModule-579051fb823404a605679ad2bce9d3194e57a85b22861f443aa688e13aeaa4513a7d4a7ed5c8b7b21aa20683541d5e43a7a66de4572c6a5b84b68837a4f834af"' : 'data-bs-target="#xs-components-links-module-ManutencaoLoteModule-579051fb823404a605679ad2bce9d3194e57a85b22861f443aa688e13aeaa4513a7d4a7ed5c8b7b21aa20683541d5e43a7a66de4572c6a5b84b68837a4f834af"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ManutencaoLoteModule-579051fb823404a605679ad2bce9d3194e57a85b22861f443aa688e13aeaa4513a7d4a7ed5c8b7b21aa20683541d5e43a7a66de4572c6a5b84b68837a4f834af"' :
                                            'id="xs-components-links-module-ManutencaoLoteModule-579051fb823404a605679ad2bce9d3194e57a85b22861f443aa688e13aeaa4513a7d4a7ed5c8b7b21aa20683541d5e43a7a66de4572c6a5b84b68837a4f834af"' }>
                                            <li class="link">
                                                <a href="components/TemplateComponent-3.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >TemplateComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ManutencaoLoteRoutingModule.html" data-type="entity-link" >ManutencaoLoteRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/MaterialMedidaModule.html" data-type="entity-link" >MaterialMedidaModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-MaterialMedidaModule-ae0844db6d1b6fb4d6c463bab3d736ae3f021f1be4bb3e5639e1aa7bedb0636abf047725e46efaec7c506f22771b365ebcc276bc3a92cd60d7285d30d12ce76b"' : 'data-bs-target="#xs-components-links-module-MaterialMedidaModule-ae0844db6d1b6fb4d6c463bab3d736ae3f021f1be4bb3e5639e1aa7bedb0636abf047725e46efaec7c506f22771b365ebcc276bc3a92cd60d7285d30d12ce76b"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-MaterialMedidaModule-ae0844db6d1b6fb4d6c463bab3d736ae3f021f1be4bb3e5639e1aa7bedb0636abf047725e46efaec7c506f22771b365ebcc276bc3a92cd60d7285d30d12ce76b"' :
                                            'id="xs-components-links-module-MaterialMedidaModule-ae0844db6d1b6fb4d6c463bab3d736ae3f021f1be4bb3e5639e1aa7bedb0636abf047725e46efaec7c506f22771b365ebcc276bc3a92cd60d7285d30d12ce76b"' }>
                                            <li class="link">
                                                <a href="components/MaterialMedidaComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >MaterialMedidaComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/MaterialMedidaRoutingModule.html" data-type="entity-link" >MaterialMedidaRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/MaterialModule.html" data-type="entity-link" >MaterialModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-MaterialModule-0c5e98a9a0f819b52a00a21b331d9ca408ff5198569536de5b4a0ae71956d9bac1267735dca4ccef7ff7458633bff0274af03754b29e72bf58b7631f41780ecc"' : 'data-bs-target="#xs-components-links-module-MaterialModule-0c5e98a9a0f819b52a00a21b331d9ca408ff5198569536de5b4a0ae71956d9bac1267735dca4ccef7ff7458633bff0274af03754b29e72bf58b7631f41780ecc"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-MaterialModule-0c5e98a9a0f819b52a00a21b331d9ca408ff5198569536de5b4a0ae71956d9bac1267735dca4ccef7ff7458633bff0274af03754b29e72bf58b7631f41780ecc"' :
                                            'id="xs-components-links-module-MaterialModule-0c5e98a9a0f819b52a00a21b331d9ca408ff5198569536de5b4a0ae71956d9bac1267735dca4ccef7ff7458633bff0274af03754b29e72bf58b7631f41780ecc"' }>
                                            <li class="link">
                                                <a href="components/MaterialComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >MaterialComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/MaterialRoutingModule.html" data-type="entity-link" >MaterialRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/ModalArquivoCertificadoModule.html" data-type="entity-link" >ModalArquivoCertificadoModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-ModalArquivoCertificadoModule-c8782a7f63a14e7696de5725666cd6496ac728127c6c9605d9cd6df83f29c33dad7dc6cbcada0e12438d3704c3429cafd0916d843aa82c212c525c19f0731e44"' : 'data-bs-target="#xs-components-links-module-ModalArquivoCertificadoModule-c8782a7f63a14e7696de5725666cd6496ac728127c6c9605d9cd6df83f29c33dad7dc6cbcada0e12438d3704c3429cafd0916d843aa82c212c525c19f0731e44"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ModalArquivoCertificadoModule-c8782a7f63a14e7696de5725666cd6496ac728127c6c9605d9cd6df83f29c33dad7dc6cbcada0e12438d3704c3429cafd0916d843aa82c212c525c19f0731e44"' :
                                            'id="xs-components-links-module-ModalArquivoCertificadoModule-c8782a7f63a14e7696de5725666cd6496ac728127c6c9605d9cd6df83f29c33dad7dc6cbcada0e12438d3704c3429cafd0916d843aa82c212c525c19f0731e44"' }>
                                            <li class="link">
                                                <a href="components/ModalArquivoCertificadoComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ModalArquivoCertificadoComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ModalBuscaFornecedorModule.html" data-type="entity-link" >ModalBuscaFornecedorModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-ModalBuscaFornecedorModule-cc98a024c9fdd8d3b0f906ba44131aa757f1da3662fa12b086a1cbef55762b6658d984d77a3b15f509e3f0b13b0a32a913f68388bdfea7fafcc71edb078ea5bd"' : 'data-bs-target="#xs-components-links-module-ModalBuscaFornecedorModule-cc98a024c9fdd8d3b0f906ba44131aa757f1da3662fa12b086a1cbef55762b6658d984d77a3b15f509e3f0b13b0a32a913f68388bdfea7fafcc71edb078ea5bd"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ModalBuscaFornecedorModule-cc98a024c9fdd8d3b0f906ba44131aa757f1da3662fa12b086a1cbef55762b6658d984d77a3b15f509e3f0b13b0a32a913f68388bdfea7fafcc71edb078ea5bd"' :
                                            'id="xs-components-links-module-ModalBuscaFornecedorModule-cc98a024c9fdd8d3b0f906ba44131aa757f1da3662fa12b086a1cbef55762b6658d984d77a3b15f509e3f0b13b0a32a913f68388bdfea7fafcc71edb078ea5bd"' }>
                                            <li class="link">
                                                <a href="components/ModalBuscaFornecedorComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ModalBuscaFornecedorComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ModalCortadasModule.html" data-type="entity-link" >ModalCortadasModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-ModalCortadasModule-f49eb1700228d56ac245db9835263fa2833cb7785b6b190ddcaa2a85876250f290a52723e7d9d01a6d626f7272678ac490ca7299f0613640fea743338b0e1fbf"' : 'data-bs-target="#xs-components-links-module-ModalCortadasModule-f49eb1700228d56ac245db9835263fa2833cb7785b6b190ddcaa2a85876250f290a52723e7d9d01a6d626f7272678ac490ca7299f0613640fea743338b0e1fbf"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ModalCortadasModule-f49eb1700228d56ac245db9835263fa2833cb7785b6b190ddcaa2a85876250f290a52723e7d9d01a6d626f7272678ac490ca7299f0613640fea743338b0e1fbf"' :
                                            'id="xs-components-links-module-ModalCortadasModule-f49eb1700228d56ac245db9835263fa2833cb7785b6b190ddcaa2a85876250f290a52723e7d9d01a6d626f7272678ac490ca7299f0613640fea743338b0e1fbf"' }>
                                            <li class="link">
                                                <a href="components/ModalCortadasComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ModalCortadasComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ModalEntradaItemPedidoModule.html" data-type="entity-link" >ModalEntradaItemPedidoModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-ModalEntradaItemPedidoModule-5a618d14bebe9fbc116400d346cc463bd98b6cb250035645521f6d6caf439699afa0ccf3718c5ae4cac2618089d92c411d2e6b7243b3e8ae7a5d51f832ea9637"' : 'data-bs-target="#xs-components-links-module-ModalEntradaItemPedidoModule-5a618d14bebe9fbc116400d346cc463bd98b6cb250035645521f6d6caf439699afa0ccf3718c5ae4cac2618089d92c411d2e6b7243b3e8ae7a5d51f832ea9637"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ModalEntradaItemPedidoModule-5a618d14bebe9fbc116400d346cc463bd98b6cb250035645521f6d6caf439699afa0ccf3718c5ae4cac2618089d92c411d2e6b7243b3e8ae7a5d51f832ea9637"' :
                                            'id="xs-components-links-module-ModalEntradaItemPedidoModule-5a618d14bebe9fbc116400d346cc463bd98b6cb250035645521f6d6caf439699afa0ccf3718c5ae4cac2618089d92c411d2e6b7243b3e8ae7a5d51f832ea9637"' }>
                                            <li class="link">
                                                <a href="components/ModalEntradaItemPedidoComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ModalEntradaItemPedidoComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ModalLotesModule.html" data-type="entity-link" >ModalLotesModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-ModalLotesModule-d2294ccb9d62f6dd77b3190f75fda9b10aef8a84898c7d8923fa7f678975918221a7fa80c91b11d92a6d1928debd72ae34b9cd66019df63880c3867e2d51aece"' : 'data-bs-target="#xs-components-links-module-ModalLotesModule-d2294ccb9d62f6dd77b3190f75fda9b10aef8a84898c7d8923fa7f678975918221a7fa80c91b11d92a6d1928debd72ae34b9cd66019df63880c3867e2d51aece"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ModalLotesModule-d2294ccb9d62f6dd77b3190f75fda9b10aef8a84898c7d8923fa7f678975918221a7fa80c91b11d92a6d1928debd72ae34b9cd66019df63880c3867e2d51aece"' :
                                            'id="xs-components-links-module-ModalLotesModule-d2294ccb9d62f6dd77b3190f75fda9b10aef8a84898c7d8923fa7f678975918221a7fa80c91b11d92a6d1928debd72ae34b9cd66019df63880c3867e2d51aece"' }>
                                            <li class="link">
                                                <a href="components/ModalLotesComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ModalLotesComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ModalOrdemCompraModule.html" data-type="entity-link" >ModalOrdemCompraModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-ModalOrdemCompraModule-cdeb5ea36100d3b92e2a6cd907f525041c4d2117d3171d276cad09ba9f97ebf217e5631b6e66d2082f73ef8fbe0b7d1c0882a50d476c4e71198de9bff384c7bb"' : 'data-bs-target="#xs-components-links-module-ModalOrdemCompraModule-cdeb5ea36100d3b92e2a6cd907f525041c4d2117d3171d276cad09ba9f97ebf217e5631b6e66d2082f73ef8fbe0b7d1c0882a50d476c4e71198de9bff384c7bb"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ModalOrdemCompraModule-cdeb5ea36100d3b92e2a6cd907f525041c4d2117d3171d276cad09ba9f97ebf217e5631b6e66d2082f73ef8fbe0b7d1c0882a50d476c4e71198de9bff384c7bb"' :
                                            'id="xs-components-links-module-ModalOrdemCompraModule-cdeb5ea36100d3b92e2a6cd907f525041c4d2117d3171d276cad09ba9f97ebf217e5631b6e66d2082f73ef8fbe0b7d1c0882a50d476c4e71198de9bff384c7bb"' }>
                                            <li class="link">
                                                <a href="components/ModalOrdemCompraComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ModalOrdemCompraComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/MovimentacaoEntradaModule.html" data-type="entity-link" >MovimentacaoEntradaModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-MovimentacaoEntradaModule-95699e9fafde1bb598a91faff4448a00c558bed0280b54a40a3c78fbcef6f3e867e781255bfd330cf44cf5aa8bb8fa59a2181b4930f8fd2f5d17574f7445d09e"' : 'data-bs-target="#xs-components-links-module-MovimentacaoEntradaModule-95699e9fafde1bb598a91faff4448a00c558bed0280b54a40a3c78fbcef6f3e867e781255bfd330cf44cf5aa8bb8fa59a2181b4930f8fd2f5d17574f7445d09e"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-MovimentacaoEntradaModule-95699e9fafde1bb598a91faff4448a00c558bed0280b54a40a3c78fbcef6f3e867e781255bfd330cf44cf5aa8bb8fa59a2181b4930f8fd2f5d17574f7445d09e"' :
                                            'id="xs-components-links-module-MovimentacaoEntradaModule-95699e9fafde1bb598a91faff4448a00c558bed0280b54a40a3c78fbcef6f3e867e781255bfd330cf44cf5aa8bb8fa59a2181b4930f8fd2f5d17574f7445d09e"' }>
                                            <li class="link">
                                                <a href="components/MovimentacaoEntradaComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >MovimentacaoEntradaComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/PecaInteiraComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >PecaInteiraComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/MovimentacaoModule.html" data-type="entity-link" >MovimentacaoModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-MovimentacaoModule-2d5c29781bd03a3fb3330dce1977874b9ab758a2a860133c1b0102082e029f4967c80b40bcb8f3752e479d01cb3fd100234dfc2dac229162a43d2e2d0fff6687"' : 'data-bs-target="#xs-components-links-module-MovimentacaoModule-2d5c29781bd03a3fb3330dce1977874b9ab758a2a860133c1b0102082e029f4967c80b40bcb8f3752e479d01cb3fd100234dfc2dac229162a43d2e2d0fff6687"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-MovimentacaoModule-2d5c29781bd03a3fb3330dce1977874b9ab758a2a860133c1b0102082e029f4967c80b40bcb8f3752e479d01cb3fd100234dfc2dac229162a43d2e2d0fff6687"' :
                                            'id="xs-components-links-module-MovimentacaoModule-2d5c29781bd03a3fb3330dce1977874b9ab758a2a860133c1b0102082e029f4967c80b40bcb8f3752e479d01cb3fd100234dfc2dac229162a43d2e2d0fff6687"' }>
                                            <li class="link">
                                                <a href="components/TemplateComponent-4.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >TemplateComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/MovimentacaoRoutingModule.html" data-type="entity-link" >MovimentacaoRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/MovimentacaoSaidaModule.html" data-type="entity-link" >MovimentacaoSaidaModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-MovimentacaoSaidaModule-09f545478cc797ae56301e5dacbc68950131b30efe478e00335178bfb1dac47af3b1c8bd75e769c4de73add61ed987f1b8fd0a3004d7ba4eeae7e658d101a2a7"' : 'data-bs-target="#xs-components-links-module-MovimentacaoSaidaModule-09f545478cc797ae56301e5dacbc68950131b30efe478e00335178bfb1dac47af3b1c8bd75e769c4de73add61ed987f1b8fd0a3004d7ba4eeae7e658d101a2a7"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-MovimentacaoSaidaModule-09f545478cc797ae56301e5dacbc68950131b30efe478e00335178bfb1dac47af3b1c8bd75e769c4de73add61ed987f1b8fd0a3004d7ba4eeae7e658d101a2a7"' :
                                            'id="xs-components-links-module-MovimentacaoSaidaModule-09f545478cc797ae56301e5dacbc68950131b30efe478e00335178bfb1dac47af3b1c8bd75e769c4de73add61ed987f1b8fd0a3004d7ba4eeae7e658d101a2a7"' }>
                                            <li class="link">
                                                <a href="components/MovimentacaoSaidaComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >MovimentacaoSaidaComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/PagesModule.html" data-type="entity-link" >PagesModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-PagesModule-053705d1c4400f6a28d53108f9c9166457100b7904db9b0a47d928f564438f5d95576b0a45b27e5173a61609edd80776a25ce9aca545c71cdc7826aa53eda077"' : 'data-bs-target="#xs-components-links-module-PagesModule-053705d1c4400f6a28d53108f9c9166457100b7904db9b0a47d928f564438f5d95576b0a45b27e5173a61609edd80776a25ce9aca545c71cdc7826aa53eda077"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-PagesModule-053705d1c4400f6a28d53108f9c9166457100b7904db9b0a47d928f564438f5d95576b0a45b27e5173a61609edd80776a25ce9aca545c71cdc7826aa53eda077"' :
                                            'id="xs-components-links-module-PagesModule-053705d1c4400f6a28d53108f9c9166457100b7904db9b0a47d928f564438f5d95576b0a45b27e5173a61609edd80776a25ce9aca545c71cdc7826aa53eda077"' }>
                                            <li class="link">
                                                <a href="components/PagesComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >PagesComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/PagesRoutingModule.html" data-type="entity-link" >PagesRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/PecaCortadaModule.html" data-type="entity-link" >PecaCortadaModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-PecaCortadaModule-e468396f0cef32d00e08bae60a116591bf20028dce9cfcde3daf4eb87c6139ddf85de5de39b046a32e38d4f6c1a51f1cb5a8d78f1db290391f47fd34ee9a8c2f"' : 'data-bs-target="#xs-components-links-module-PecaCortadaModule-e468396f0cef32d00e08bae60a116591bf20028dce9cfcde3daf4eb87c6139ddf85de5de39b046a32e38d4f6c1a51f1cb5a8d78f1db290391f47fd34ee9a8c2f"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-PecaCortadaModule-e468396f0cef32d00e08bae60a116591bf20028dce9cfcde3daf4eb87c6139ddf85de5de39b046a32e38d4f6c1a51f1cb5a8d78f1db290391f47fd34ee9a8c2f"' :
                                            'id="xs-components-links-module-PecaCortadaModule-e468396f0cef32d00e08bae60a116591bf20028dce9cfcde3daf4eb87c6139ddf85de5de39b046a32e38d4f6c1a51f1cb5a8d78f1db290391f47fd34ee9a8c2f"' }>
                                            <li class="link">
                                                <a href="components/PecaCortadaComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >PecaCortadaComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/TabelaPecasCortadasEntradaComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >TabelaPecasCortadasEntradaComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/PecaCortadaModule.html" data-type="entity-link" >PecaCortadaModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-PecaCortadaModule-a8c60180c0fa1a50d68addba165ba8b1cd1318fa9c4aded9aef2c65567358bec82882f73036feec9d0cd299f65c3e82348ed5fbd3fa6d581378ad86188aa5572-1"' : 'data-bs-target="#xs-components-links-module-PecaCortadaModule-a8c60180c0fa1a50d68addba165ba8b1cd1318fa9c4aded9aef2c65567358bec82882f73036feec9d0cd299f65c3e82348ed5fbd3fa6d581378ad86188aa5572-1"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-PecaCortadaModule-a8c60180c0fa1a50d68addba165ba8b1cd1318fa9c4aded9aef2c65567358bec82882f73036feec9d0cd299f65c3e82348ed5fbd3fa6d581378ad86188aa5572-1"' :
                                            'id="xs-components-links-module-PecaCortadaModule-a8c60180c0fa1a50d68addba165ba8b1cd1318fa9c4aded9aef2c65567358bec82882f73036feec9d0cd299f65c3e82348ed5fbd3fa6d581378ad86188aa5572-1"' }>
                                            <li class="link">
                                                <a href="components/PecaCortadaComponent-1.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >PecaCortadaComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/TabelaPecasCortadasComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >TabelaPecasCortadasComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/TabelaPecasCortadasSaidaComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >TabelaPecasCortadasSaidaComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/PecaInteiraModule.html" data-type="entity-link" >PecaInteiraModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/PecaInteiraModule.html" data-type="entity-link" >PecaInteiraModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-PecaInteiraModule-0a0a3ae23171cdb8c20169ad71944fe3ffcdedd41f26dcae734bf7c196d9bcd7d9493ece71ef0ced262c7289a000b2f25fb5df1223178e8fde91f7ed852de9ba-1"' : 'data-bs-target="#xs-components-links-module-PecaInteiraModule-0a0a3ae23171cdb8c20169ad71944fe3ffcdedd41f26dcae734bf7c196d9bcd7d9493ece71ef0ced262c7289a000b2f25fb5df1223178e8fde91f7ed852de9ba-1"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-PecaInteiraModule-0a0a3ae23171cdb8c20169ad71944fe3ffcdedd41f26dcae734bf7c196d9bcd7d9493ece71ef0ced262c7289a000b2f25fb5df1223178e8fde91f7ed852de9ba-1"' :
                                            'id="xs-components-links-module-PecaInteiraModule-0a0a3ae23171cdb8c20169ad71944fe3ffcdedd41f26dcae734bf7c196d9bcd7d9493ece71ef0ced262c7289a000b2f25fb5df1223178e8fde91f7ed852de9ba-1"' }>
                                            <li class="link">
                                                <a href="components/PecaInteiraComponent-1.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >PecaInteiraComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/PedidoCompraModule.html" data-type="entity-link" >PedidoCompraModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-PedidoCompraModule-eff060c24ebf7eee16dcee0b24817f76e29eb06a1950bb70dbfe59fe39768983d59b81d67ae0eecf66cf18bef11720385bf9f5f0c33e2211f13616c26463ebb8"' : 'data-bs-target="#xs-components-links-module-PedidoCompraModule-eff060c24ebf7eee16dcee0b24817f76e29eb06a1950bb70dbfe59fe39768983d59b81d67ae0eecf66cf18bef11720385bf9f5f0c33e2211f13616c26463ebb8"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-PedidoCompraModule-eff060c24ebf7eee16dcee0b24817f76e29eb06a1950bb70dbfe59fe39768983d59b81d67ae0eecf66cf18bef11720385bf9f5f0c33e2211f13616c26463ebb8"' :
                                            'id="xs-components-links-module-PedidoCompraModule-eff060c24ebf7eee16dcee0b24817f76e29eb06a1950bb70dbfe59fe39768983d59b81d67ae0eecf66cf18bef11720385bf9f5f0c33e2211f13616c26463ebb8"' }>
                                            <li class="link">
                                                <a href="components/TemplateComponent-5.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >TemplateComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/PedidoCompraRoutingModule.html" data-type="entity-link" >PedidoCompraRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/PercentualBaixaBlocoModule.html" data-type="entity-link" >PercentualBaixaBlocoModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-PercentualBaixaBlocoModule-b611560a5020437826051799b345d8a9600be40448f0fe93877b2e8f43a0a90a7f3d6b1f2b40a90dd9a2f9af23d8549293b92e5ab70f7b0fa67a4b4ad3d5baee"' : 'data-bs-target="#xs-components-links-module-PercentualBaixaBlocoModule-b611560a5020437826051799b345d8a9600be40448f0fe93877b2e8f43a0a90a7f3d6b1f2b40a90dd9a2f9af23d8549293b92e5ab70f7b0fa67a4b4ad3d5baee"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-PercentualBaixaBlocoModule-b611560a5020437826051799b345d8a9600be40448f0fe93877b2e8f43a0a90a7f3d6b1f2b40a90dd9a2f9af23d8549293b92e5ab70f7b0fa67a4b4ad3d5baee"' :
                                            'id="xs-components-links-module-PercentualBaixaBlocoModule-b611560a5020437826051799b345d8a9600be40448f0fe93877b2e8f43a0a90a7f3d6b1f2b40a90dd9a2f9af23d8549293b92e5ab70f7b0fa67a4b4ad3d5baee"' }>
                                            <li class="link">
                                                <a href="components/PercentualBaixaBlocoComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >PercentualBaixaBlocoComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/PercentualBaixaBlocoRoutingModule.html" data-type="entity-link" >PercentualBaixaBlocoRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/PesoBaixaBlocoModule.html" data-type="entity-link" >PesoBaixaBlocoModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-PesoBaixaBlocoModule-0946b43117c6a7aad434b2ceaefc42dadc1e2f6f9107a19d35a908fb7524a33daaadf13dab9a058fb0abde864087cfa02663cffc610148d5abf6bc57f7e17f05"' : 'data-bs-target="#xs-components-links-module-PesoBaixaBlocoModule-0946b43117c6a7aad434b2ceaefc42dadc1e2f6f9107a19d35a908fb7524a33daaadf13dab9a058fb0abde864087cfa02663cffc610148d5abf6bc57f7e17f05"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-PesoBaixaBlocoModule-0946b43117c6a7aad434b2ceaefc42dadc1e2f6f9107a19d35a908fb7524a33daaadf13dab9a058fb0abde864087cfa02663cffc610148d5abf6bc57f7e17f05"' :
                                            'id="xs-components-links-module-PesoBaixaBlocoModule-0946b43117c6a7aad434b2ceaefc42dadc1e2f6f9107a19d35a908fb7524a33daaadf13dab9a058fb0abde864087cfa02663cffc610148d5abf6bc57f7e17f05"' }>
                                            <li class="link">
                                                <a href="components/PesoBaixaBlocoComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >PesoBaixaBlocoComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/PesoBaixaBlocoRoutingModule.html" data-type="entity-link" >PesoBaixaBlocoRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/PrecoPraticadoEstoqueModule.html" data-type="entity-link" >PrecoPraticadoEstoqueModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-PrecoPraticadoEstoqueModule-4ea424b8e30c65b9f740c41e85ffc634e65d4ce750f8445f5a5cbd58b8663cdeaed250672ed7279b88f6444fdcdd2e638b6f9e68436c6f8a871509140c319750"' : 'data-bs-target="#xs-components-links-module-PrecoPraticadoEstoqueModule-4ea424b8e30c65b9f740c41e85ffc634e65d4ce750f8445f5a5cbd58b8663cdeaed250672ed7279b88f6444fdcdd2e638b6f9e68436c6f8a871509140c319750"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-PrecoPraticadoEstoqueModule-4ea424b8e30c65b9f740c41e85ffc634e65d4ce750f8445f5a5cbd58b8663cdeaed250672ed7279b88f6444fdcdd2e638b6f9e68436c6f8a871509140c319750"' :
                                            'id="xs-components-links-module-PrecoPraticadoEstoqueModule-4ea424b8e30c65b9f740c41e85ffc634e65d4ce750f8445f5a5cbd58b8663cdeaed250672ed7279b88f6444fdcdd2e638b6f9e68436c6f8a871509140c319750"' }>
                                            <li class="link">
                                                <a href="components/TemplateComponent-6.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >TemplateComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/PrecoPraticadoEstoqueRoutingModule.html" data-type="entity-link" >PrecoPraticadoEstoqueRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/PrecosColunasModule.html" data-type="entity-link" >PrecosColunasModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-PrecosColunasModule-2fa922a7baaf07653daf57773c67df680ab154da9cbfcf76fef777070ce6734bdb63a625e5b07d5d634a3ba9c8dbb3042178a815fb9d2dbe70f711131eab7554"' : 'data-bs-target="#xs-components-links-module-PrecosColunasModule-2fa922a7baaf07653daf57773c67df680ab154da9cbfcf76fef777070ce6734bdb63a625e5b07d5d634a3ba9c8dbb3042178a815fb9d2dbe70f711131eab7554"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-PrecosColunasModule-2fa922a7baaf07653daf57773c67df680ab154da9cbfcf76fef777070ce6734bdb63a625e5b07d5d634a3ba9c8dbb3042178a815fb9d2dbe70f711131eab7554"' :
                                            'id="xs-components-links-module-PrecosColunasModule-2fa922a7baaf07653daf57773c67df680ab154da9cbfcf76fef777070ce6734bdb63a625e5b07d5d634a3ba9c8dbb3042178a815fb9d2dbe70f711131eab7554"' }>
                                            <li class="link">
                                                <a href="components/PrecosColunasComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >PrecosColunasComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/PrecosColunasRoutingModule.html" data-type="entity-link" >PrecosColunasRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/RecebimentoMaterialModule.html" data-type="entity-link" >RecebimentoMaterialModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-RecebimentoMaterialModule-ea1595dfb392c0415d97c00d87f334f7a3bf392082386187806f1311f284f7cd3306d408ff98ea3a206e42213f1a714ac0530b25fc92cd247b1e3aa9ade81ef3"' : 'data-bs-target="#xs-components-links-module-RecebimentoMaterialModule-ea1595dfb392c0415d97c00d87f334f7a3bf392082386187806f1311f284f7cd3306d408ff98ea3a206e42213f1a714ac0530b25fc92cd247b1e3aa9ade81ef3"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-RecebimentoMaterialModule-ea1595dfb392c0415d97c00d87f334f7a3bf392082386187806f1311f284f7cd3306d408ff98ea3a206e42213f1a714ac0530b25fc92cd247b1e3aa9ade81ef3"' :
                                            'id="xs-components-links-module-RecebimentoMaterialModule-ea1595dfb392c0415d97c00d87f334f7a3bf392082386187806f1311f284f7cd3306d408ff98ea3a206e42213f1a714ac0530b25fc92cd247b1e3aa9ade81ef3"' }>
                                            <li class="link">
                                                <a href="components/TemplateComponent-7.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >TemplateComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/RecebimentoMaterialRoutingModule.html" data-type="entity-link" >RecebimentoMaterialRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/RelatorioEntradaMaterialSegmentoModule.html" data-type="entity-link" >RelatorioEntradaMaterialSegmentoModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-RelatorioEntradaMaterialSegmentoModule-4f9f0af37cc153ee2e853d888e9ea63207b562a022979801763e419364f140b8fb46cdf597579d48846984d4b4b85e8d7970825d8b8176d28e62ecd1b99bf654"' : 'data-bs-target="#xs-components-links-module-RelatorioEntradaMaterialSegmentoModule-4f9f0af37cc153ee2e853d888e9ea63207b562a022979801763e419364f140b8fb46cdf597579d48846984d4b4b85e8d7970825d8b8176d28e62ecd1b99bf654"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-RelatorioEntradaMaterialSegmentoModule-4f9f0af37cc153ee2e853d888e9ea63207b562a022979801763e419364f140b8fb46cdf597579d48846984d4b4b85e8d7970825d8b8176d28e62ecd1b99bf654"' :
                                            'id="xs-components-links-module-RelatorioEntradaMaterialSegmentoModule-4f9f0af37cc153ee2e853d888e9ea63207b562a022979801763e419364f140b8fb46cdf597579d48846984d4b4b85e8d7970825d8b8176d28e62ecd1b99bf654"' }>
                                            <li class="link">
                                                <a href="components/RelatorioEntradaMaterialSegmentoComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >RelatorioEntradaMaterialSegmentoComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/RelatorioInformacoesItemEstoqueModule.html" data-type="entity-link" >RelatorioInformacoesItemEstoqueModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-RelatorioInformacoesItemEstoqueModule-495edbcc50664d73c62594dbefa5aeedd88d676265c21c97ba8ed22ee67660bf1d36049876b8d65bca9781cb2dfc57195afe4ddb49e214c0be0e167689b1acbc"' : 'data-bs-target="#xs-components-links-module-RelatorioInformacoesItemEstoqueModule-495edbcc50664d73c62594dbefa5aeedd88d676265c21c97ba8ed22ee67660bf1d36049876b8d65bca9781cb2dfc57195afe4ddb49e214c0be0e167689b1acbc"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-RelatorioInformacoesItemEstoqueModule-495edbcc50664d73c62594dbefa5aeedd88d676265c21c97ba8ed22ee67660bf1d36049876b8d65bca9781cb2dfc57195afe4ddb49e214c0be0e167689b1acbc"' :
                                            'id="xs-components-links-module-RelatorioInformacoesItemEstoqueModule-495edbcc50664d73c62594dbefa5aeedd88d676265c21c97ba8ed22ee67660bf1d36049876b8d65bca9781cb2dfc57195afe4ddb49e214c0be0e167689b1acbc"' }>
                                            <li class="link">
                                                <a href="components/RelatorioInformacoesItemEstoqueComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >RelatorioInformacoesItemEstoqueComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/RelatorioInventarioIMEModule.html" data-type="entity-link" >RelatorioInventarioIMEModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-RelatorioInventarioIMEModule-5ffc65c2bc04f7a667a838d8f097358e193c195f1c96d580cd31051599b27ccc3b25f7be371719ca56029a9736bef93f91d1c53a8d93275b33744d7b275e4490"' : 'data-bs-target="#xs-components-links-module-RelatorioInventarioIMEModule-5ffc65c2bc04f7a667a838d8f097358e193c195f1c96d580cd31051599b27ccc3b25f7be371719ca56029a9736bef93f91d1c53a8d93275b33744d7b275e4490"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-RelatorioInventarioIMEModule-5ffc65c2bc04f7a667a838d8f097358e193c195f1c96d580cd31051599b27ccc3b25f7be371719ca56029a9736bef93f91d1c53a8d93275b33744d7b275e4490"' :
                                            'id="xs-components-links-module-RelatorioInventarioIMEModule-5ffc65c2bc04f7a667a838d8f097358e193c195f1c96d580cd31051599b27ccc3b25f7be371719ca56029a9736bef93f91d1c53a8d93275b33744d7b275e4490"' }>
                                            <li class="link">
                                                <a href="components/RelatorioInventarioIMEComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >RelatorioInventarioIMEComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/RelatorioItemEstoqueCodigoModule.html" data-type="entity-link" >RelatorioItemEstoqueCodigoModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-RelatorioItemEstoqueCodigoModule-a320bb69dd35dd422f6f02270233ade43cffb16cc5d77e80e199fc4a023577391d3f43361be3d368b75dec1276e0ac9cb6914fe29dc9638eaba1207cad67989e"' : 'data-bs-target="#xs-components-links-module-RelatorioItemEstoqueCodigoModule-a320bb69dd35dd422f6f02270233ade43cffb16cc5d77e80e199fc4a023577391d3f43361be3d368b75dec1276e0ac9cb6914fe29dc9638eaba1207cad67989e"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-RelatorioItemEstoqueCodigoModule-a320bb69dd35dd422f6f02270233ade43cffb16cc5d77e80e199fc4a023577391d3f43361be3d368b75dec1276e0ac9cb6914fe29dc9638eaba1207cad67989e"' :
                                            'id="xs-components-links-module-RelatorioItemEstoqueCodigoModule-a320bb69dd35dd422f6f02270233ade43cffb16cc5d77e80e199fc4a023577391d3f43361be3d368b75dec1276e0ac9cb6914fe29dc9638eaba1207cad67989e"' }>
                                            <li class="link">
                                                <a href="components/RelatorioItemEstoqueCodigoComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >RelatorioItemEstoqueCodigoComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/RelatorioItemEstoqueModule.html" data-type="entity-link" >RelatorioItemEstoqueModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-RelatorioItemEstoqueModule-5576f72279533f74cc4a509312fed385eb2fae61dc22da6bee01899fb88e20de0abe73e702ad09ea4ea854e8223ff8ba125250bb5a0fc1ad2a476c06f521421a"' : 'data-bs-target="#xs-components-links-module-RelatorioItemEstoqueModule-5576f72279533f74cc4a509312fed385eb2fae61dc22da6bee01899fb88e20de0abe73e702ad09ea4ea854e8223ff8ba125250bb5a0fc1ad2a476c06f521421a"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-RelatorioItemEstoqueModule-5576f72279533f74cc4a509312fed385eb2fae61dc22da6bee01899fb88e20de0abe73e702ad09ea4ea854e8223ff8ba125250bb5a0fc1ad2a476c06f521421a"' :
                                            'id="xs-components-links-module-RelatorioItemEstoqueModule-5576f72279533f74cc4a509312fed385eb2fae61dc22da6bee01899fb88e20de0abe73e702ad09ea4ea854e8223ff8ba125250bb5a0fc1ad2a476c06f521421a"' }>
                                            <li class="link">
                                                <a href="components/RelatorioItemEstoqueComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >RelatorioItemEstoqueComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/RelatorioItemEstoqueQtdMinModule.html" data-type="entity-link" >RelatorioItemEstoqueQtdMinModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-RelatorioItemEstoqueQtdMinModule-0a6d2426c3177ff82e7073c7a48eecbaba5c8cb1796ee9c5c6ebdbaadd92822dee639d414c40fc3ab2c9ce395fb4011e74ab22ff1f2f9592172e95dad40b5b8b"' : 'data-bs-target="#xs-components-links-module-RelatorioItemEstoqueQtdMinModule-0a6d2426c3177ff82e7073c7a48eecbaba5c8cb1796ee9c5c6ebdbaadd92822dee639d414c40fc3ab2c9ce395fb4011e74ab22ff1f2f9592172e95dad40b5b8b"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-RelatorioItemEstoqueQtdMinModule-0a6d2426c3177ff82e7073c7a48eecbaba5c8cb1796ee9c5c6ebdbaadd92822dee639d414c40fc3ab2c9ce395fb4011e74ab22ff1f2f9592172e95dad40b5b8b"' :
                                            'id="xs-components-links-module-RelatorioItemEstoqueQtdMinModule-0a6d2426c3177ff82e7073c7a48eecbaba5c8cb1796ee9c5c6ebdbaadd92822dee639d414c40fc3ab2c9ce395fb4011e74ab22ff1f2f9592172e95dad40b5b8b"' }>
                                            <li class="link">
                                                <a href="components/RelatorioItemEstoqueQtdMinComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >RelatorioItemEstoqueQtdMinComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/RelatorioItemPedidoCompraModule.html" data-type="entity-link" >RelatorioItemPedidoCompraModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-RelatorioItemPedidoCompraModule-02e8ec10d199fc795d101db225cd837f6b22e4b928e05f7c8e30e8c0008c11239eb315ea57f4c20b8190052a989bd202763c7e43545b5b0e20e40ab8325abf3c"' : 'data-bs-target="#xs-components-links-module-RelatorioItemPedidoCompraModule-02e8ec10d199fc795d101db225cd837f6b22e4b928e05f7c8e30e8c0008c11239eb315ea57f4c20b8190052a989bd202763c7e43545b5b0e20e40ab8325abf3c"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-RelatorioItemPedidoCompraModule-02e8ec10d199fc795d101db225cd837f6b22e4b928e05f7c8e30e8c0008c11239eb315ea57f4c20b8190052a989bd202763c7e43545b5b0e20e40ab8325abf3c"' :
                                            'id="xs-components-links-module-RelatorioItemPedidoCompraModule-02e8ec10d199fc795d101db225cd837f6b22e4b928e05f7c8e30e8c0008c11239eb315ea57f4c20b8190052a989bd202763c7e43545b5b0e20e40ab8325abf3c"' }>
                                            <li class="link">
                                                <a href="components/RelatorioItemPedidoCompraComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >RelatorioItemPedidoCompraComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/RelatorioMarcacaoLotesModule.html" data-type="entity-link" >RelatorioMarcacaoLotesModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-RelatorioMarcacaoLotesModule-385ef91fca96b3b16a669a4c5aacea5fce4a10dfbc96fe480ea356c020fdbb5bcf3930ae3236f22935e4782baced4c707445c6a96401e37f7671f6c4ac5f308c"' : 'data-bs-target="#xs-components-links-module-RelatorioMarcacaoLotesModule-385ef91fca96b3b16a669a4c5aacea5fce4a10dfbc96fe480ea356c020fdbb5bcf3930ae3236f22935e4782baced4c707445c6a96401e37f7671f6c4ac5f308c"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-RelatorioMarcacaoLotesModule-385ef91fca96b3b16a669a4c5aacea5fce4a10dfbc96fe480ea356c020fdbb5bcf3930ae3236f22935e4782baced4c707445c6a96401e37f7671f6c4ac5f308c"' :
                                            'id="xs-components-links-module-RelatorioMarcacaoLotesModule-385ef91fca96b3b16a669a4c5aacea5fce4a10dfbc96fe480ea356c020fdbb5bcf3930ae3236f22935e4782baced4c707445c6a96401e37f7671f6c4ac5f308c"' }>
                                            <li class="link">
                                                <a href="components/RelatorioMarcacaoLotesComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >RelatorioMarcacaoLotesComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/RelatorioMaterialEmpenhadoModule.html" data-type="entity-link" >RelatorioMaterialEmpenhadoModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-RelatorioMaterialEmpenhadoModule-5280bad46458ed16216ec5763bfaea6060e41592af763b732dd83aa9403511ae2a92e8902b400fc1cbf83f8b8d2717aaad731c7818f214f59a9a71addcbd329a"' : 'data-bs-target="#xs-components-links-module-RelatorioMaterialEmpenhadoModule-5280bad46458ed16216ec5763bfaea6060e41592af763b732dd83aa9403511ae2a92e8902b400fc1cbf83f8b8d2717aaad731c7818f214f59a9a71addcbd329a"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-RelatorioMaterialEmpenhadoModule-5280bad46458ed16216ec5763bfaea6060e41592af763b732dd83aa9403511ae2a92e8902b400fc1cbf83f8b8d2717aaad731c7818f214f59a9a71addcbd329a"' :
                                            'id="xs-components-links-module-RelatorioMaterialEmpenhadoModule-5280bad46458ed16216ec5763bfaea6060e41592af763b732dd83aa9403511ae2a92e8902b400fc1cbf83f8b8d2717aaad731c7818f214f59a9a71addcbd329a"' }>
                                            <li class="link">
                                                <a href="components/RelatorioMaterialEmpenhadoComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >RelatorioMaterialEmpenhadoComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/RelatorioModule.html" data-type="entity-link" >RelatorioModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-RelatorioModule-9909e1fe86d4bba040b22017c45e32bdcb1f6ee602368a86de790c70b5decd72b2a34e1c2c6bca239048b3bfb20f62dd7e5369d97125177a86bacbedcf1b3206"' : 'data-bs-target="#xs-components-links-module-RelatorioModule-9909e1fe86d4bba040b22017c45e32bdcb1f6ee602368a86de790c70b5decd72b2a34e1c2c6bca239048b3bfb20f62dd7e5369d97125177a86bacbedcf1b3206"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-RelatorioModule-9909e1fe86d4bba040b22017c45e32bdcb1f6ee602368a86de790c70b5decd72b2a34e1c2c6bca239048b3bfb20f62dd7e5369d97125177a86bacbedcf1b3206"' :
                                            'id="xs-components-links-module-RelatorioModule-9909e1fe86d4bba040b22017c45e32bdcb1f6ee602368a86de790c70b5decd72b2a34e1c2c6bca239048b3bfb20f62dd7e5369d97125177a86bacbedcf1b3206"' }>
                                            <li class="link">
                                                <a href="components/TemplateComponent-8.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >TemplateComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/RelatorioMovEntradaConsultasModule.html" data-type="entity-link" >RelatorioMovEntradaConsultasModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-RelatorioMovEntradaConsultasModule-af8d3fe19662e2e46c89524e9e5ac224569cb03ff24f6724007f8c6a43e0811830957e9949306de4448d409cac2eb52de8ea71f8025559656ff79d1f88a1c239"' : 'data-bs-target="#xs-components-links-module-RelatorioMovEntradaConsultasModule-af8d3fe19662e2e46c89524e9e5ac224569cb03ff24f6724007f8c6a43e0811830957e9949306de4448d409cac2eb52de8ea71f8025559656ff79d1f88a1c239"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-RelatorioMovEntradaConsultasModule-af8d3fe19662e2e46c89524e9e5ac224569cb03ff24f6724007f8c6a43e0811830957e9949306de4448d409cac2eb52de8ea71f8025559656ff79d1f88a1c239"' :
                                            'id="xs-components-links-module-RelatorioMovEntradaConsultasModule-af8d3fe19662e2e46c89524e9e5ac224569cb03ff24f6724007f8c6a43e0811830957e9949306de4448d409cac2eb52de8ea71f8025559656ff79d1f88a1c239"' }>
                                            <li class="link">
                                                <a href="components/RelatorioMovEntradaConsultasComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >RelatorioMovEntradaConsultasComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/RelatorioMovimentacaoEstoqueModule.html" data-type="entity-link" >RelatorioMovimentacaoEstoqueModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-RelatorioMovimentacaoEstoqueModule-b7a5ba0cf5969b49bd9b2c3c27ed132da5b188365b8a9aad6ebbc6e49ce4cddd8589c1a42cf5b03197b9fee6d4f61c186defb43390f9a9c3966bb242f5443862"' : 'data-bs-target="#xs-components-links-module-RelatorioMovimentacaoEstoqueModule-b7a5ba0cf5969b49bd9b2c3c27ed132da5b188365b8a9aad6ebbc6e49ce4cddd8589c1a42cf5b03197b9fee6d4f61c186defb43390f9a9c3966bb242f5443862"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-RelatorioMovimentacaoEstoqueModule-b7a5ba0cf5969b49bd9b2c3c27ed132da5b188365b8a9aad6ebbc6e49ce4cddd8589c1a42cf5b03197b9fee6d4f61c186defb43390f9a9c3966bb242f5443862"' :
                                            'id="xs-components-links-module-RelatorioMovimentacaoEstoqueModule-b7a5ba0cf5969b49bd9b2c3c27ed132da5b188365b8a9aad6ebbc6e49ce4cddd8589c1a42cf5b03197b9fee6d4f61c186defb43390f9a9c3966bb242f5443862"' }>
                                            <li class="link">
                                                <a href="components/RelatorioMovimentacaoEstoqueComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >RelatorioMovimentacaoEstoqueComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/RelatorioMovimentacaoEstoqueSegmentoModule.html" data-type="entity-link" >RelatorioMovimentacaoEstoqueSegmentoModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-RelatorioMovimentacaoEstoqueSegmentoModule-09dee1aaf11b334fb7e4c70502cb6809e4ce1d4c60439b27cce91c3396b6f15e818453d5c4bf03d71e83fd22621a51a0862d5d6eb83d68d82d0af2efb23464c8"' : 'data-bs-target="#xs-components-links-module-RelatorioMovimentacaoEstoqueSegmentoModule-09dee1aaf11b334fb7e4c70502cb6809e4ce1d4c60439b27cce91c3396b6f15e818453d5c4bf03d71e83fd22621a51a0862d5d6eb83d68d82d0af2efb23464c8"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-RelatorioMovimentacaoEstoqueSegmentoModule-09dee1aaf11b334fb7e4c70502cb6809e4ce1d4c60439b27cce91c3396b6f15e818453d5c4bf03d71e83fd22621a51a0862d5d6eb83d68d82d0af2efb23464c8"' :
                                            'id="xs-components-links-module-RelatorioMovimentacaoEstoqueSegmentoModule-09dee1aaf11b334fb7e4c70502cb6809e4ce1d4c60439b27cce91c3396b6f15e818453d5c4bf03d71e83fd22621a51a0862d5d6eb83d68d82d0af2efb23464c8"' }>
                                            <li class="link">
                                                <a href="components/RelatorioMovimentacaoEstoqueSegmentoComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >RelatorioMovimentacaoEstoqueSegmentoComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/RelatorioPecasCortadasModule.html" data-type="entity-link" >RelatorioPecasCortadasModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-RelatorioPecasCortadasModule-c9770bc24644ffc27ab6202ef37f75570feffcbd6f452d5a44cd50183ef9a27997c2f28b56cf195fa5b48c7146f8c757a77ade31a56f4cd6030952fe152243b3"' : 'data-bs-target="#xs-components-links-module-RelatorioPecasCortadasModule-c9770bc24644ffc27ab6202ef37f75570feffcbd6f452d5a44cd50183ef9a27997c2f28b56cf195fa5b48c7146f8c757a77ade31a56f4cd6030952fe152243b3"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-RelatorioPecasCortadasModule-c9770bc24644ffc27ab6202ef37f75570feffcbd6f452d5a44cd50183ef9a27997c2f28b56cf195fa5b48c7146f8c757a77ade31a56f4cd6030952fe152243b3"' :
                                            'id="xs-components-links-module-RelatorioPecasCortadasModule-c9770bc24644ffc27ab6202ef37f75570feffcbd6f452d5a44cd50183ef9a27997c2f28b56cf195fa5b48c7146f8c757a77ade31a56f4cd6030952fe152243b3"' }>
                                            <li class="link">
                                                <a href="components/RelatorioPecasCortadasComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >RelatorioPecasCortadasComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/RelatorioPesosCobradosAbaixoTeoricoModule.html" data-type="entity-link" >RelatorioPesosCobradosAbaixoTeoricoModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-RelatorioPesosCobradosAbaixoTeoricoModule-5053969880dce3f3dc65690ee7cfb36e9b5a9354ec5ea32ba19a155104080421fa1b6b55d30d5237439f99efeb84bb9498ccd31f7d6083cfad1ae5b6736d0f44"' : 'data-bs-target="#xs-components-links-module-RelatorioPesosCobradosAbaixoTeoricoModule-5053969880dce3f3dc65690ee7cfb36e9b5a9354ec5ea32ba19a155104080421fa1b6b55d30d5237439f99efeb84bb9498ccd31f7d6083cfad1ae5b6736d0f44"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-RelatorioPesosCobradosAbaixoTeoricoModule-5053969880dce3f3dc65690ee7cfb36e9b5a9354ec5ea32ba19a155104080421fa1b6b55d30d5237439f99efeb84bb9498ccd31f7d6083cfad1ae5b6736d0f44"' :
                                            'id="xs-components-links-module-RelatorioPesosCobradosAbaixoTeoricoModule-5053969880dce3f3dc65690ee7cfb36e9b5a9354ec5ea32ba19a155104080421fa1b6b55d30d5237439f99efeb84bb9498ccd31f7d6083cfad1ae5b6736d0f44"' }>
                                            <li class="link">
                                                <a href="components/RelatorioPesosCobradosAbaixoTeoricoComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >RelatorioPesosCobradosAbaixoTeoricoComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/RelatorioQuantidadeCompradaModule.html" data-type="entity-link" >RelatorioQuantidadeCompradaModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-RelatorioQuantidadeCompradaModule-08294482f2e21f9a59f109b85dcb6b679c0f9c9801995ebbf3f6a2cbd44c18ac34135200348ac43dab6300ed13529a3e9a19f1cb9f544ad53710bab6e17e813b"' : 'data-bs-target="#xs-components-links-module-RelatorioQuantidadeCompradaModule-08294482f2e21f9a59f109b85dcb6b679c0f9c9801995ebbf3f6a2cbd44c18ac34135200348ac43dab6300ed13529a3e9a19f1cb9f544ad53710bab6e17e813b"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-RelatorioQuantidadeCompradaModule-08294482f2e21f9a59f109b85dcb6b679c0f9c9801995ebbf3f6a2cbd44c18ac34135200348ac43dab6300ed13529a3e9a19f1cb9f544ad53710bab6e17e813b"' :
                                            'id="xs-components-links-module-RelatorioQuantidadeCompradaModule-08294482f2e21f9a59f109b85dcb6b679c0f9c9801995ebbf3f6a2cbd44c18ac34135200348ac43dab6300ed13529a3e9a19f1cb9f544ad53710bab6e17e813b"' }>
                                            <li class="link">
                                                <a href="components/RelatorioQuantidadeCompradaComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >RelatorioQuantidadeCompradaComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/RelatorioRoutingModule.html" data-type="entity-link" >RelatorioRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/RelatorioSegmentoGrupoModule.html" data-type="entity-link" >RelatorioSegmentoGrupoModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-RelatorioSegmentoGrupoModule-a5b88397459e60885b7e63cfab05e10e4de927be92ec3372078a233cdfebfda83d915ba8f2effacefad8a2af654f38d343c7b11b32d047ef8e3bfdaa74ba83ac"' : 'data-bs-target="#xs-components-links-module-RelatorioSegmentoGrupoModule-a5b88397459e60885b7e63cfab05e10e4de927be92ec3372078a233cdfebfda83d915ba8f2effacefad8a2af654f38d343c7b11b32d047ef8e3bfdaa74ba83ac"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-RelatorioSegmentoGrupoModule-a5b88397459e60885b7e63cfab05e10e4de927be92ec3372078a233cdfebfda83d915ba8f2effacefad8a2af654f38d343c7b11b32d047ef8e3bfdaa74ba83ac"' :
                                            'id="xs-components-links-module-RelatorioSegmentoGrupoModule-a5b88397459e60885b7e63cfab05e10e4de927be92ec3372078a233cdfebfda83d915ba8f2effacefad8a2af654f38d343c7b11b32d047ef8e3bfdaa74ba83ac"' }>
                                            <li class="link">
                                                <a href="components/RelatorioSegmentoGrupoComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >RelatorioSegmentoGrupoComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/RelatorioTabelaPrecosModule.html" data-type="entity-link" >RelatorioTabelaPrecosModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-RelatorioTabelaPrecosModule-2a12f05b6bfd59d2aa129e550293c5d401465386b6d06180f1bc79b8bbc62265ffb5da2b79c5497450e9a3bbaf27a85c8cd942a8e29e4a4a97dc97277b51036f"' : 'data-bs-target="#xs-components-links-module-RelatorioTabelaPrecosModule-2a12f05b6bfd59d2aa129e550293c5d401465386b6d06180f1bc79b8bbc62265ffb5da2b79c5497450e9a3bbaf27a85c8cd942a8e29e4a4a97dc97277b51036f"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-RelatorioTabelaPrecosModule-2a12f05b6bfd59d2aa129e550293c5d401465386b6d06180f1bc79b8bbc62265ffb5da2b79c5497450e9a3bbaf27a85c8cd942a8e29e4a4a97dc97277b51036f"' :
                                            'id="xs-components-links-module-RelatorioTabelaPrecosModule-2a12f05b6bfd59d2aa129e550293c5d401465386b6d06180f1bc79b8bbc62265ffb5da2b79c5497450e9a3bbaf27a85c8cd942a8e29e4a4a97dc97277b51036f"' }>
                                            <li class="link">
                                                <a href="components/RelatorioTabelaPrecosComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >RelatorioTabelaPrecosComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/SegmentoModule.html" data-type="entity-link" >SegmentoModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-SegmentoModule-c3e7229536e44158a1d810cc65e9591111f83b66509bd37a6e83ecbf7c975157a5e850dd801d57bf4f00aa3efe22f18f6796c57a6da194c37b49ab4e4da6c640"' : 'data-bs-target="#xs-components-links-module-SegmentoModule-c3e7229536e44158a1d810cc65e9591111f83b66509bd37a6e83ecbf7c975157a5e850dd801d57bf4f00aa3efe22f18f6796c57a6da194c37b49ab4e4da6c640"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-SegmentoModule-c3e7229536e44158a1d810cc65e9591111f83b66509bd37a6e83ecbf7c975157a5e850dd801d57bf4f00aa3efe22f18f6796c57a6da194c37b49ab4e4da6c640"' :
                                            'id="xs-components-links-module-SegmentoModule-c3e7229536e44158a1d810cc65e9591111f83b66509bd37a6e83ecbf7c975157a5e850dd801d57bf4f00aa3efe22f18f6796c57a6da194c37b49ab4e4da6c640"' }>
                                            <li class="link">
                                                <a href="components/SegmentoComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >SegmentoComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/SegmentoRoutingModule.html" data-type="entity-link" >SegmentoRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/SharedDirectivesModule.html" data-type="entity-link" >SharedDirectivesModule</a>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                        'data-bs-target="#directives-links-module-SharedDirectivesModule-0747c9cffce91e10029e82b5d05659f708bfb43c67efae889504d96542033aa7815c99208012849de51dc4c4a1e9a91d982552cd1e8836dc87d76a47dc8c0f70"' : 'data-bs-target="#xs-directives-links-module-SharedDirectivesModule-0747c9cffce91e10029e82b5d05659f708bfb43c67efae889504d96542033aa7815c99208012849de51dc4c4a1e9a91d982552cd1e8836dc87d76a47dc8c0f70"' }>
                                        <span class="icon ion-md-code-working"></span>
                                        <span>Directives</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="directives-links-module-SharedDirectivesModule-0747c9cffce91e10029e82b5d05659f708bfb43c67efae889504d96542033aa7815c99208012849de51dc4c4a1e9a91d982552cd1e8836dc87d76a47dc8c0f70"' :
                                        'id="xs-directives-links-module-SharedDirectivesModule-0747c9cffce91e10029e82b5d05659f708bfb43c67efae889504d96542033aa7815c99208012849de51dc4c4a1e9a91d982552cd1e8836dc87d76a47dc8c0f70"' }>
                                        <li class="link">
                                            <a href="directives/AutofocusDirective.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AutofocusDirective</a>
                                        </li>
                                        <li class="link">
                                            <a href="directives/CheckPermissionsDirective.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >CheckPermissionsDirective</a>
                                        </li>
                                        <li class="link">
                                            <a href="directives/InputDirective.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >InputDirective</a>
                                        </li>
                                        <li class="link">
                                            <a href="directives/NumerosDirective.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >NumerosDirective</a>
                                        </li>
                                        <li class="link">
                                            <a href="directives/ValidadorCampoFormularioDirective.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ValidadorCampoFormularioDirective</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/SharedModule.html" data-type="entity-link" >SharedModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-SharedModule-35c74d670c42692b9044040d6502056de207340999cfb8deb456ed995884f3e1d316eead9f2271d685fc3649754ccd2b7d3f3ea58ef5f52a604da956fac939c1"' : 'data-bs-target="#xs-components-links-module-SharedModule-35c74d670c42692b9044040d6502056de207340999cfb8deb456ed995884f3e1d316eead9f2271d685fc3649754ccd2b7d3f3ea58ef5f52a604da956fac939c1"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-SharedModule-35c74d670c42692b9044040d6502056de207340999cfb8deb456ed995884f3e1d316eead9f2271d685fc3649754ccd2b7d3f3ea58ef5f52a604da956fac939c1"' :
                                            'id="xs-components-links-module-SharedModule-35c74d670c42692b9044040d6502056de207340999cfb8deb456ed995884f3e1d316eead9f2271d685fc3649754ccd2b7d3f3ea58ef5f52a604da956fac939c1"' }>
                                            <li class="link">
                                                <a href="components/ErroCampoFormularioComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ErroCampoFormularioComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/TabelaSaldosGalpaoComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >TabelaSaldosGalpaoComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#pipes-links-module-SharedModule-35c74d670c42692b9044040d6502056de207340999cfb8deb456ed995884f3e1d316eead9f2271d685fc3649754ccd2b7d3f3ea58ef5f52a604da956fac939c1"' : 'data-bs-target="#xs-pipes-links-module-SharedModule-35c74d670c42692b9044040d6502056de207340999cfb8deb456ed995884f3e1d316eead9f2271d685fc3649754ccd2b7d3f3ea58ef5f52a604da956fac939c1"' }>
                                            <span class="icon ion-md-add"></span>
                                            <span>Pipes</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="pipes-links-module-SharedModule-35c74d670c42692b9044040d6502056de207340999cfb8deb456ed995884f3e1d316eead9f2271d685fc3649754ccd2b7d3f3ea58ef5f52a604da956fac939c1"' :
                                            'id="xs-pipes-links-module-SharedModule-35c74d670c42692b9044040d6502056de207340999cfb8deb456ed995884f3e1d316eead9f2271d685fc3649754ccd2b7d3f3ea58ef5f52a604da956fac939c1"' }>
                                            <li class="link">
                                                <a href="pipes/FormataMedidaPipe.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FormataMedidaPipe</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/SubfatorModule.html" data-type="entity-link" >SubfatorModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-SubfatorModule-1b00288451476a2ce5474253069aff03386d9124029c66bef074e75e956da94d8a1e029241fae91d78b94a2bd25f9fe88d6b861e1c3c9de1e99abf543866d7c5"' : 'data-bs-target="#xs-components-links-module-SubfatorModule-1b00288451476a2ce5474253069aff03386d9124029c66bef074e75e956da94d8a1e029241fae91d78b94a2bd25f9fe88d6b861e1c3c9de1e99abf543866d7c5"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-SubfatorModule-1b00288451476a2ce5474253069aff03386d9124029c66bef074e75e956da94d8a1e029241fae91d78b94a2bd25f9fe88d6b861e1c3c9de1e99abf543866d7c5"' :
                                            'id="xs-components-links-module-SubfatorModule-1b00288451476a2ce5474253069aff03386d9124029c66bef074e75e956da94d8a1e029241fae91d78b94a2bd25f9fe88d6b861e1c3c9de1e99abf543866d7c5"' }>
                                            <li class="link">
                                                <a href="components/SubfatorComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >SubfatorComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/SubfatorRoutingModule.html" data-type="entity-link" >SubfatorRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/SubGrupoModule.html" data-type="entity-link" >SubGrupoModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-SubGrupoModule-47bd470a44c975b26a6fb097441c65d27167e497c55c5526e3bba6fe1fef576a13a6c668fa8fd982c19a02f1746a03975ae23c83c09681a7925fb3914a71b482"' : 'data-bs-target="#xs-components-links-module-SubGrupoModule-47bd470a44c975b26a6fb097441c65d27167e497c55c5526e3bba6fe1fef576a13a6c668fa8fd982c19a02f1746a03975ae23c83c09681a7925fb3914a71b482"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-SubGrupoModule-47bd470a44c975b26a6fb097441c65d27167e497c55c5526e3bba6fe1fef576a13a6c668fa8fd982c19a02f1746a03975ae23c83c09681a7925fb3914a71b482"' :
                                            'id="xs-components-links-module-SubGrupoModule-47bd470a44c975b26a6fb097441c65d27167e497c55c5526e3bba6fe1fef576a13a6c668fa8fd982c19a02f1746a03975ae23c83c09681a7925fb3914a71b482"' }>
                                            <li class="link">
                                                <a href="components/SubGrupoComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >SubGrupoComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/SubGrupoRoutingModule.html" data-type="entity-link" >SubGrupoRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/TabelaAcabamentoMaterialModule.html" data-type="entity-link" >TabelaAcabamentoMaterialModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-TabelaAcabamentoMaterialModule-d8ab3498ea2555fc729b5ed2a1a80621ae18bead5ed3a131214d0c23ddf1af69ece6561191c0c9f1277c840855433ea170c87fa0e85539f90c6d6f7cd3ea4581"' : 'data-bs-target="#xs-components-links-module-TabelaAcabamentoMaterialModule-d8ab3498ea2555fc729b5ed2a1a80621ae18bead5ed3a131214d0c23ddf1af69ece6561191c0c9f1277c840855433ea170c87fa0e85539f90c6d6f7cd3ea4581"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-TabelaAcabamentoMaterialModule-d8ab3498ea2555fc729b5ed2a1a80621ae18bead5ed3a131214d0c23ddf1af69ece6561191c0c9f1277c840855433ea170c87fa0e85539f90c6d6f7cd3ea4581"' :
                                            'id="xs-components-links-module-TabelaAcabamentoMaterialModule-d8ab3498ea2555fc729b5ed2a1a80621ae18bead5ed3a131214d0c23ddf1af69ece6561191c0c9f1277c840855433ea170c87fa0e85539f90c6d6f7cd3ea4581"' }>
                                            <li class="link">
                                                <a href="components/TabelaAcabamentoMaterialComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >TabelaAcabamentoMaterialComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/TabelaApelidosItemEstoqueModule.html" data-type="entity-link" >TabelaApelidosItemEstoqueModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-TabelaApelidosItemEstoqueModule-0f9600fe33f9cca50197b7b7e7d58897392a78503b3983d5798e75c2e4bd835bb9698ae8536b2fd2040d461bb987ce14d83a11ba55180d9709fcdafa8c52721f"' : 'data-bs-target="#xs-components-links-module-TabelaApelidosItemEstoqueModule-0f9600fe33f9cca50197b7b7e7d58897392a78503b3983d5798e75c2e4bd835bb9698ae8536b2fd2040d461bb987ce14d83a11ba55180d9709fcdafa8c52721f"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-TabelaApelidosItemEstoqueModule-0f9600fe33f9cca50197b7b7e7d58897392a78503b3983d5798e75c2e4bd835bb9698ae8536b2fd2040d461bb987ce14d83a11ba55180d9709fcdafa8c52721f"' :
                                            'id="xs-components-links-module-TabelaApelidosItemEstoqueModule-0f9600fe33f9cca50197b7b7e7d58897392a78503b3983d5798e75c2e4bd835bb9698ae8536b2fd2040d461bb987ce14d83a11ba55180d9709fcdafa8c52721f"' }>
                                            <li class="link">
                                                <a href="components/TabelaApelidosItemEstoqueComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >TabelaApelidosItemEstoqueComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/TabelaClassificacaoNcmModule.html" data-type="entity-link" >TabelaClassificacaoNcmModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-TabelaClassificacaoNcmModule-7919fed4d951b078c868f36675d18c6927c8f47c2525d9727ccfaa6c0fe1d34be1ebb27a39a43a3a3d8aade121dd0f9661bbe788a456f60ae4d6f0a2e563c872"' : 'data-bs-target="#xs-components-links-module-TabelaClassificacaoNcmModule-7919fed4d951b078c868f36675d18c6927c8f47c2525d9727ccfaa6c0fe1d34be1ebb27a39a43a3a3d8aade121dd0f9661bbe788a456f60ae4d6f0a2e563c872"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-TabelaClassificacaoNcmModule-7919fed4d951b078c868f36675d18c6927c8f47c2525d9727ccfaa6c0fe1d34be1ebb27a39a43a3a3d8aade121dd0f9661bbe788a456f60ae4d6f0a2e563c872"' :
                                            'id="xs-components-links-module-TabelaClassificacaoNcmModule-7919fed4d951b078c868f36675d18c6927c8f47c2525d9727ccfaa6c0fe1d34be1ebb27a39a43a3a3d8aade121dd0f9661bbe788a456f60ae4d6f0a2e563c872"' }>
                                            <li class="link">
                                                <a href="components/TabelaClassificacaoNcmComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >TabelaClassificacaoNcmComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/TabelaEntradaGeraLoteModule.html" data-type="entity-link" >TabelaEntradaGeraLoteModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-TabelaEntradaGeraLoteModule-8cd9d0466855c98a2b6375fae60dc9b2932a4d3de45d6ab24b409ae89c8fd4821c1c5c7eeaf5c3c37942c393cc80516ae49a03751fc53f64502f9efd058e65e8"' : 'data-bs-target="#xs-components-links-module-TabelaEntradaGeraLoteModule-8cd9d0466855c98a2b6375fae60dc9b2932a4d3de45d6ab24b409ae89c8fd4821c1c5c7eeaf5c3c37942c393cc80516ae49a03751fc53f64502f9efd058e65e8"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-TabelaEntradaGeraLoteModule-8cd9d0466855c98a2b6375fae60dc9b2932a4d3de45d6ab24b409ae89c8fd4821c1c5c7eeaf5c3c37942c393cc80516ae49a03751fc53f64502f9efd058e65e8"' :
                                            'id="xs-components-links-module-TabelaEntradaGeraLoteModule-8cd9d0466855c98a2b6375fae60dc9b2932a4d3de45d6ab24b409ae89c8fd4821c1c5c7eeaf5c3c37942c393cc80516ae49a03751fc53f64502f9efd058e65e8"' }>
                                            <li class="link">
                                                <a href="components/TabelaEntradaGeraLoteComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >TabelaEntradaGeraLoteComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/TabelaEntradaItemDevolucaoModule.html" data-type="entity-link" >TabelaEntradaItemDevolucaoModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-TabelaEntradaItemDevolucaoModule-c895fac8a6b8bd5e1ca21d9276112cfc5f8fc2f0593311c3e1fd8beb291049111c651892e670f272cab24f7561c67de4e24ce2b275ea362c4570b09606b58d96"' : 'data-bs-target="#xs-components-links-module-TabelaEntradaItemDevolucaoModule-c895fac8a6b8bd5e1ca21d9276112cfc5f8fc2f0593311c3e1fd8beb291049111c651892e670f272cab24f7561c67de4e24ce2b275ea362c4570b09606b58d96"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-TabelaEntradaItemDevolucaoModule-c895fac8a6b8bd5e1ca21d9276112cfc5f8fc2f0593311c3e1fd8beb291049111c651892e670f272cab24f7561c67de4e24ce2b275ea362c4570b09606b58d96"' :
                                            'id="xs-components-links-module-TabelaEntradaItemDevolucaoModule-c895fac8a6b8bd5e1ca21d9276112cfc5f8fc2f0593311c3e1fd8beb291049111c651892e670f272cab24f7561c67de4e24ce2b275ea362c4570b09606b58d96"' }>
                                            <li class="link">
                                                <a href="components/TabelaEntradaItemDevolucaoComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >TabelaEntradaItemDevolucaoComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#pipes-links-module-TabelaEntradaItemDevolucaoModule-c895fac8a6b8bd5e1ca21d9276112cfc5f8fc2f0593311c3e1fd8beb291049111c651892e670f272cab24f7561c67de4e24ce2b275ea362c4570b09606b58d96"' : 'data-bs-target="#xs-pipes-links-module-TabelaEntradaItemDevolucaoModule-c895fac8a6b8bd5e1ca21d9276112cfc5f8fc2f0593311c3e1fd8beb291049111c651892e670f272cab24f7561c67de4e24ce2b275ea362c4570b09606b58d96"' }>
                                            <span class="icon ion-md-add"></span>
                                            <span>Pipes</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="pipes-links-module-TabelaEntradaItemDevolucaoModule-c895fac8a6b8bd5e1ca21d9276112cfc5f8fc2f0593311c3e1fd8beb291049111c651892e670f272cab24f7561c67de4e24ce2b275ea362c4570b09606b58d96"' :
                                            'id="xs-pipes-links-module-TabelaEntradaItemDevolucaoModule-c895fac8a6b8bd5e1ca21d9276112cfc5f8fc2f0593311c3e1fd8beb291049111c651892e670f272cab24f7561c67de4e24ce2b275ea362c4570b09606b58d96"' }>
                                            <li class="link">
                                                <a href="pipes/FormataMedidaPipe.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FormataMedidaPipe</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/TabelaEntradaItemEstoqueModule.html" data-type="entity-link" >TabelaEntradaItemEstoqueModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-TabelaEntradaItemEstoqueModule-bb428c4e48600de5b6efc27cc61c923798fc92c1a50f51baea6e720d6f68470a0a611bdbd8b6a5e1f9a3cd4ea13a5f5a7b1d37b94705aa05dca3b327314d1666"' : 'data-bs-target="#xs-components-links-module-TabelaEntradaItemEstoqueModule-bb428c4e48600de5b6efc27cc61c923798fc92c1a50f51baea6e720d6f68470a0a611bdbd8b6a5e1f9a3cd4ea13a5f5a7b1d37b94705aa05dca3b327314d1666"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-TabelaEntradaItemEstoqueModule-bb428c4e48600de5b6efc27cc61c923798fc92c1a50f51baea6e720d6f68470a0a611bdbd8b6a5e1f9a3cd4ea13a5f5a7b1d37b94705aa05dca3b327314d1666"' :
                                            'id="xs-components-links-module-TabelaEntradaItemEstoqueModule-bb428c4e48600de5b6efc27cc61c923798fc92c1a50f51baea6e720d6f68470a0a611bdbd8b6a5e1f9a3cd4ea13a5f5a7b1d37b94705aa05dca3b327314d1666"' }>
                                            <li class="link">
                                                <a href="components/TabelaEntradaItemEstoqueComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >TabelaEntradaItemEstoqueComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/TabelaEntradaItemGeraLoteModule.html" data-type="entity-link" >TabelaEntradaItemGeraLoteModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-TabelaEntradaItemGeraLoteModule-ff3e3f2c9e64a1afb3508462333df99a56fbfbda02fb6502e386f4995768cd690b63834f1f9d8d43c5a9e1fcacf8f4e69c2afb1a263c5059e879978f2ccc6d33"' : 'data-bs-target="#xs-components-links-module-TabelaEntradaItemGeraLoteModule-ff3e3f2c9e64a1afb3508462333df99a56fbfbda02fb6502e386f4995768cd690b63834f1f9d8d43c5a9e1fcacf8f4e69c2afb1a263c5059e879978f2ccc6d33"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-TabelaEntradaItemGeraLoteModule-ff3e3f2c9e64a1afb3508462333df99a56fbfbda02fb6502e386f4995768cd690b63834f1f9d8d43c5a9e1fcacf8f4e69c2afb1a263c5059e879978f2ccc6d33"' :
                                            'id="xs-components-links-module-TabelaEntradaItemGeraLoteModule-ff3e3f2c9e64a1afb3508462333df99a56fbfbda02fb6502e386f4995768cd690b63834f1f9d8d43c5a9e1fcacf8f4e69c2afb1a263c5059e879978f2ccc6d33"' }>
                                            <li class="link">
                                                <a href="components/TabelaEntradaItemGeraLoteComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >TabelaEntradaItemGeraLoteComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#pipes-links-module-TabelaEntradaItemGeraLoteModule-ff3e3f2c9e64a1afb3508462333df99a56fbfbda02fb6502e386f4995768cd690b63834f1f9d8d43c5a9e1fcacf8f4e69c2afb1a263c5059e879978f2ccc6d33"' : 'data-bs-target="#xs-pipes-links-module-TabelaEntradaItemGeraLoteModule-ff3e3f2c9e64a1afb3508462333df99a56fbfbda02fb6502e386f4995768cd690b63834f1f9d8d43c5a9e1fcacf8f4e69c2afb1a263c5059e879978f2ccc6d33"' }>
                                            <span class="icon ion-md-add"></span>
                                            <span>Pipes</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="pipes-links-module-TabelaEntradaItemGeraLoteModule-ff3e3f2c9e64a1afb3508462333df99a56fbfbda02fb6502e386f4995768cd690b63834f1f9d8d43c5a9e1fcacf8f4e69c2afb1a263c5059e879978f2ccc6d33"' :
                                            'id="xs-pipes-links-module-TabelaEntradaItemGeraLoteModule-ff3e3f2c9e64a1afb3508462333df99a56fbfbda02fb6502e386f4995768cd690b63834f1f9d8d43c5a9e1fcacf8f4e69c2afb1a263c5059e879978f2ccc6d33"' }>
                                            <li class="link">
                                                <a href="pipes/FormataMedidaPipe.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FormataMedidaPipe</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/TabelaEntradaItemMaterialAchadoModule.html" data-type="entity-link" >TabelaEntradaItemMaterialAchadoModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-TabelaEntradaItemMaterialAchadoModule-61f7a84f8e439931584c21cdbacd4e08615c82dede68240a56ec96d02ab8c03f97ddaa2bf3f09de515256ef95bd196f60aab954903a090fe0dea5aff7a86c4cc"' : 'data-bs-target="#xs-components-links-module-TabelaEntradaItemMaterialAchadoModule-61f7a84f8e439931584c21cdbacd4e08615c82dede68240a56ec96d02ab8c03f97ddaa2bf3f09de515256ef95bd196f60aab954903a090fe0dea5aff7a86c4cc"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-TabelaEntradaItemMaterialAchadoModule-61f7a84f8e439931584c21cdbacd4e08615c82dede68240a56ec96d02ab8c03f97ddaa2bf3f09de515256ef95bd196f60aab954903a090fe0dea5aff7a86c4cc"' :
                                            'id="xs-components-links-module-TabelaEntradaItemMaterialAchadoModule-61f7a84f8e439931584c21cdbacd4e08615c82dede68240a56ec96d02ab8c03f97ddaa2bf3f09de515256ef95bd196f60aab954903a090fe0dea5aff7a86c4cc"' }>
                                            <li class="link">
                                                <a href="components/TabelaEntradaItemMaterialAchadoComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >TabelaEntradaItemMaterialAchadoComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#pipes-links-module-TabelaEntradaItemMaterialAchadoModule-61f7a84f8e439931584c21cdbacd4e08615c82dede68240a56ec96d02ab8c03f97ddaa2bf3f09de515256ef95bd196f60aab954903a090fe0dea5aff7a86c4cc"' : 'data-bs-target="#xs-pipes-links-module-TabelaEntradaItemMaterialAchadoModule-61f7a84f8e439931584c21cdbacd4e08615c82dede68240a56ec96d02ab8c03f97ddaa2bf3f09de515256ef95bd196f60aab954903a090fe0dea5aff7a86c4cc"' }>
                                            <span class="icon ion-md-add"></span>
                                            <span>Pipes</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="pipes-links-module-TabelaEntradaItemMaterialAchadoModule-61f7a84f8e439931584c21cdbacd4e08615c82dede68240a56ec96d02ab8c03f97ddaa2bf3f09de515256ef95bd196f60aab954903a090fe0dea5aff7a86c4cc"' :
                                            'id="xs-pipes-links-module-TabelaEntradaItemMaterialAchadoModule-61f7a84f8e439931584c21cdbacd4e08615c82dede68240a56ec96d02ab8c03f97ddaa2bf3f09de515256ef95bd196f60aab954903a090fe0dea5aff7a86c4cc"' }>
                                            <li class="link">
                                                <a href="pipes/FormataMedidaPipe.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FormataMedidaPipe</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/TabelaEntradaItemModule.html" data-type="entity-link" >TabelaEntradaItemModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-TabelaEntradaItemModule-da44e53cc79adec2519ee8dd5417009c75b64dc07f3927a6ecdf4c7744384117e86059782ec567473e096117bb89fc02078479b59da09ad28328c6ad89ca83b5"' : 'data-bs-target="#xs-components-links-module-TabelaEntradaItemModule-da44e53cc79adec2519ee8dd5417009c75b64dc07f3927a6ecdf4c7744384117e86059782ec567473e096117bb89fc02078479b59da09ad28328c6ad89ca83b5"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-TabelaEntradaItemModule-da44e53cc79adec2519ee8dd5417009c75b64dc07f3927a6ecdf4c7744384117e86059782ec567473e096117bb89fc02078479b59da09ad28328c6ad89ca83b5"' :
                                            'id="xs-components-links-module-TabelaEntradaItemModule-da44e53cc79adec2519ee8dd5417009c75b64dc07f3927a6ecdf4c7744384117e86059782ec567473e096117bb89fc02078479b59da09ad28328c6ad89ca83b5"' }>
                                            <li class="link">
                                                <a href="components/TabelaEntradaItemComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >TabelaEntradaItemComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#pipes-links-module-TabelaEntradaItemModule-da44e53cc79adec2519ee8dd5417009c75b64dc07f3927a6ecdf4c7744384117e86059782ec567473e096117bb89fc02078479b59da09ad28328c6ad89ca83b5"' : 'data-bs-target="#xs-pipes-links-module-TabelaEntradaItemModule-da44e53cc79adec2519ee8dd5417009c75b64dc07f3927a6ecdf4c7744384117e86059782ec567473e096117bb89fc02078479b59da09ad28328c6ad89ca83b5"' }>
                                            <span class="icon ion-md-add"></span>
                                            <span>Pipes</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="pipes-links-module-TabelaEntradaItemModule-da44e53cc79adec2519ee8dd5417009c75b64dc07f3927a6ecdf4c7744384117e86059782ec567473e096117bb89fc02078479b59da09ad28328c6ad89ca83b5"' :
                                            'id="xs-pipes-links-module-TabelaEntradaItemModule-da44e53cc79adec2519ee8dd5417009c75b64dc07f3927a6ecdf4c7744384117e86059782ec567473e096117bb89fc02078479b59da09ad28328c6ad89ca83b5"' }>
                                            <li class="link">
                                                <a href="pipes/FormataMedidaPipe.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FormataMedidaPipe</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/TabelaEntradaItemNovoModule.html" data-type="entity-link" >TabelaEntradaItemNovoModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-TabelaEntradaItemNovoModule-e2c7aded757e91934c0d3ec0f036cc4eade4f29e6c3618378f83255351a8952f4d26d4bda6866f7414fed55a40adaa5a623a4f92250fe22f866f6b896b5224dd"' : 'data-bs-target="#xs-components-links-module-TabelaEntradaItemNovoModule-e2c7aded757e91934c0d3ec0f036cc4eade4f29e6c3618378f83255351a8952f4d26d4bda6866f7414fed55a40adaa5a623a4f92250fe22f866f6b896b5224dd"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-TabelaEntradaItemNovoModule-e2c7aded757e91934c0d3ec0f036cc4eade4f29e6c3618378f83255351a8952f4d26d4bda6866f7414fed55a40adaa5a623a4f92250fe22f866f6b896b5224dd"' :
                                            'id="xs-components-links-module-TabelaEntradaItemNovoModule-e2c7aded757e91934c0d3ec0f036cc4eade4f29e6c3618378f83255351a8952f4d26d4bda6866f7414fed55a40adaa5a623a4f92250fe22f866f6b896b5224dd"' }>
                                            <li class="link">
                                                <a href="components/TabelaEntradaItemNovoComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >TabelaEntradaItemNovoComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#pipes-links-module-TabelaEntradaItemNovoModule-e2c7aded757e91934c0d3ec0f036cc4eade4f29e6c3618378f83255351a8952f4d26d4bda6866f7414fed55a40adaa5a623a4f92250fe22f866f6b896b5224dd"' : 'data-bs-target="#xs-pipes-links-module-TabelaEntradaItemNovoModule-e2c7aded757e91934c0d3ec0f036cc4eade4f29e6c3618378f83255351a8952f4d26d4bda6866f7414fed55a40adaa5a623a4f92250fe22f866f6b896b5224dd"' }>
                                            <span class="icon ion-md-add"></span>
                                            <span>Pipes</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="pipes-links-module-TabelaEntradaItemNovoModule-e2c7aded757e91934c0d3ec0f036cc4eade4f29e6c3618378f83255351a8952f4d26d4bda6866f7414fed55a40adaa5a623a4f92250fe22f866f6b896b5224dd"' :
                                            'id="xs-pipes-links-module-TabelaEntradaItemNovoModule-e2c7aded757e91934c0d3ec0f036cc4eade4f29e6c3618378f83255351a8952f4d26d4bda6866f7414fed55a40adaa5a623a4f92250fe22f866f6b896b5224dd"' }>
                                            <li class="link">
                                                <a href="pipes/FormataMedidaPipe.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FormataMedidaPipe</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/TabelaEstadoMaterialModule.html" data-type="entity-link" >TabelaEstadoMaterialModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-TabelaEstadoMaterialModule-f54c307b4d05f7938a62694cb446d67bba72c5d357e6fa81ca54c417a501dabe66a2a466af98d6d432ef0b85667b10c43e61d9879ef52d0f46827ab4101eab50"' : 'data-bs-target="#xs-components-links-module-TabelaEstadoMaterialModule-f54c307b4d05f7938a62694cb446d67bba72c5d357e6fa81ca54c417a501dabe66a2a466af98d6d432ef0b85667b10c43e61d9879ef52d0f46827ab4101eab50"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-TabelaEstadoMaterialModule-f54c307b4d05f7938a62694cb446d67bba72c5d357e6fa81ca54c417a501dabe66a2a466af98d6d432ef0b85667b10c43e61d9879ef52d0f46827ab4101eab50"' :
                                            'id="xs-components-links-module-TabelaEstadoMaterialModule-f54c307b4d05f7938a62694cb446d67bba72c5d357e6fa81ca54c417a501dabe66a2a466af98d6d432ef0b85667b10c43e61d9879ef52d0f46827ab4101eab50"' }>
                                            <li class="link">
                                                <a href="components/TabelaEstadoMaterialComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >TabelaEstadoMaterialComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/TabelaFormaMaterialModule.html" data-type="entity-link" >TabelaFormaMaterialModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-TabelaFormaMaterialModule-8d01c145927684cfeebd515f369d876fc32270652417c2ba2152a241303be9b558cf5bd350e2becdafaef93ab0b5be0877cbc21ac528fcd709433a3eeb9e3d1b"' : 'data-bs-target="#xs-components-links-module-TabelaFormaMaterialModule-8d01c145927684cfeebd515f369d876fc32270652417c2ba2152a241303be9b558cf5bd350e2becdafaef93ab0b5be0877cbc21ac528fcd709433a3eeb9e3d1b"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-TabelaFormaMaterialModule-8d01c145927684cfeebd515f369d876fc32270652417c2ba2152a241303be9b558cf5bd350e2becdafaef93ab0b5be0877cbc21ac528fcd709433a3eeb9e3d1b"' :
                                            'id="xs-components-links-module-TabelaFormaMaterialModule-8d01c145927684cfeebd515f369d876fc32270652417c2ba2152a241303be9b558cf5bd350e2becdafaef93ab0b5be0877cbc21ac528fcd709433a3eeb9e3d1b"' }>
                                            <li class="link">
                                                <a href="components/TabelaFormaMaterialComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >TabelaFormaMaterialComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/TabelaFornecedorCadastroModule.html" data-type="entity-link" >TabelaFornecedorCadastroModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-TabelaFornecedorCadastroModule-913804e8aa023478d47c9331f97e488a00bfff80b3ebc5fc0f63db6415db9671c3c2f62c912436bc55f8ea478a6839aa066d737acd543c53a12c430591e702b2"' : 'data-bs-target="#xs-components-links-module-TabelaFornecedorCadastroModule-913804e8aa023478d47c9331f97e488a00bfff80b3ebc5fc0f63db6415db9671c3c2f62c912436bc55f8ea478a6839aa066d737acd543c53a12c430591e702b2"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-TabelaFornecedorCadastroModule-913804e8aa023478d47c9331f97e488a00bfff80b3ebc5fc0f63db6415db9671c3c2f62c912436bc55f8ea478a6839aa066d737acd543c53a12c430591e702b2"' :
                                            'id="xs-components-links-module-TabelaFornecedorCadastroModule-913804e8aa023478d47c9331f97e488a00bfff80b3ebc5fc0f63db6415db9671c3c2f62c912436bc55f8ea478a6839aa066d737acd543c53a12c430591e702b2"' }>
                                            <li class="link">
                                                <a href="components/TabelaFornecedorCadastroComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >TabelaFornecedorCadastroComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/TabelaGalpaoModule.html" data-type="entity-link" >TabelaGalpaoModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-TabelaGalpaoModule-c46063dc7cb4e02b167ca792713216a87c7f2289db82bc729f192ea4c5a2469bc017f0b37d774c822bb5b5795d34157e309a487dbc92689345d41cdba775afff"' : 'data-bs-target="#xs-components-links-module-TabelaGalpaoModule-c46063dc7cb4e02b167ca792713216a87c7f2289db82bc729f192ea4c5a2469bc017f0b37d774c822bb5b5795d34157e309a487dbc92689345d41cdba775afff"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-TabelaGalpaoModule-c46063dc7cb4e02b167ca792713216a87c7f2289db82bc729f192ea4c5a2469bc017f0b37d774c822bb5b5795d34157e309a487dbc92689345d41cdba775afff"' :
                                            'id="xs-components-links-module-TabelaGalpaoModule-c46063dc7cb4e02b167ca792713216a87c7f2289db82bc729f192ea4c5a2469bc017f0b37d774c822bb5b5795d34157e309a487dbc92689345d41cdba775afff"' }>
                                            <li class="link">
                                                <a href="components/TabelaGalpaoComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >TabelaGalpaoComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/TabelaGrupoModule.html" data-type="entity-link" >TabelaGrupoModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-TabelaGrupoModule-52d7281df71517fe558a814979e55754f2b36d32b398dc8c670bf8c1deaf08cbc450f3275b4176fecf637621b4ee7d588acbdc57750a2a2069714a040a075dcd"' : 'data-bs-target="#xs-components-links-module-TabelaGrupoModule-52d7281df71517fe558a814979e55754f2b36d32b398dc8c670bf8c1deaf08cbc450f3275b4176fecf637621b4ee7d588acbdc57750a2a2069714a040a075dcd"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-TabelaGrupoModule-52d7281df71517fe558a814979e55754f2b36d32b398dc8c670bf8c1deaf08cbc450f3275b4176fecf637621b4ee7d588acbdc57750a2a2069714a040a075dcd"' :
                                            'id="xs-components-links-module-TabelaGrupoModule-52d7281df71517fe558a814979e55754f2b36d32b398dc8c670bf8c1deaf08cbc450f3275b4176fecf637621b4ee7d588acbdc57750a2a2069714a040a075dcd"' }>
                                            <li class="link">
                                                <a href="components/TabelaGrupoComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >TabelaGrupoComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/TabelaGrupoSegmentoModule.html" data-type="entity-link" >TabelaGrupoSegmentoModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-TabelaGrupoSegmentoModule-5a12afdd44ec67aae0ffb9cb9c70825f9487543dd0c88b8ad35f91d237458da132b7ea4951d74a6be955a33b4bd10e8c27e8adfa626580b7eeba7d824b08a729"' : 'data-bs-target="#xs-components-links-module-TabelaGrupoSegmentoModule-5a12afdd44ec67aae0ffb9cb9c70825f9487543dd0c88b8ad35f91d237458da132b7ea4951d74a6be955a33b4bd10e8c27e8adfa626580b7eeba7d824b08a729"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-TabelaGrupoSegmentoModule-5a12afdd44ec67aae0ffb9cb9c70825f9487543dd0c88b8ad35f91d237458da132b7ea4951d74a6be955a33b4bd10e8c27e8adfa626580b7eeba7d824b08a729"' :
                                            'id="xs-components-links-module-TabelaGrupoSegmentoModule-5a12afdd44ec67aae0ffb9cb9c70825f9487543dd0c88b8ad35f91d237458da132b7ea4951d74a6be955a33b4bd10e8c27e8adfa626580b7eeba7d824b08a729"' }>
                                            <li class="link">
                                                <a href="components/TabelaGrupoSegmentoComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >TabelaGrupoSegmentoComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/TabelaItemEstoqueModule.html" data-type="entity-link" >TabelaItemEstoqueModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-TabelaItemEstoqueModule-66530e8d9f790676e688993e3c6723f8c31f573cbe93663f7cc4d2cfa3efc17a7b7103b86e60fcd6fa01ac9318af2aed15b523ab48cf08f548085f71b66d5a60"' : 'data-bs-target="#xs-components-links-module-TabelaItemEstoqueModule-66530e8d9f790676e688993e3c6723f8c31f573cbe93663f7cc4d2cfa3efc17a7b7103b86e60fcd6fa01ac9318af2aed15b523ab48cf08f548085f71b66d5a60"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-TabelaItemEstoqueModule-66530e8d9f790676e688993e3c6723f8c31f573cbe93663f7cc4d2cfa3efc17a7b7103b86e60fcd6fa01ac9318af2aed15b523ab48cf08f548085f71b66d5a60"' :
                                            'id="xs-components-links-module-TabelaItemEstoqueModule-66530e8d9f790676e688993e3c6723f8c31f573cbe93663f7cc4d2cfa3efc17a7b7103b86e60fcd6fa01ac9318af2aed15b523ab48cf08f548085f71b66d5a60"' }>
                                            <li class="link">
                                                <a href="components/TabelaItemEstoqueComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >TabelaItemEstoqueComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#pipes-links-module-TabelaItemEstoqueModule-66530e8d9f790676e688993e3c6723f8c31f573cbe93663f7cc4d2cfa3efc17a7b7103b86e60fcd6fa01ac9318af2aed15b523ab48cf08f548085f71b66d5a60"' : 'data-bs-target="#xs-pipes-links-module-TabelaItemEstoqueModule-66530e8d9f790676e688993e3c6723f8c31f573cbe93663f7cc4d2cfa3efc17a7b7103b86e60fcd6fa01ac9318af2aed15b523ab48cf08f548085f71b66d5a60"' }>
                                            <span class="icon ion-md-add"></span>
                                            <span>Pipes</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="pipes-links-module-TabelaItemEstoqueModule-66530e8d9f790676e688993e3c6723f8c31f573cbe93663f7cc4d2cfa3efc17a7b7103b86e60fcd6fa01ac9318af2aed15b523ab48cf08f548085f71b66d5a60"' :
                                            'id="xs-pipes-links-module-TabelaItemEstoqueModule-66530e8d9f790676e688993e3c6723f8c31f573cbe93663f7cc4d2cfa3efc17a7b7103b86e60fcd6fa01ac9318af2aed15b523ab48cf08f548085f71b66d5a60"' }>
                                            <li class="link">
                                                <a href="pipes/FormataMedidaPipe.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FormataMedidaPipe</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/TabelaLoteRastreabilidadeModule.html" data-type="entity-link" >TabelaLoteRastreabilidadeModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-TabelaLoteRastreabilidadeModule-1b8720e7de5247951367f8d15a82d17dd2030a24fd17e927fd74c6deadd03fa4dcbd5b83912dc9490496e384443881331a08866a25a4538c2dbd9956c06dd09a"' : 'data-bs-target="#xs-components-links-module-TabelaLoteRastreabilidadeModule-1b8720e7de5247951367f8d15a82d17dd2030a24fd17e927fd74c6deadd03fa4dcbd5b83912dc9490496e384443881331a08866a25a4538c2dbd9956c06dd09a"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-TabelaLoteRastreabilidadeModule-1b8720e7de5247951367f8d15a82d17dd2030a24fd17e927fd74c6deadd03fa4dcbd5b83912dc9490496e384443881331a08866a25a4538c2dbd9956c06dd09a"' :
                                            'id="xs-components-links-module-TabelaLoteRastreabilidadeModule-1b8720e7de5247951367f8d15a82d17dd2030a24fd17e927fd74c6deadd03fa4dcbd5b83912dc9490496e384443881331a08866a25a4538c2dbd9956c06dd09a"' }>
                                            <li class="link">
                                                <a href="components/TabelaLoteRastreabilidadeComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >TabelaLoteRastreabilidadeComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/TabelaManutencaoLoteModule.html" data-type="entity-link" >TabelaManutencaoLoteModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-TabelaManutencaoLoteModule-4b3a935de051fc33d606d613c4db6d3884ff4b3d7f3aa7cfc3f97e732b69bdda51813e288698f3b44e09ac1fe5777a39a28104285ab768656c2f26e679f90ba7"' : 'data-bs-target="#xs-components-links-module-TabelaManutencaoLoteModule-4b3a935de051fc33d606d613c4db6d3884ff4b3d7f3aa7cfc3f97e732b69bdda51813e288698f3b44e09ac1fe5777a39a28104285ab768656c2f26e679f90ba7"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-TabelaManutencaoLoteModule-4b3a935de051fc33d606d613c4db6d3884ff4b3d7f3aa7cfc3f97e732b69bdda51813e288698f3b44e09ac1fe5777a39a28104285ab768656c2f26e679f90ba7"' :
                                            'id="xs-components-links-module-TabelaManutencaoLoteModule-4b3a935de051fc33d606d613c4db6d3884ff4b3d7f3aa7cfc3f97e732b69bdda51813e288698f3b44e09ac1fe5777a39a28104285ab768656c2f26e679f90ba7"' }>
                                            <li class="link">
                                                <a href="components/TabelaManutencaoLoteComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >TabelaManutencaoLoteComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/TabelaMaterialMedidaModule.html" data-type="entity-link" >TabelaMaterialMedidaModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-TabelaMaterialMedidaModule-adb12731e536d6d1535de9af1d731029cfb5f80b7ddc65a73857ed6f8bac0d1cfe39a0057d37b99162530ee18d5cdab1e0f4d37dc298ff196a8e67011166b812"' : 'data-bs-target="#xs-components-links-module-TabelaMaterialMedidaModule-adb12731e536d6d1535de9af1d731029cfb5f80b7ddc65a73857ed6f8bac0d1cfe39a0057d37b99162530ee18d5cdab1e0f4d37dc298ff196a8e67011166b812"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-TabelaMaterialMedidaModule-adb12731e536d6d1535de9af1d731029cfb5f80b7ddc65a73857ed6f8bac0d1cfe39a0057d37b99162530ee18d5cdab1e0f4d37dc298ff196a8e67011166b812"' :
                                            'id="xs-components-links-module-TabelaMaterialMedidaModule-adb12731e536d6d1535de9af1d731029cfb5f80b7ddc65a73857ed6f8bac0d1cfe39a0057d37b99162530ee18d5cdab1e0f4d37dc298ff196a8e67011166b812"' }>
                                            <li class="link">
                                                <a href="components/TabelaMaterialMedidaComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >TabelaMaterialMedidaComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/TabelaMaterialModule.html" data-type="entity-link" >TabelaMaterialModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-TabelaMaterialModule-df8049a332da3ac344eb2f4e00d6b3edd48961a43e1904a3d05a23e04563996c54b82847139c994b355277548afa15c239d8f950fbc98f29fe2d008f3d5f9d4d"' : 'data-bs-target="#xs-components-links-module-TabelaMaterialModule-df8049a332da3ac344eb2f4e00d6b3edd48961a43e1904a3d05a23e04563996c54b82847139c994b355277548afa15c239d8f950fbc98f29fe2d008f3d5f9d4d"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-TabelaMaterialModule-df8049a332da3ac344eb2f4e00d6b3edd48961a43e1904a3d05a23e04563996c54b82847139c994b355277548afa15c239d8f950fbc98f29fe2d008f3d5f9d4d"' :
                                            'id="xs-components-links-module-TabelaMaterialModule-df8049a332da3ac344eb2f4e00d6b3edd48961a43e1904a3d05a23e04563996c54b82847139c994b355277548afa15c239d8f950fbc98f29fe2d008f3d5f9d4d"' }>
                                            <li class="link">
                                                <a href="components/TabelaMaterialComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >TabelaMaterialComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/TabelaPedidoCompraModule.html" data-type="entity-link" >TabelaPedidoCompraModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-TabelaPedidoCompraModule-5321dbcbba19064718ba8b06877f828e0c58f12a97a97b38fbcfd346da197012c22847078e4e41aa290976f58b6cfd32be7cebabbcd90c4ecd6c945207d83ea9"' : 'data-bs-target="#xs-components-links-module-TabelaPedidoCompraModule-5321dbcbba19064718ba8b06877f828e0c58f12a97a97b38fbcfd346da197012c22847078e4e41aa290976f58b6cfd32be7cebabbcd90c4ecd6c945207d83ea9"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-TabelaPedidoCompraModule-5321dbcbba19064718ba8b06877f828e0c58f12a97a97b38fbcfd346da197012c22847078e4e41aa290976f58b6cfd32be7cebabbcd90c4ecd6c945207d83ea9"' :
                                            'id="xs-components-links-module-TabelaPedidoCompraModule-5321dbcbba19064718ba8b06877f828e0c58f12a97a97b38fbcfd346da197012c22847078e4e41aa290976f58b6cfd32be7cebabbcd90c4ecd6c945207d83ea9"' }>
                                            <li class="link">
                                                <a href="components/TabelaPedidoCompraComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >TabelaPedidoCompraComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                        'data-bs-target="#directives-links-module-TabelaPedidoCompraModule-5321dbcbba19064718ba8b06877f828e0c58f12a97a97b38fbcfd346da197012c22847078e4e41aa290976f58b6cfd32be7cebabbcd90c4ecd6c945207d83ea9"' : 'data-bs-target="#xs-directives-links-module-TabelaPedidoCompraModule-5321dbcbba19064718ba8b06877f828e0c58f12a97a97b38fbcfd346da197012c22847078e4e41aa290976f58b6cfd32be7cebabbcd90c4ecd6c945207d83ea9"' }>
                                        <span class="icon ion-md-code-working"></span>
                                        <span>Directives</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="directives-links-module-TabelaPedidoCompraModule-5321dbcbba19064718ba8b06877f828e0c58f12a97a97b38fbcfd346da197012c22847078e4e41aa290976f58b6cfd32be7cebabbcd90c4ecd6c945207d83ea9"' :
                                        'id="xs-directives-links-module-TabelaPedidoCompraModule-5321dbcbba19064718ba8b06877f828e0c58f12a97a97b38fbcfd346da197012c22847078e4e41aa290976f58b6cfd32be7cebabbcd90c4ecd6c945207d83ea9"' }>
                                        <li class="link">
                                            <a href="directives/ColorBackgroundParentDirective.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ColorBackgroundParentDirective</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/TabelaPercentualBaixaBlocoModule.html" data-type="entity-link" >TabelaPercentualBaixaBlocoModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-TabelaPercentualBaixaBlocoModule-d4eb9cfb94e57ef2daec468e1a4ba95268a4c0eead93ce6ab4bcda55df2530f8e4a66dc072f6e985cba3467765b19ecbd905a7220407803e9e000a449a510a8d"' : 'data-bs-target="#xs-components-links-module-TabelaPercentualBaixaBlocoModule-d4eb9cfb94e57ef2daec468e1a4ba95268a4c0eead93ce6ab4bcda55df2530f8e4a66dc072f6e985cba3467765b19ecbd905a7220407803e9e000a449a510a8d"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-TabelaPercentualBaixaBlocoModule-d4eb9cfb94e57ef2daec468e1a4ba95268a4c0eead93ce6ab4bcda55df2530f8e4a66dc072f6e985cba3467765b19ecbd905a7220407803e9e000a449a510a8d"' :
                                            'id="xs-components-links-module-TabelaPercentualBaixaBlocoModule-d4eb9cfb94e57ef2daec468e1a4ba95268a4c0eead93ce6ab4bcda55df2530f8e4a66dc072f6e985cba3467765b19ecbd905a7220407803e9e000a449a510a8d"' }>
                                            <li class="link">
                                                <a href="components/TabelaPercentualBaixaBlocoComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >TabelaPercentualBaixaBlocoComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/TabelaPesoBaixaBlocoModule.html" data-type="entity-link" >TabelaPesoBaixaBlocoModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-TabelaPesoBaixaBlocoModule-2f3f5c6fb21a86f299119dc867eb2f3d91e2a3661a8922257b5cf456161e2df0d3e2c3f2557017c6760761da1634461220da18cd2eabe3965c9ff839afac7812"' : 'data-bs-target="#xs-components-links-module-TabelaPesoBaixaBlocoModule-2f3f5c6fb21a86f299119dc867eb2f3d91e2a3661a8922257b5cf456161e2df0d3e2c3f2557017c6760761da1634461220da18cd2eabe3965c9ff839afac7812"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-TabelaPesoBaixaBlocoModule-2f3f5c6fb21a86f299119dc867eb2f3d91e2a3661a8922257b5cf456161e2df0d3e2c3f2557017c6760761da1634461220da18cd2eabe3965c9ff839afac7812"' :
                                            'id="xs-components-links-module-TabelaPesoBaixaBlocoModule-2f3f5c6fb21a86f299119dc867eb2f3d91e2a3661a8922257b5cf456161e2df0d3e2c3f2557017c6760761da1634461220da18cd2eabe3965c9ff839afac7812"' }>
                                            <li class="link">
                                                <a href="components/TabelaPesoBaixaBlocoComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >TabelaPesoBaixaBlocoComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/TabelaPrecoPraticadoEstoqueModule.html" data-type="entity-link" >TabelaPrecoPraticadoEstoqueModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-TabelaPrecoPraticadoEstoqueModule-fa1e61b97bfeb5ca658974453edd8506a1f5c0d26209ac12c43277860d022b0912c397da334fde9c0e5fa2800568fc4e738b24d914aa63ed20ae9e98a0d158ea"' : 'data-bs-target="#xs-components-links-module-TabelaPrecoPraticadoEstoqueModule-fa1e61b97bfeb5ca658974453edd8506a1f5c0d26209ac12c43277860d022b0912c397da334fde9c0e5fa2800568fc4e738b24d914aa63ed20ae9e98a0d158ea"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-TabelaPrecoPraticadoEstoqueModule-fa1e61b97bfeb5ca658974453edd8506a1f5c0d26209ac12c43277860d022b0912c397da334fde9c0e5fa2800568fc4e738b24d914aa63ed20ae9e98a0d158ea"' :
                                            'id="xs-components-links-module-TabelaPrecoPraticadoEstoqueModule-fa1e61b97bfeb5ca658974453edd8506a1f5c0d26209ac12c43277860d022b0912c397da334fde9c0e5fa2800568fc4e738b24d914aa63ed20ae9e98a0d158ea"' }>
                                            <li class="link">
                                                <a href="components/TabelaPrecoPraticadoEstoqueComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >TabelaPrecoPraticadoEstoqueComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/TabelaPrecosColunasModule.html" data-type="entity-link" >TabelaPrecosColunasModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-TabelaPrecosColunasModule-af25c0e73af72a07ffad0e7ef02fdd48788a55426b4fe5cfb68a6d1c983d8405e6cfbd69770c557fb5d323c9ca5c966da92f1f63e9adc74944a2d5b5fd1bfc4d"' : 'data-bs-target="#xs-components-links-module-TabelaPrecosColunasModule-af25c0e73af72a07ffad0e7ef02fdd48788a55426b4fe5cfb68a6d1c983d8405e6cfbd69770c557fb5d323c9ca5c966da92f1f63e9adc74944a2d5b5fd1bfc4d"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-TabelaPrecosColunasModule-af25c0e73af72a07ffad0e7ef02fdd48788a55426b4fe5cfb68a6d1c983d8405e6cfbd69770c557fb5d323c9ca5c966da92f1f63e9adc74944a2d5b5fd1bfc4d"' :
                                            'id="xs-components-links-module-TabelaPrecosColunasModule-af25c0e73af72a07ffad0e7ef02fdd48788a55426b4fe5cfb68a6d1c983d8405e6cfbd69770c557fb5d323c9ca5c966da92f1f63e9adc74944a2d5b5fd1bfc4d"' }>
                                            <li class="link">
                                                <a href="components/TabelaPrecosColunasComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >TabelaPrecosColunasComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/TabelaPrimeModule.html" data-type="entity-link" >TabelaPrimeModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-TabelaPrimeModule-c81bb6b82c34360b87381220b9313a21ecabb46eeb5ebfe8773dacbba3ea77c490d0e3c71a8863a534a50cd63a91e312dd27b9b6ea5c0d257ed1efbf17d740b0"' : 'data-bs-target="#xs-components-links-module-TabelaPrimeModule-c81bb6b82c34360b87381220b9313a21ecabb46eeb5ebfe8773dacbba3ea77c490d0e3c71a8863a534a50cd63a91e312dd27b9b6ea5c0d257ed1efbf17d740b0"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-TabelaPrimeModule-c81bb6b82c34360b87381220b9313a21ecabb46eeb5ebfe8773dacbba3ea77c490d0e3c71a8863a534a50cd63a91e312dd27b9b6ea5c0d257ed1efbf17d740b0"' :
                                            'id="xs-components-links-module-TabelaPrimeModule-c81bb6b82c34360b87381220b9313a21ecabb46eeb5ebfe8773dacbba3ea77c490d0e3c71a8863a534a50cd63a91e312dd27b9b6ea5c0d257ed1efbf17d740b0"' }>
                                            <li class="link">
                                                <a href="components/TabelaPrimeComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >TabelaPrimeComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/TabelaRecebimentoMaterialModule.html" data-type="entity-link" >TabelaRecebimentoMaterialModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-TabelaRecebimentoMaterialModule-96460c652cc590dceb7e02b6f2da5b403c0cf246cc41de4db71200262b6219ae108788461b37897bcb6e3b734f3618fb7a8516cea19551d7f6de598dfe6b2614"' : 'data-bs-target="#xs-components-links-module-TabelaRecebimentoMaterialModule-96460c652cc590dceb7e02b6f2da5b403c0cf246cc41de4db71200262b6219ae108788461b37897bcb6e3b734f3618fb7a8516cea19551d7f6de598dfe6b2614"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-TabelaRecebimentoMaterialModule-96460c652cc590dceb7e02b6f2da5b403c0cf246cc41de4db71200262b6219ae108788461b37897bcb6e3b734f3618fb7a8516cea19551d7f6de598dfe6b2614"' :
                                            'id="xs-components-links-module-TabelaRecebimentoMaterialModule-96460c652cc590dceb7e02b6f2da5b403c0cf246cc41de4db71200262b6219ae108788461b37897bcb6e3b734f3618fb7a8516cea19551d7f6de598dfe6b2614"' }>
                                            <li class="link">
                                                <a href="components/TabelaRecebimentoMaterialComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >TabelaRecebimentoMaterialComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/TabelaSegmentoModule.html" data-type="entity-link" >TabelaSegmentoModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-TabelaSegmentoModule-df1a71698a95293d8d5cd2a9c41d4fc2a1324a95cd46f80101775c65d561294da7d8038c7afbb5f56204f89e459e7879b43d5c4a7ad4f35c0c9d7a13a2b9c75f"' : 'data-bs-target="#xs-components-links-module-TabelaSegmentoModule-df1a71698a95293d8d5cd2a9c41d4fc2a1324a95cd46f80101775c65d561294da7d8038c7afbb5f56204f89e459e7879b43d5c4a7ad4f35c0c9d7a13a2b9c75f"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-TabelaSegmentoModule-df1a71698a95293d8d5cd2a9c41d4fc2a1324a95cd46f80101775c65d561294da7d8038c7afbb5f56204f89e459e7879b43d5c4a7ad4f35c0c9d7a13a2b9c75f"' :
                                            'id="xs-components-links-module-TabelaSegmentoModule-df1a71698a95293d8d5cd2a9c41d4fc2a1324a95cd46f80101775c65d561294da7d8038c7afbb5f56204f89e459e7879b43d5c4a7ad4f35c0c9d7a13a2b9c75f"' }>
                                            <li class="link">
                                                <a href="components/TabelaSegmentoComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >TabelaSegmentoComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/TabelaSubfatorModule.html" data-type="entity-link" >TabelaSubfatorModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-TabelaSubfatorModule-a7945c1d65814722ddb206239dd31de0844546989bafadd30643d647a0a964f136758f3b63c94d09eccad9c403552c9ce1064ccc5d711418f0c1b23955745ef0"' : 'data-bs-target="#xs-components-links-module-TabelaSubfatorModule-a7945c1d65814722ddb206239dd31de0844546989bafadd30643d647a0a964f136758f3b63c94d09eccad9c403552c9ce1064ccc5d711418f0c1b23955745ef0"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-TabelaSubfatorModule-a7945c1d65814722ddb206239dd31de0844546989bafadd30643d647a0a964f136758f3b63c94d09eccad9c403552c9ce1064ccc5d711418f0c1b23955745ef0"' :
                                            'id="xs-components-links-module-TabelaSubfatorModule-a7945c1d65814722ddb206239dd31de0844546989bafadd30643d647a0a964f136758f3b63c94d09eccad9c403552c9ce1064ccc5d711418f0c1b23955745ef0"' }>
                                            <li class="link">
                                                <a href="components/TabelaSubfatorComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >TabelaSubfatorComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/TabelaSubGrupoModule.html" data-type="entity-link" >TabelaSubGrupoModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-TabelaSubGrupoModule-ad5a4ce8cdd8ff2c069acb9ee4c88c3fd50b309df5abc9615136f6964d987cac95aa16844ea5e366ae285ffe076fd12584320b61bd45a8d328a091617e4f4586"' : 'data-bs-target="#xs-components-links-module-TabelaSubGrupoModule-ad5a4ce8cdd8ff2c069acb9ee4c88c3fd50b309df5abc9615136f6964d987cac95aa16844ea5e366ae285ffe076fd12584320b61bd45a8d328a091617e4f4586"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-TabelaSubGrupoModule-ad5a4ce8cdd8ff2c069acb9ee4c88c3fd50b309df5abc9615136f6964d987cac95aa16844ea5e366ae285ffe076fd12584320b61bd45a8d328a091617e4f4586"' :
                                            'id="xs-components-links-module-TabelaSubGrupoModule-ad5a4ce8cdd8ff2c069acb9ee4c88c3fd50b309df5abc9615136f6964d987cac95aa16844ea5e366ae285ffe076fd12584320b61bd45a8d328a091617e4f4586"' }>
                                            <li class="link">
                                                <a href="components/TabelaSubGrupoComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >TabelaSubGrupoComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/TabelaTipoMaterialModule.html" data-type="entity-link" >TabelaTipoMaterialModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-TabelaTipoMaterialModule-5c5b36071f964b932a54ff61080f3df508670e0a3414a50d46c9ea4bc78161844a22701d0aeab8be7564c9777be4f5035f9623e8abdcdccc81f8e93196f15d0b"' : 'data-bs-target="#xs-components-links-module-TabelaTipoMaterialModule-5c5b36071f964b932a54ff61080f3df508670e0a3414a50d46c9ea4bc78161844a22701d0aeab8be7564c9777be4f5035f9623e8abdcdccc81f8e93196f15d0b"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-TabelaTipoMaterialModule-5c5b36071f964b932a54ff61080f3df508670e0a3414a50d46c9ea4bc78161844a22701d0aeab8be7564c9777be4f5035f9623e8abdcdccc81f8e93196f15d0b"' :
                                            'id="xs-components-links-module-TabelaTipoMaterialModule-5c5b36071f964b932a54ff61080f3df508670e0a3414a50d46c9ea4bc78161844a22701d0aeab8be7564c9777be4f5035f9623e8abdcdccc81f8e93196f15d0b"' }>
                                            <li class="link">
                                                <a href="components/TabelaTipoMaterialComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >TabelaTipoMaterialComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/TabelaTipoMovimentacaoModule.html" data-type="entity-link" >TabelaTipoMovimentacaoModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-TabelaTipoMovimentacaoModule-e72341e3ed4db7088be7fd5da424e714c2e59f7ae8a87939bec93a2bdb3d0b367e66615d1ea890eb69b9ec26902b3e4df382ae5daaa552d0eba5ecd590080b1b"' : 'data-bs-target="#xs-components-links-module-TabelaTipoMovimentacaoModule-e72341e3ed4db7088be7fd5da424e714c2e59f7ae8a87939bec93a2bdb3d0b367e66615d1ea890eb69b9ec26902b3e4df382ae5daaa552d0eba5ecd590080b1b"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-TabelaTipoMovimentacaoModule-e72341e3ed4db7088be7fd5da424e714c2e59f7ae8a87939bec93a2bdb3d0b367e66615d1ea890eb69b9ec26902b3e4df382ae5daaa552d0eba5ecd590080b1b"' :
                                            'id="xs-components-links-module-TabelaTipoMovimentacaoModule-e72341e3ed4db7088be7fd5da424e714c2e59f7ae8a87939bec93a2bdb3d0b367e66615d1ea890eb69b9ec26902b3e4df382ae5daaa552d0eba5ecd590080b1b"' }>
                                            <li class="link">
                                                <a href="components/TabelaTipoMovimentacaoComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >TabelaTipoMovimentacaoComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/TabelaTratamentoTermicoModule.html" data-type="entity-link" >TabelaTratamentoTermicoModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-TabelaTratamentoTermicoModule-9e5522a07a138b2c84cd86bad465b3aafe39b766394e6f98f4a13d496521185f4b419f67095c141374b328a6cc8ada288bcb62fe15a4959b5ebfb28bfff9b1da"' : 'data-bs-target="#xs-components-links-module-TabelaTratamentoTermicoModule-9e5522a07a138b2c84cd86bad465b3aafe39b766394e6f98f4a13d496521185f4b419f67095c141374b328a6cc8ada288bcb62fe15a4959b5ebfb28bfff9b1da"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-TabelaTratamentoTermicoModule-9e5522a07a138b2c84cd86bad465b3aafe39b766394e6f98f4a13d496521185f4b419f67095c141374b328a6cc8ada288bcb62fe15a4959b5ebfb28bfff9b1da"' :
                                            'id="xs-components-links-module-TabelaTratamentoTermicoModule-9e5522a07a138b2c84cd86bad465b3aafe39b766394e6f98f4a13d496521185f4b419f67095c141374b328a6cc8ada288bcb62fe15a4959b5ebfb28bfff9b1da"' }>
                                            <li class="link">
                                                <a href="components/TabelaTratamentoTermicoComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >TabelaTratamentoTermicoComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/TabelaUnidadeLocalModule.html" data-type="entity-link" >TabelaUnidadeLocalModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-TabelaUnidadeLocalModule-83a553bd3290ae1b54e103b6328e07ead111f84d7cd6f35da56f31fc6f1801636ef05288d557fd4f69c64d666ebe9afcb2d9230a3cc572aafd81c9ec4a5c96b6"' : 'data-bs-target="#xs-components-links-module-TabelaUnidadeLocalModule-83a553bd3290ae1b54e103b6328e07ead111f84d7cd6f35da56f31fc6f1801636ef05288d557fd4f69c64d666ebe9afcb2d9230a3cc572aafd81c9ec4a5c96b6"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-TabelaUnidadeLocalModule-83a553bd3290ae1b54e103b6328e07ead111f84d7cd6f35da56f31fc6f1801636ef05288d557fd4f69c64d666ebe9afcb2d9230a3cc572aafd81c9ec4a5c96b6"' :
                                            'id="xs-components-links-module-TabelaUnidadeLocalModule-83a553bd3290ae1b54e103b6328e07ead111f84d7cd6f35da56f31fc6f1801636ef05288d557fd4f69c64d666ebe9afcb2d9230a3cc572aafd81c9ec4a5c96b6"' }>
                                            <li class="link">
                                                <a href="components/TabelaUnidadeLocalComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >TabelaUnidadeLocalComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/TesteInputsModule.html" data-type="entity-link" >TesteInputsModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-TesteInputsModule-10ce93556ef8ad155909ee7ea64f755e994d4acc62cc999bbf2391be7ba4e72bab66c1b5995a7ea5b5056e7722b92fab6d2baa89fdf019fcb7b3e3323de8334e"' : 'data-bs-target="#xs-components-links-module-TesteInputsModule-10ce93556ef8ad155909ee7ea64f755e994d4acc62cc999bbf2391be7ba4e72bab66c1b5995a7ea5b5056e7722b92fab6d2baa89fdf019fcb7b3e3323de8334e"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-TesteInputsModule-10ce93556ef8ad155909ee7ea64f755e994d4acc62cc999bbf2391be7ba4e72bab66c1b5995a7ea5b5056e7722b92fab6d2baa89fdf019fcb7b3e3323de8334e"' :
                                            'id="xs-components-links-module-TesteInputsModule-10ce93556ef8ad155909ee7ea64f755e994d4acc62cc999bbf2391be7ba4e72bab66c1b5995a7ea5b5056e7722b92fab6d2baa89fdf019fcb7b3e3323de8334e"' }>
                                            <li class="link">
                                                <a href="components/TesteInputsComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >TesteInputsComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/TipoMaterialModule.html" data-type="entity-link" >TipoMaterialModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-TipoMaterialModule-d31f5b909cc0736aa0729bf61ee32498355643ad92096f8f1ef69c78f96f452e46065fb6165b72f5477ae342b98b029af1b120ee9bd3a8c582f5a4372c5aeb56"' : 'data-bs-target="#xs-components-links-module-TipoMaterialModule-d31f5b909cc0736aa0729bf61ee32498355643ad92096f8f1ef69c78f96f452e46065fb6165b72f5477ae342b98b029af1b120ee9bd3a8c582f5a4372c5aeb56"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-TipoMaterialModule-d31f5b909cc0736aa0729bf61ee32498355643ad92096f8f1ef69c78f96f452e46065fb6165b72f5477ae342b98b029af1b120ee9bd3a8c582f5a4372c5aeb56"' :
                                            'id="xs-components-links-module-TipoMaterialModule-d31f5b909cc0736aa0729bf61ee32498355643ad92096f8f1ef69c78f96f452e46065fb6165b72f5477ae342b98b029af1b120ee9bd3a8c582f5a4372c5aeb56"' }>
                                            <li class="link">
                                                <a href="components/TipoMaterialComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >TipoMaterialComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/TipoMaterialRoutingModule.html" data-type="entity-link" >TipoMaterialRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/TipoMovimentacaoModule.html" data-type="entity-link" >TipoMovimentacaoModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-TipoMovimentacaoModule-2a89a929a4d7ac4103ac1399719bd9fc49ae1db315b00c7eea7b31a1ec6e828545e4d176cabb22f05012bf5060c041a8cebc5bb33f3a4bd8a095418ffc773f84"' : 'data-bs-target="#xs-components-links-module-TipoMovimentacaoModule-2a89a929a4d7ac4103ac1399719bd9fc49ae1db315b00c7eea7b31a1ec6e828545e4d176cabb22f05012bf5060c041a8cebc5bb33f3a4bd8a095418ffc773f84"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-TipoMovimentacaoModule-2a89a929a4d7ac4103ac1399719bd9fc49ae1db315b00c7eea7b31a1ec6e828545e4d176cabb22f05012bf5060c041a8cebc5bb33f3a4bd8a095418ffc773f84"' :
                                            'id="xs-components-links-module-TipoMovimentacaoModule-2a89a929a4d7ac4103ac1399719bd9fc49ae1db315b00c7eea7b31a1ec6e828545e4d176cabb22f05012bf5060c041a8cebc5bb33f3a4bd8a095418ffc773f84"' }>
                                            <li class="link">
                                                <a href="components/TipoMovimentacaoComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >TipoMovimentacaoComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/TipoMovimentacaoRoutingModule.html" data-type="entity-link" >TipoMovimentacaoRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/TratamentoTermicoModule.html" data-type="entity-link" >TratamentoTermicoModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-TratamentoTermicoModule-c4d9446ddf66a8aa993f6f3fc5e4fc9914971263cbc13a2cbcaf287d9f7f2935818ae4cd3ab47e5aa04eb7532d5446b63e2dc749e8790946e83a90e695ccab9a"' : 'data-bs-target="#xs-components-links-module-TratamentoTermicoModule-c4d9446ddf66a8aa993f6f3fc5e4fc9914971263cbc13a2cbcaf287d9f7f2935818ae4cd3ab47e5aa04eb7532d5446b63e2dc749e8790946e83a90e695ccab9a"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-TratamentoTermicoModule-c4d9446ddf66a8aa993f6f3fc5e4fc9914971263cbc13a2cbcaf287d9f7f2935818ae4cd3ab47e5aa04eb7532d5446b63e2dc749e8790946e83a90e695ccab9a"' :
                                            'id="xs-components-links-module-TratamentoTermicoModule-c4d9446ddf66a8aa993f6f3fc5e4fc9914971263cbc13a2cbcaf287d9f7f2935818ae4cd3ab47e5aa04eb7532d5446b63e2dc749e8790946e83a90e695ccab9a"' }>
                                            <li class="link">
                                                <a href="components/TratamentoTermicoComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >TratamentoTermicoComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/TratamentoTermicoRoutingModule.html" data-type="entity-link" >TratamentoTermicoRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/UnidadeLocalModule.html" data-type="entity-link" >UnidadeLocalModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-UnidadeLocalModule-8d04fe820770f6525b69f6e9720592b647393e651cb5c01f93792fd4863a7b391fa5ada3ea8dc29c7ea7f871371ba1dbf9197965ff4c3a261958ff534b389fc3"' : 'data-bs-target="#xs-components-links-module-UnidadeLocalModule-8d04fe820770f6525b69f6e9720592b647393e651cb5c01f93792fd4863a7b391fa5ada3ea8dc29c7ea7f871371ba1dbf9197965ff4c3a261958ff534b389fc3"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-UnidadeLocalModule-8d04fe820770f6525b69f6e9720592b647393e651cb5c01f93792fd4863a7b391fa5ada3ea8dc29c7ea7f871371ba1dbf9197965ff4c3a261958ff534b389fc3"' :
                                            'id="xs-components-links-module-UnidadeLocalModule-8d04fe820770f6525b69f6e9720592b647393e651cb5c01f93792fd4863a7b391fa5ada3ea8dc29c7ea7f871371ba1dbf9197965ff4c3a261958ff534b389fc3"' }>
                                            <li class="link">
                                                <a href="components/UnidadeLocalComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >UnidadeLocalComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/UnidadeLocalRoutingModule.html" data-type="entity-link" >UnidadeLocalRoutingModule</a>
                            </li>
                </ul>
                </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ? 'data-bs-target="#components-links"' :
                            'data-bs-target="#xs-components-links"' }>
                            <span class="icon ion-md-cog"></span>
                            <span>Components</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="components-links"' : 'id="xs-components-links"' }>
                            <li class="link">
                                <a href="components/AccordionComponent.html" data-type="entity-link" >AccordionComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/BaseComboComponent-1.html" data-type="entity-link" >BaseComboComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/BaseListComponent.html" data-type="entity-link" >BaseListComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/BuscaLoteComponent.html" data-type="entity-link" >BuscaLoteComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/ButtonActionComponent.html" data-type="entity-link" >ButtonActionComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/ButtonAddCircleComponent.html" data-type="entity-link" >ButtonAddCircleComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/ButtonCloseCircleComponent.html" data-type="entity-link" >ButtonCloseCircleComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/ButtonComponent.html" data-type="entity-link" >ButtonComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/ButtonConsultasLoteComponent.html" data-type="entity-link" >ButtonConsultasLoteComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/ButtonMovimentacaoComponent.html" data-type="entity-link" >ButtonMovimentacaoComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/CertificadoUsinaComponent.html" data-type="entity-link" >CertificadoUsinaComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/CheckboxComponent.html" data-type="entity-link" >CheckboxComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/CodigosFornecedorCadastradosComponent.html" data-type="entity-link" >CodigosFornecedorCadastradosComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/ComboCategoriaMovimentacaoComponent.html" data-type="entity-link" >ComboCategoriaMovimentacaoComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/ComboClassificacaoFiscalComponent.html" data-type="entity-link" >ComboClassificacaoFiscalComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/ComboColunaComponent.html" data-type="entity-link" >ComboColunaComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/ComboComposicaoQuimicaComponent.html" data-type="entity-link" >ComboComposicaoQuimicaComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/ComboCondicoesPagamentoComponent.html" data-type="entity-link" >ComboCondicoesPagamentoComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/ComboCstComponent.html" data-type="entity-link" >ComboCstComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/ComboCstEspComponent.html" data-type="entity-link" >ComboCstEspComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/ComboEmpresaComponent.html" data-type="entity-link" >ComboEmpresaComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/ComboEstadoComponent.html" data-type="entity-link" >ComboEstadoComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/ComboFormaComponent.html" data-type="entity-link" >ComboFormaComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/ComboFornecedorComponent.html" data-type="entity-link" >ComboFornecedorComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/ComboFreteComponent.html" data-type="entity-link" >ComboFreteComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/ComboGalpaoComponent.html" data-type="entity-link" >ComboGalpaoComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/ComboGrupoComponent.html" data-type="entity-link" >ComboGrupoComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/ComboGrupoSegmentoComponent.html" data-type="entity-link" >ComboGrupoSegmentoComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/ComboItemEstoqueComponent.html" data-type="entity-link" >ComboItemEstoqueComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/ComboMaterialComponent.html" data-type="entity-link" >ComboMaterialComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/ComboMedidaCorteComponent.html" data-type="entity-link" >ComboMedidaCorteComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/ComboMedidaMaterialComponent.html" data-type="entity-link" >ComboMedidaMaterialComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/ComboMotivoPagamentoComponent.html" data-type="entity-link" >ComboMotivoPagamentoComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/ComboMunicipioComponent.html" data-type="entity-link" >ComboMunicipioComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/ComboOrigemMovimentacaoComponent.html" data-type="entity-link" >ComboOrigemMovimentacaoComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/ComboPaisesComponent.html" data-type="entity-link" >ComboPaisesComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/ComboRelatorioTipoCadastroComponent.html" data-type="entity-link" >ComboRelatorioTipoCadastroComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/ComboRelatorioTipoGrupoComponent.html" data-type="entity-link" >ComboRelatorioTipoGrupoComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/ComboRelatorioTipoGrupoSegmentoComponent.html" data-type="entity-link" >ComboRelatorioTipoGrupoSegmentoComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/ComboSegmentoComponent.html" data-type="entity-link" >ComboSegmentoComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/ComboStatusLiberacaoComponent.html" data-type="entity-link" >ComboStatusLiberacaoComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/ComboSubFormaComponent.html" data-type="entity-link" >ComboSubFormaComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/ComboSubGrupoComponent.html" data-type="entity-link" >ComboSubGrupoComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/ComboTipoEntradaItemEstoqueComponent.html" data-type="entity-link" >ComboTipoEntradaItemEstoqueComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/ComboTipoLoteComponent.html" data-type="entity-link" >ComboTipoLoteComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/ComboTipoMaterialComponent.html" data-type="entity-link" >ComboTipoMaterialComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/ComboTipoMovimentacaoComponent.html" data-type="entity-link" >ComboTipoMovimentacaoComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/ComboTipoPagamentoComponent.html" data-type="entity-link" >ComboTipoPagamentoComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/ComboUfComponent.html" data-type="entity-link" >ComboUfComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/ComboUnidadeLocalComponent.html" data-type="entity-link" >ComboUnidadeLocalComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/ComboUnidadePesoComponent.html" data-type="entity-link" >ComboUnidadePesoComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/ContatosFornecedorCadastroComponent.html" data-type="entity-link" >ContatosFornecedorCadastroComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/ControlValueAccessorComponent.html" data-type="entity-link" >ControlValueAccessorComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/CriterioAtribuicaoNotasComponent.html" data-type="entity-link" >CriterioAtribuicaoNotasComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/DashboardCrescimentoEstoqueComponent.html" data-type="entity-link" >DashboardCrescimentoEstoqueComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/DashboardMenuComponent.html" data-type="entity-link" >DashboardMenuComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/EnderecosFornecedorCadastroComponent.html" data-type="entity-link" >EnderecosFornecedorCadastroComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/FiltroDescricaoComponent.html" data-type="entity-link" >FiltroDescricaoComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/FiltroFornecedorComponent.html" data-type="entity-link" >FiltroFornecedorComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/FiltroPrecoPraticadoComponent.html" data-type="entity-link" >FiltroPrecoPraticadoComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/FormItemPedidoCompraAbstractComponent.html" data-type="entity-link" >FormItemPedidoCompraAbstractComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/InputFileComponent.html" data-type="entity-link" >InputFileComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/InputMoneyComponent.html" data-type="entity-link" >InputMoneyComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/InputsTableAvaliacaoComponent.html" data-type="entity-link" >InputsTableAvaliacaoComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/InputTextareaComponent.html" data-type="entity-link" >InputTextareaComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/InputValidatorComponent.html" data-type="entity-link" >InputValidatorComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/ListComponent.html" data-type="entity-link" >ListComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/MedidaGroupComponent.html" data-type="entity-link" >MedidaGroupComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/ModalAtalhosComponent.html" data-type="entity-link" >ModalAtalhosComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/ModalContatosFornecedorComponent.html" data-type="entity-link" >ModalContatosFornecedorComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/ModalEnderecosFornecedorCadastroComponent.html" data-type="entity-link" >ModalEnderecosFornecedorCadastroComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/ModalEntradasComponent.html" data-type="entity-link" >ModalEntradasComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/ModalGerarEtiquetaComponent.html" data-type="entity-link" >ModalGerarEtiquetaComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/ModalLotesRastreabilidadeComponent.html" data-type="entity-link" >ModalLotesRastreabilidadeComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/ModalMovimentacaoComponent.html" data-type="entity-link" >ModalMovimentacaoComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/ModalPedidoCompraComponent.html" data-type="entity-link" >ModalPedidoCompraComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/PeriodoComponent.html" data-type="entity-link" >PeriodoComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/PortalComponent.html" data-type="entity-link" >PortalComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/ReportMenuComponent.html" data-type="entity-link" >ReportMenuComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/SelecionaMedidaGroupsComponent.html" data-type="entity-link" >SelecionaMedidaGroupsComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/TabelaCertificadoUsinaComponent.html" data-type="entity-link" >TabelaCertificadoUsinaComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/TabelaComprasComponent-1.html" data-type="entity-link" >TabelaComprasComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/TabelaEnsaioTemperabilidadeComponent.html" data-type="entity-link" >TabelaEnsaioTemperabilidadeComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/TabelaHistoricoQualificacaoComponent.html" data-type="entity-link" >TabelaHistoricoQualificacaoComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/TabelaIcmsIncentivoComponent.html" data-type="entity-link" >TabelaIcmsIncentivoComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/TabelaIndicadorSaldoItemEstoqueComponent.html" data-type="entity-link" >TabelaIndicadorSaldoItemEstoqueComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/TabelaLoteRastreabilidadeComponent-1.html" data-type="entity-link" >TabelaLoteRastreabilidadeComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/TabelaLotesZeradosComponent.html" data-type="entity-link" >TabelaLotesZeradosComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/TabelaRecebimentoItemComponent.html" data-type="entity-link" >TabelaRecebimentoItemComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/TemplateComponent-9.html" data-type="entity-link" >TemplateComponent</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ? 'data-bs-target="#classes-links"' :
                            'data-bs-target="#xs-classes-links"' }>
                            <span class="icon ion-ios-paper"></span>
                            <span>Classes</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="classes-links"' : 'id="xs-classes-links"' }>
                            <li class="link">
                                <a href="classes/AcabamentoMaterial.html" data-type="entity-link" >AcabamentoMaterial</a>
                            </li>
                            <li class="link">
                                <a href="classes/BuscaNumeroLoteTipoLote.html" data-type="entity-link" >BuscaNumeroLoteTipoLote</a>
                            </li>
                            <li class="link">
                                <a href="classes/CertificadoUsina.html" data-type="entity-link" >CertificadoUsina</a>
                            </li>
                            <li class="link">
                                <a href="classes/Cest.html" data-type="entity-link" >Cest</a>
                            </li>
                            <li class="link">
                                <a href="classes/ClassificacaoNcm.html" data-type="entity-link" >ClassificacaoNcm</a>
                            </li>
                            <li class="link">
                                <a href="classes/Coluna.html" data-type="entity-link" >Coluna</a>
                            </li>
                            <li class="link">
                                <a href="classes/Contato.html" data-type="entity-link" >Contato</a>
                            </li>
                            <li class="link">
                                <a href="classes/ControlValueAccessor.html" data-type="entity-link" >ControlValueAccessor</a>
                            </li>
                            <li class="link">
                                <a href="classes/CrudGenericService.html" data-type="entity-link" >CrudGenericService</a>
                            </li>
                            <li class="link">
                                <a href="classes/DadosPaginacao.html" data-type="entity-link" >DadosPaginacao</a>
                            </li>
                            <li class="link">
                                <a href="classes/EnsaioMetalografico.html" data-type="entity-link" >EnsaioMetalografico</a>
                            </li>
                            <li class="link">
                                <a href="classes/Entrada.html" data-type="entity-link" >Entrada</a>
                            </li>
                            <li class="link">
                                <a href="classes/EntradaFormValidations.html" data-type="entity-link" >EntradaFormValidations</a>
                            </li>
                            <li class="link">
                                <a href="classes/EstadoMaterial.html" data-type="entity-link" >EstadoMaterial</a>
                            </li>
                            <li class="link">
                                <a href="classes/FiltroEntradaItemEstoque.html" data-type="entity-link" >FiltroEntradaItemEstoque</a>
                            </li>
                            <li class="link">
                                <a href="classes/FiltroFornecedor.html" data-type="entity-link" >FiltroFornecedor</a>
                            </li>
                            <li class="link">
                                <a href="classes/FiltroItemEstoque.html" data-type="entity-link" >FiltroItemEstoque</a>
                            </li>
                            <li class="link">
                                <a href="classes/FiltroLoteRastreabilidade.html" data-type="entity-link" >FiltroLoteRastreabilidade</a>
                            </li>
                            <li class="link">
                                <a href="classes/FiltroManutencaoLote.html" data-type="entity-link" >FiltroManutencaoLote</a>
                            </li>
                            <li class="link">
                                <a href="classes/FiltroPedidoCompra.html" data-type="entity-link" >FiltroPedidoCompra</a>
                            </li>
                            <li class="link">
                                <a href="classes/FiltroPrecoPraticado.html" data-type="entity-link" >FiltroPrecoPraticado</a>
                            </li>
                            <li class="link">
                                <a href="classes/FiltroRecebimentoMaterial.html" data-type="entity-link" >FiltroRecebimentoMaterial</a>
                            </li>
                            <li class="link">
                                <a href="classes/FormMovimentacaoValidations.html" data-type="entity-link" >FormMovimentacaoValidations</a>
                            </li>
                            <li class="link">
                                <a href="classes/FormUtil.html" data-type="entity-link" >FormUtil</a>
                            </li>
                            <li class="link">
                                <a href="classes/Galpao.html" data-type="entity-link" >Galpao</a>
                            </li>
                            <li class="link">
                                <a href="classes/Grupo.html" data-type="entity-link" >Grupo</a>
                            </li>
                            <li class="link">
                                <a href="classes/GrupoSegmento.html" data-type="entity-link" >GrupoSegmento</a>
                            </li>
                            <li class="link">
                                <a href="classes/ItemEntrada.html" data-type="entity-link" >ItemEntrada</a>
                            </li>
                            <li class="link">
                                <a href="classes/ItemEstoque.html" data-type="entity-link" >ItemEstoque</a>
                            </li>
                            <li class="link">
                                <a href="classes/ItemEstoqueUtil.html" data-type="entity-link" >ItemEstoqueUtil</a>
                            </li>
                            <li class="link">
                                <a href="classes/ItemPedidoCompra.html" data-type="entity-link" >ItemPedidoCompra</a>
                            </li>
                            <li class="link">
                                <a href="classes/LoteMovimentacaoDetalhe.html" data-type="entity-link" >LoteMovimentacaoDetalhe</a>
                            </li>
                            <li class="link">
                                <a href="classes/MaterialEstoque.html" data-type="entity-link" >MaterialEstoque</a>
                            </li>
                            <li class="link">
                                <a href="classes/MathUtil.html" data-type="entity-link" >MathUtil</a>
                            </li>
                            <li class="link">
                                <a href="classes/Medida.html" data-type="entity-link" >Medida</a>
                            </li>
                            <li class="link">
                                <a href="classes/MedidaMaterial.html" data-type="entity-link" >MedidaMaterial</a>
                            </li>
                            <li class="link">
                                <a href="classes/MovimentacaoLote.html" data-type="entity-link" >MovimentacaoLote</a>
                            </li>
                            <li class="link">
                                <a href="classes/MovimentacaoSaldo.html" data-type="entity-link" >MovimentacaoSaldo</a>
                            </li>
                            <li class="link">
                                <a href="classes/MovimentacaoSaldoEntrada.html" data-type="entity-link" >MovimentacaoSaldoEntrada</a>
                            </li>
                            <li class="link">
                                <a href="classes/MovimentacaoSaldoSaida.html" data-type="entity-link" >MovimentacaoSaldoSaida</a>
                            </li>
                            <li class="link">
                                <a href="classes/OrdemCompra.html" data-type="entity-link" >OrdemCompra</a>
                            </li>
                            <li class="link">
                                <a href="classes/PecaCortada.html" data-type="entity-link" >PecaCortada</a>
                            </li>
                            <li class="link">
                                <a href="classes/PercentualBaixaBloco.html" data-type="entity-link" >PercentualBaixaBloco</a>
                            </li>
                            <li class="link">
                                <a href="classes/PesoBaixaBloco.html" data-type="entity-link" >PesoBaixaBloco</a>
                            </li>
                            <li class="link">
                                <a href="classes/PropriedadeMecanica.html" data-type="entity-link" >PropriedadeMecanica</a>
                            </li>
                            <li class="link">
                                <a href="classes/Qualificacao.html" data-type="entity-link" >Qualificacao</a>
                            </li>
                            <li class="link">
                                <a href="classes/Recebimento.html" data-type="entity-link" >Recebimento</a>
                            </li>
                            <li class="link">
                                <a href="classes/RecebimentoHelper.html" data-type="entity-link" >RecebimentoHelper</a>
                            </li>
                            <li class="link">
                                <a href="classes/SaldoGalpao.html" data-type="entity-link" >SaldoGalpao</a>
                            </li>
                            <li class="link">
                                <a href="classes/Segmento.html" data-type="entity-link" >Segmento</a>
                            </li>
                            <li class="link">
                                <a href="classes/SubFator.html" data-type="entity-link" >SubFator</a>
                            </li>
                            <li class="link">
                                <a href="classes/SubGrupo.html" data-type="entity-link" >SubGrupo</a>
                            </li>
                            <li class="link">
                                <a href="classes/TelaAtalho.html" data-type="entity-link" >TelaAtalho</a>
                            </li>
                            <li class="link">
                                <a href="classes/TipoMaterial.html" data-type="entity-link" >TipoMaterial</a>
                            </li>
                            <li class="link">
                                <a href="classes/TipoMovimentacao.html" data-type="entity-link" >TipoMovimentacao</a>
                            </li>
                            <li class="link">
                                <a href="classes/TiposFornecedor.html" data-type="entity-link" >TiposFornecedor</a>
                            </li>
                            <li class="link">
                                <a href="classes/TratamentoTermico.html" data-type="entity-link" >TratamentoTermico</a>
                            </li>
                            <li class="link">
                                <a href="classes/UnidadeLocal.html" data-type="entity-link" >UnidadeLocal</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ? 'data-bs-target="#injectables-links"' :
                                'data-bs-target="#xs-injectables-links"' }>
                                <span class="icon ion-md-arrow-round-down"></span>
                                <span>Injectables</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="injectables-links"' : 'id="xs-injectables-links"' }>
                                <li class="link">
                                    <a href="injectables/AlertService.html" data-type="entity-link" >AlertService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ApelidoMaterialService.html" data-type="entity-link" >ApelidoMaterialService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ApelidosItemEstoqueService.html" data-type="entity-link" >ApelidosItemEstoqueService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/AtalhosService.html" data-type="entity-link" >AtalhosService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/AuthService.html" data-type="entity-link" >AuthService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/BlocoService.html" data-type="entity-link" >BlocoService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/BreadcrumbService.html" data-type="entity-link" >BreadcrumbService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/BuscaLoteService.html" data-type="entity-link" >BuscaLoteService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/CalculoItemPedidoService.html" data-type="entity-link" >CalculoItemPedidoService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/CepService.html" data-type="entity-link" >CepService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/CertificadoService.html" data-type="entity-link" >CertificadoService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ClassificacaoFiscalService.html" data-type="entity-link" >ClassificacaoFiscalService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ComboService.html" data-type="entity-link" >ComboService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ConfirmService.html" data-type="entity-link" >ConfirmService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/DashboardCrescimentoEstoqueService.html" data-type="entity-link" >DashboardCrescimentoEstoqueService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/DataServiceGeneric.html" data-type="entity-link" >DataServiceGeneric</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/EnderecoService.html" data-type="entity-link" >EnderecoService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/EntradaService.html" data-type="entity-link" >EntradaService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ErrorHandlerImpl.html" data-type="entity-link" >ErrorHandlerImpl</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/FileDownloaderService.html" data-type="entity-link" >FileDownloaderService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/FiltroEntradaService.html" data-type="entity-link" >FiltroEntradaService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/FiltroFornecedorService.html" data-type="entity-link" >FiltroFornecedorService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/FiltroItemEstoqueService.html" data-type="entity-link" >FiltroItemEstoqueService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/FiltroLoteRastreabilidadeService.html" data-type="entity-link" >FiltroLoteRastreabilidadeService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/FiltroManutencaoLoteService.html" data-type="entity-link" >FiltroManutencaoLoteService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/FiltroPedidoCompraService.html" data-type="entity-link" >FiltroPedidoCompraService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/FiltroPrecoPraticadoService.html" data-type="entity-link" >FiltroPrecoPraticadoService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/FiltroRecebimentoMaterialService.html" data-type="entity-link" >FiltroRecebimentoMaterialService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/FormaMaterialService.html" data-type="entity-link" >FormaMaterialService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/FormDataService.html" data-type="entity-link" >FormDataService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/FornecedorClienteService.html" data-type="entity-link" >FornecedorClienteService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/FornecedorService.html" data-type="entity-link" >FornecedorService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/GalpaoService.html" data-type="entity-link" >GalpaoService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/GenericFilterStoreService.html" data-type="entity-link" >GenericFilterStoreService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/IndicadorItemEstoqueService.html" data-type="entity-link" >IndicadorItemEstoqueService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ItemEstoqueService.html" data-type="entity-link" >ItemEstoqueService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/LayoutService.html" data-type="entity-link" >LayoutService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/LoteManutencaoService.html" data-type="entity-link" >LoteManutencaoService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/LoteMovimentacaoService.html" data-type="entity-link" >LoteMovimentacaoService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/LoteRastreabilidadeService.html" data-type="entity-link" >LoteRastreabilidadeService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/MaterialService.html" data-type="entity-link" >MaterialService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/MenuService.html" data-type="entity-link" >MenuService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/MessageService.html" data-type="entity-link" >MessageService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ModalArquivoCertificadoService.html" data-type="entity-link" >ModalArquivoCertificadoService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ModalAtalhosTeclaService.html" data-type="entity-link" >ModalAtalhosTeclaService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ModalBuscaFornecedorService.html" data-type="entity-link" >ModalBuscaFornecedorService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ModalContatosFornecedorService.html" data-type="entity-link" >ModalContatosFornecedorService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ModalCortadasService.html" data-type="entity-link" >ModalCortadasService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ModalEnderecosFornecedorService.html" data-type="entity-link" >ModalEnderecosFornecedorService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ModalEntradasService.html" data-type="entity-link" >ModalEntradasService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ModalGerarEtiquetaService.html" data-type="entity-link" >ModalGerarEtiquetaService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ModalLotesRastreabilidadeService.html" data-type="entity-link" >ModalLotesRastreabilidadeService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ModalLotesService.html" data-type="entity-link" >ModalLotesService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ModalMovimentacaoService.html" data-type="entity-link" >ModalMovimentacaoService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ModalOrdemCompraService.html" data-type="entity-link" >ModalOrdemCompraService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ModalPedidoCompraService.html" data-type="entity-link" >ModalPedidoCompraService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/MovimentacaoSaldoValidationsService.html" data-type="entity-link" >MovimentacaoSaldoValidationsService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/MovimentacaoService.html" data-type="entity-link" >MovimentacaoService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/PecasCortadaService.html" data-type="entity-link" >PecasCortadaService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/PedidoCompraItemService.html" data-type="entity-link" >PedidoCompraItemService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/PedidoCompraItemStoreService.html" data-type="entity-link" >PedidoCompraItemStoreService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/PedidoCompraService.html" data-type="entity-link" >PedidoCompraService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/PedidoCompraStoreService.html" data-type="entity-link" >PedidoCompraStoreService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/PermissionsManagerService.html" data-type="entity-link" >PermissionsManagerService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/RecebimentoMaterialService.html" data-type="entity-link" >RecebimentoMaterialService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/RelatorioEntradaMaterialService.html" data-type="entity-link" >RelatorioEntradaMaterialService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/RelatorioEntradaService.html" data-type="entity-link" >RelatorioEntradaService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/RelatorioInventarioMeService.html" data-type="entity-link" >RelatorioInventarioMeService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/RelatorioItemEstoqueService.html" data-type="entity-link" >RelatorioItemEstoqueService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/RelatorioItemPedidoCompraService.html" data-type="entity-link" >RelatorioItemPedidoCompraService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/RelatorioMarcacaoLotesService.html" data-type="entity-link" >RelatorioMarcacaoLotesService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/RelatorioMaterialService.html" data-type="entity-link" >RelatorioMaterialService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/RelatorioMovimentacaoEstoqueService.html" data-type="entity-link" >RelatorioMovimentacaoEstoqueService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/RelatorioPecasCortadasService.html" data-type="entity-link" >RelatorioPecasCortadasService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/RelatorioPedidoVendaService.html" data-type="entity-link" >RelatorioPedidoVendaService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/RelatorioPrecosService.html" data-type="entity-link" >RelatorioPrecosService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/RoteamentoService.html" data-type="entity-link" >RoteamentoService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/RouteReuseStrategyImpl.html" data-type="entity-link" >RouteReuseStrategyImpl</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/S3Service.html" data-type="entity-link" >S3Service</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/SubGruposService.html" data-type="entity-link" >SubGruposService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/TabelaLotesZeradosService.html" data-type="entity-link" >TabelaLotesZeradosService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/TabelaSaldoGalpaoService.html" data-type="entity-link" >TabelaSaldoGalpaoService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/TipoMaterialService.html" data-type="entity-link" >TipoMaterialService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/TituloPaginaService.html" data-type="entity-link" >TituloPaginaService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/TokenService.html" data-type="entity-link" >TokenService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/UnidadeLocalService.html" data-type="entity-link" >UnidadeLocalService</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ? 'data-bs-target="#interceptors-links"' :
                            'data-bs-target="#xs-interceptors-links"' }>
                            <span class="icon ion-ios-swap"></span>
                            <span>Interceptors</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="interceptors-links"' : 'id="xs-interceptors-links"' }>
                            <li class="link">
                                <a href="interceptors/ErrorInterceptorService.html" data-type="entity-link" >ErrorInterceptorService</a>
                            </li>
                            <li class="link">
                                <a href="interceptors/ProgressbarInterceptor.html" data-type="entity-link" >ProgressbarInterceptor</a>
                            </li>
                            <li class="link">
                                <a href="interceptors/TokenInterceptorService.html" data-type="entity-link" >TokenInterceptorService</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ? 'data-bs-target="#guards-links"' :
                            'data-bs-target="#xs-guards-links"' }>
                            <span class="icon ion-ios-lock"></span>
                            <span>Guards</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="guards-links"' : 'id="xs-guards-links"' }>
                            <li class="link">
                                <a href="guards/AppGuard.html" data-type="entity-link" >AppGuard</a>
                            </li>
                            <li class="link">
                                <a href="guards/AuthGuard.html" data-type="entity-link" >AuthGuard</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ? 'data-bs-target="#interfaces-links"' :
                            'data-bs-target="#xs-interfaces-links"' }>
                            <span class="icon ion-md-information-circle-outline"></span>
                            <span>Interfaces</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? ' id="interfaces-links"' : 'id="xs-interfaces-links"' }>
                            <li class="link">
                                <a href="interfaces/AppConfig.html" data-type="entity-link" >AppConfig</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Atalhos.html" data-type="entity-link" >Atalhos</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Breadcrumb.html" data-type="entity-link" >Breadcrumb</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Cep.html" data-type="entity-link" >Cep</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Comprimento.html" data-type="entity-link" >Comprimento</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/CrudGeneric.html" data-type="entity-link" >CrudGeneric</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/DashboardCrescimentoEstoque.html" data-type="entity-link" >DashboardCrescimentoEstoque</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/DashboardMenu.html" data-type="entity-link" >DashboardMenu</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/DecodedToken.html" data-type="entity-link" >DecodedToken</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/EntradaMaterial.html" data-type="entity-link" >EntradaMaterial</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Error.html" data-type="entity-link" >Error</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Errors.html" data-type="entity-link" >Errors</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Espessura.html" data-type="entity-link" >Espessura</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/EstadoMaterial.html" data-type="entity-link" >EstadoMaterial</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Filter.html" data-type="entity-link" >Filter</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/FormaMaterial.html" data-type="entity-link" >FormaMaterial</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Fornecedor.html" data-type="entity-link" >Fornecedor</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/FuncaoAtalho.html" data-type="entity-link" >FuncaoAtalho</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Galpao.html" data-type="entity-link" >Galpao</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Info.html" data-type="entity-link" >Info</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/InfoPecaCalculoQuantidade.html" data-type="entity-link" >InfoPecaCalculoQuantidade</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ItemEstoqueDetalhe.html" data-type="entity-link" >ItemEstoqueDetalhe</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ItemEstoqueFiltro.html" data-type="entity-link" >ItemEstoqueFiltro</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ItemMaterialEstoqueDescricao.html" data-type="entity-link" >ItemMaterialEstoqueDescricao</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Largura.html" data-type="entity-link" >Largura</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/LayoutState.html" data-type="entity-link" >LayoutState</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/LocalEstoque.html" data-type="entity-link" >LocalEstoque</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/LoginForm.html" data-type="entity-link" >LoginForm</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/LoteManutencao.html" data-type="entity-link" >LoteManutencao</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/LoteManutencaoDetalhe.html" data-type="entity-link" >LoteManutencaoDetalhe</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/MaterialEstoque.html" data-type="entity-link" >MaterialEstoque</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Medida.html" data-type="entity-link" >Medida</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/MedidaGroup.html" data-type="entity-link" >MedidaGroup</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/MedidaMaterial.html" data-type="entity-link" >MedidaMaterial</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/MenuChangeEvent.html" data-type="entity-link" >MenuChangeEvent</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/MessageResponse.html" data-type="entity-link" >MessageResponse</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ModalStructure.html" data-type="entity-link" >ModalStructure</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ObjetoGenerico.html" data-type="entity-link" >ObjetoGenerico</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ParamsInfoGrupo.html" data-type="entity-link" >ParamsInfoGrupo</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ParamsInventarioME.html" data-type="entity-link" >ParamsInventarioME</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ParamsItemEstoque.html" data-type="entity-link" >ParamsItemEstoque</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ParamsItemEstoqueCodigo.html" data-type="entity-link" >ParamsItemEstoqueCodigo</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ParamsLoteZerado.html" data-type="entity-link" >ParamsLoteZerado</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ParamsMaterialEmpenhado.html" data-type="entity-link" >ParamsMaterialEmpenhado</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ParamsMovEntradaConsulta.html" data-type="entity-link" >ParamsMovEntradaConsulta</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ParamsMovEstoque.html" data-type="entity-link" >ParamsMovEstoque</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ParamsMovEstoqueSeg.html" data-type="entity-link" >ParamsMovEstoqueSeg</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ParamsPesoCobradoAbaixoTeorico.html" data-type="entity-link" >ParamsPesoCobradoAbaixoTeorico</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ParamsRelEntradaMaterialSeg.html" data-type="entity-link" >ParamsRelEntradaMaterialSeg</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ParamsRelEtiqueta.html" data-type="entity-link" >ParamsRelEtiqueta</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ParamsRelMarcacaoLotes.html" data-type="entity-link" >ParamsRelMarcacaoLotes</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ParamsRelPecasCortadas.html" data-type="entity-link" >ParamsRelPecasCortadas</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ParamsRelPedidoCompra.html" data-type="entity-link" >ParamsRelPedidoCompra</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ParamsRelTabelaPrecos.html" data-type="entity-link" >ParamsRelTabelaPrecos</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/PecaCortadaParcial.html" data-type="entity-link" >PecaCortadaParcial</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/PedidoCompra.html" data-type="entity-link" >PedidoCompra</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/PedidoCompraItem.html" data-type="entity-link" >PedidoCompraItem</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Permission.html" data-type="entity-link" >Permission</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/PesoTeoricoCalc.html" data-type="entity-link" >PesoTeoricoCalc</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ReportMenu.html" data-type="entity-link" >ReportMenu</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/SaldoPorFilial.html" data-type="entity-link" >SaldoPorFilial</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Segmento.html" data-type="entity-link" >Segmento</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Structure.html" data-type="entity-link" >Structure</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/SubstituicaoTributaria.html" data-type="entity-link" >SubstituicaoTributaria</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/TipoMaterial.html" data-type="entity-link" >TipoMaterial</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/UserPermission.html" data-type="entity-link" >UserPermission</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ? 'data-bs-target="#pipes-links"' :
                                'data-bs-target="#xs-pipes-links"' }>
                                <span class="icon ion-md-add"></span>
                                <span>Pipes</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="pipes-links"' : 'id="xs-pipes-links"' }>
                                <li class="link">
                                    <a href="pipes/FormataMedidaPipe.html" data-type="entity-link" >FormataMedidaPipe</a>
                                </li>
                                <li class="link">
                                    <a href="pipes/FormControlPipe.html" data-type="entity-link" >FormControlPipe</a>
                                </li>
                                <li class="link">
                                    <a href="pipes/OrigemPipe.html" data-type="entity-link" >OrigemPipe</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ? 'data-bs-target="#miscellaneous-links"'
                            : 'data-bs-target="#xs-miscellaneous-links"' }>
                            <span class="icon ion-ios-cube"></span>
                            <span>Miscellaneous</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="miscellaneous-links"' : 'id="xs-miscellaneous-links"' }>
                            <li class="link">
                                <a href="miscellaneous/enumerations.html" data-type="entity-link">Enums</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/functions.html" data-type="entity-link">Functions</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/typealiases.html" data-type="entity-link">Type aliases</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/variables.html" data-type="entity-link">Variables</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <a data-type="chapter-link" href="routes.html"><span class="icon ion-ios-git-branch"></span>Routes</a>
                        </li>
                    <li class="chapter">
                        <a data-type="chapter-link" href="coverage.html"><span class="icon ion-ios-stats"></span>Documentation coverage</a>
                    </li>
                    <li class="divider"></li>
                    <li class="copyright">
                        Documentation generated using <a href="https://compodoc.app/" target="_blank" rel="noopener noreferrer">
                            <img data-src="images/compodoc-vectorise.png" class="img-responsive" data-type="compodoc-logo">
                        </a>
                    </li>
            </ul>
        </nav>
        `);
        this.innerHTML = tp.strings;
    }
});